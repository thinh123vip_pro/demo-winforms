﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Globalization;

namespace Cn.Base
{
    public class Functions
    {
        public static bool InList(string value, string list)
        {
            if (value == "")
            {
                return false;
            }
            string[] strsource = list.Split(',');
            for (int i = 0; i < strsource.Length; i++)
            {
                string listvalue = strsource[i].Trim();
                if (value.Trim() == listvalue.Trim())
                {
                    return true;
                }
            }
            return false;
        }
        public static void DeleteDatatable(DataTable dt)
        {
            DataView dv = new DataView(dt);
            foreach(DataRowView rv in dv)
            {
                rv.Delete();
            }
        }
        public static void DeleteDatatable(DataTable dt, string dieu_kien)
        {
            if (dieu_kien.Trim() == "") dieu_kien = "1=1";
            DataView dv = new DataView(dt,dieu_kien,"",DataViewRowState.CurrentRows);
            foreach (DataRowView rv in dv)
            {
                rv.Delete();
            }
        }
        public static string ParseDate(DateTime ngay)
        {
            return "'" + ngay.ToString("MM/dd/yyyy") + "'";
        }
        public static List<KySlieu> LoadKySlieu(int nam, bool tuy_y)
        {
            List<KySlieu> kyslieu = new List<KySlieu>();
            if(tuy_y)
            {
                kyslieu.Add(new KySlieu(DateTime.Today, DateTime.Today));
            }

            for(int i = 1;i<=12;i++)
            {
                kyslieu.Add(new KySlieu(new DateTime(nam, i, 1), new DateTime(nam, i, 1).AddMonths(1).AddDays(-1)));
            }

            kyslieu.Add(new KySlieu(new DateTime(nam, 1, 1), new DateTime(nam, 3, 1).AddMonths(1).AddDays(-1)));
            kyslieu.Add(new KySlieu(new DateTime(nam, 4, 1), new DateTime(nam, 6, 1).AddMonths(1).AddDays(-1)));
            kyslieu.Add(new KySlieu(new DateTime(nam, 7, 1), new DateTime(nam, 9, 1).AddMonths(1).AddDays(-1)));
            kyslieu.Add(new KySlieu(new DateTime(nam, 10, 1), new DateTime(nam, 12, 1).AddMonths(1).AddDays(-1)));

            kyslieu.Add(new KySlieu(new DateTime(nam, 1, 1), new DateTime(nam, 6, 1).AddMonths(1).AddDays(-1)));
            kyslieu.Add(new KySlieu(new DateTime(nam, 7, 1), new DateTime(nam, 12, 1).AddMonths(1).AddDays(-1)));

            kyslieu.Add(new KySlieu(new DateTime(nam, 1, 1), new DateTime(nam, 12, 1).AddMonths(1).AddDays(-1)));

            return kyslieu;
        }
        public static SqlCommand CreateInsertCommand(string tableName, DataTable dtdes, SqlConnection cnn, string exclu_list)
        {
                string query = "insert into " + tableName + "(";
                foreach (DataColumn c in dtdes.Columns)
                {
                    if (!Functions.InList(c.ColumnName.ToUpper(), exclu_list))
                    {
                        query = query + c.ColumnName + ",";
                    }
                }
                query = query.Substring(0, query.Length - 1) + ") values(";
                foreach (DataColumn c in dtdes.Columns)
                {
                    if (!Functions.InList(c.ColumnName.ToUpper(), exclu_list))
                    {
                        query = query + "@"+c.ColumnName+",";
                    }
                }
                query = query.Substring(0, query.Length - 1) + ")";
                SqlCommand cmd = new SqlCommand(query, cnn);
                SqlParameterCollection pc = cmd.Parameters;
                foreach (DataColumn c in dtdes.Columns)
                {
                    if (!Functions.InList(c.ColumnName.ToUpper(), exclu_list))
                    {
                        if (c.DataType == typeof(System.String))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.NVarChar, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.Decimal))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.Decimal, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.Int16) || c.DataType == typeof(System.Int32))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.Int, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.Int64))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.BigInt, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.DateTime))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.DateTime, 0, c.ColumnName);
                        }
                        else
                        {
                            throw new Exception("Kieu du lieu chua dinh nghia");
                        }
                    }
                }
                return cmd;
            
        }
        public static SqlCommand CreateUpdateCommand(string tableName, DataTable dtdes, SqlConnection cnn, string strKey, string exclu_list)
        {
            
                string query = "update " + tableName + " set ";
                foreach (DataColumn c in dtdes.Columns)
                {
                    if (!Functions.InList(c.ColumnName.ToUpper(), exclu_list))
                    {
                        query = query + c.ColumnName + "=@"+c.ColumnName+",";
                    }
                }
                query = query.Substring(0, query.Length - 1) + " where " + strKey + " = @@" + strKey;
                SqlCommand cmd = new SqlCommand(query, cnn);
                SqlParameterCollection pc = cmd.Parameters;
                foreach (DataColumn c in dtdes.Columns)
                {
                    if (!Functions.InList(c.ColumnName.ToUpper(), exclu_list))
                    {
                        if (c.DataType == typeof(System.String))
                        {
                            SqlParameter p = pc.Add("@"+c.ColumnName, SqlDbType.NVarChar, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.Decimal))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.Decimal, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.Int16) || c.DataType == typeof(System.Int32))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.Int, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.Int64))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.BigInt, 0, c.ColumnName);
                        }
                        else if (c.DataType == typeof(System.DateTime))
                        {
                            SqlParameter p = pc.Add("@" + c.ColumnName, SqlDbType.DateTime, 0, c.ColumnName);
                        }
                        else
                        {
                            throw new Exception("Kieu du lieu chua dinh nghia");
                        }
                    }
                }
                SqlParameter param = null;
                if (dtdes.Columns[strKey].DataType == typeof(System.String)) param = pc.Add("@@" + strKey, SqlDbType.NVarChar, 0, strKey);
                else if (dtdes.Columns[strKey].DataType == typeof(System.Decimal)) param = pc.Add("@@" + strKey, SqlDbType.Decimal, 0, strKey);
                else if (dtdes.Columns[strKey].DataType == typeof(System.Int16)) param = pc.Add("@@" + strKey, SqlDbType.Int, 0, strKey);
                else if (dtdes.Columns[strKey].DataType == typeof(System.Int32)) param = pc.Add("@@" + strKey, SqlDbType.Int, 0, strKey);
                else if (dtdes.Columns[strKey].DataType == typeof(System.Int64)) param = pc.Add("@@" + strKey, SqlDbType.BigInt, 0, strKey);
                else if (dtdes.Columns[strKey].DataType == typeof(System.DateTime)) param = pc.Add("@@" + strKey, SqlDbType.Date, 0, strKey);
                param.SourceVersion = DataRowVersion.Original;
                return cmd;
            
        }
        public static SqlCommand CreateDeleteCommand(string tableName, DataTable dtdes, SqlConnection cnn, string strKey)
        {
            string query = "delete from " + tableName + " where " + strKey + "=@@" + strKey;
            SqlCommand cmd = new SqlCommand(query, cnn);
            SqlParameterCollection pc = cmd.Parameters;
            DataColumn c = dtdes.Columns[strKey];
            if (c.DataType == typeof(System.String))
            {
                SqlParameter p = pc.Add("@@" + c.ColumnName, SqlDbType.NVarChar, 0, c.ColumnName);
            }
            else if (c.DataType == typeof(System.Decimal))
            {
                SqlParameter p = pc.Add("@@" + c.ColumnName, SqlDbType.Decimal, 0, c.ColumnName);
            }
            else if (c.DataType == typeof(System.Int16) || c.DataType == typeof(System.Int32))
            {
                SqlParameter p = pc.Add("@@" + c.ColumnName, SqlDbType.Int, 0, c.ColumnName);
            }
            else if (c.DataType == typeof(System.Int64))
            {
                SqlParameter p = pc.Add("@@" + c.ColumnName, SqlDbType.BigInt, 0, c.ColumnName);
            }
            else if (c.DataType == typeof(System.DateTime))
            {
                SqlParameter p = pc.Add("@@" + c.ColumnName, SqlDbType.DateTime, 0, c.ColumnName);
            }
            else
            {
                throw new Exception("Kieu du lieu chua dinh nghia");
            }
            return cmd;

        }
        public static void SetGridTextColumn(DataGridView grid, string columnname, string displayname, int width,bool bound = true)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.ValueType = typeof(System.String);
            if (bound)
            {
                column.DataPropertyName = columnname;
            }
            column.Name = columnname;
            column.Width = width;
            if (displayname.Trim() == "") column.HeaderText = columnname;
            else column.HeaderText = displayname;
            grid.Columns.Add(column);
        }
        public static void SetGridDateColumn(DataGridView grid, string columnname, string displayname, int width)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.ValueType = typeof(System.DateTime);
            column.DefaultCellStyle.Format = "dd/MM/yyyy";
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            column.DataPropertyName = columnname;
            column.Name = columnname;
            column.Width = width;
            if (displayname.Trim() == "") column.HeaderText = columnname;
            else column.HeaderText = displayname;
            grid.Columns.Add(column);
        }
        public static void SetGridNumberColumn(DataGridView grid, string columnname, string displayname, int width, Type datatype, int dec)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.ValueType = datatype;
            string fomat = "N0";
            if (dec > 0) fomat = "N" + dec.ToString();
            column.DefaultCellStyle.Format = fomat;
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            column.DefaultCellStyle.NullValue = "0";
            column.DefaultCellStyle.DataSourceNullValue = "0";
            column.DataPropertyName = columnname;
            column.Name = columnname;
            column.Width = width;
            if (displayname.Trim() == "") column.HeaderText = columnname;
            else column.HeaderText = displayname;
            grid.Columns.Add(column);
        }
        public static void SetGridComboColumn(DataGridView grid, string columnname, string displayname, int width, DataTable dtSrc, string valuemember, string displaymember)
        {
            DataGridViewComboBoxColumn column = new DataGridViewComboBoxColumn();
            //column.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
            //column.AutoComplete = true;
            column.ValueType = typeof(System.String);
            column.DataPropertyName = columnname;
            column.Name = columnname;
            column.Width = width;
            if (displayname.Trim() == "") column.HeaderText = columnname;
            else column.HeaderText = displayname;
            column.DataSource = dtSrc;
            column.ValueMember = valuemember;
            column.DisplayMember = displaymember;
            column.DefaultCellStyle.DataSourceNullValue = "";
            column.DefaultCellStyle.NullValue = "";
            grid.Columns.Add(column);
        }
        public static void SetGridComboColumn(DataGridView grid, string columnname, string displayname, int width, List<string> list)
        {
            DataGridViewComboBoxColumn column = new DataGridViewComboBoxColumn();
            column.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
            //column.AutoComplete = true;
            column.ValueType = typeof(System.String);
            column.DataPropertyName = columnname;
            column.Name = columnname;
            column.Width = width;
            if (displayname.Trim() == "") column.HeaderText = columnname;
            else column.HeaderText = displayname;
            foreach(string s in list)
            {
                column.Items.Add(s);
            }
            column.DefaultCellStyle.DataSourceNullValue = "";
            column.DefaultCellStyle.NullValue = "";

            grid.Columns.Add(column);
        }

        public static string DocSo(object num)
        {
            double chuso;
            int sovonglap = 0;
            string[] so = new string[10];
            string[] tumuoi = new string[9];
            string[] tuhaimuoi = new string[9];
            string[] group = new string[6];
            string thanhchu = string.Empty;
            string am = "";
            chuso = Convert.ToDouble(num);
            if (chuso == 0)
            {
                thanhchu = " không ";
            }
            else
            {
                if (chuso < 0)
                {
                    chuso = Math.Abs(chuso);
                    am = "âm ";
                }
                //lay phan nguyen cua so da tinh toan duoc
                double sokytu = Math.Floor(Math.Log10(chuso) + 1);
                double sonhom = Math.Floor((Math.Log10(chuso) + 0.5) / 3 + 1);
                if (sokytu >= 17)
                {
                    throw (new Exception("Number too long!"));
                }

                so[0] = "một";
                so[1] = "hai";
                so[2] = "ba";
                so[3] = "bốn";
                so[4] = "năm";
                so[5] = "sáu";
                so[6] = "bảy";
                so[7] = "tám";
                so[8] = "chín";
                so[9] = "mười";

                tumuoi[0] = "mười một";
                tumuoi[1] = "mười hai";
                tumuoi[2] = "mười ba";
                tumuoi[3] = "mười bốn";
                tumuoi[4] = "mười lăm";
                tumuoi[5] = "mười sáu";
                tumuoi[6] = "mười bảy";
                tumuoi[7] = "mười tám";
                tumuoi[8] = "mười chín";
                //day dem so hai muoi
                tuhaimuoi[0] = string.Empty;
                tuhaimuoi[1] = "hai mươi";
                tuhaimuoi[2] = "ba mươi";
                tuhaimuoi[3] = "bốn mươi";
                tuhaimuoi[4] = "năm mươi";
                tuhaimuoi[5] = "sáu mươi";
                tuhaimuoi[6] = "bảy mươi"; //100.010.010.001.001
                tuhaimuoi[7] = "tám mươi";
                tuhaimuoi[8] = "chín mươi";

                //khai bao them nhom day so
                group[0] = "nghìn";
                group[1] = "triệu";
                group[2] = "tỉ";

                int k = 0;

                if (chuso > 0)
                {
                    for (int i = 0; i < Convert.ToInt64(sonhom); i++)
                    {
                        sovonglap = sovonglap + 1;
                        string hangtram = string.Empty;
                        string hangchuc = string.Empty;
                        string hangdv = string.Empty;
                        double sotien = chuso;
                        chuso = Math.Floor(chuso / 1000);
                        double new_number = Math.Floor(sotien - chuso * 1000);
                        //new khac khong thuc hien doc day so
                        if (new_number != 0)
                        {
                            double tram = Math.Floor(new_number / 100);
                            double chuc = Math.Floor((new_number - tram * 100) / 10);
                            double donvi = 0;

                            if (Convert.ToString(new_number).Length > 0)
                            {
                                donvi = Convert.ToDouble(Convert.ToString(new_number).Substring(Convert.ToString(new_number).Length - 1));
                            }

                            if (tram != 0)
                            {
                                hangtram = so[Convert.ToInt32(tram - 1)] + " trăm";
                            }
                            else
                            {
                                if (sovonglap < Convert.ToInt32(sonhom) && chuc != 0)
                                {
                                    hangtram = "không trăm ";
                                }
                            }
                            if (chuc != 0)
                            {
                                if (chuc != 1)
                                {
                                    hangchuc = tuhaimuoi[Convert.ToInt32(chuc - 1)];
                                }
                                else
                                {
                                    hangchuc = so[9];
                                }
                                if (donvi != 0 && chuc != 1)
                                {
                                    hangchuc = tuhaimuoi[Convert.ToInt32(chuc - 1)];
                                    //hang don vi =5 doc la lam
                                    if (donvi != 5)
                                    {
                                        hangdv = so[Convert.ToInt32(donvi - 1)]; //mot tram hai muoi;
                                    }
                                    else
                                    {
                                        hangdv = "lăm";
                                    }
                                }
                                //hang chuc =1 hang don vi khac 0
                                if (donvi != 0 && chuc == 1)
                                {
                                    if (Math.Abs(sotien) > 100)
                                    {
                                        hangchuc = tumuoi[Convert.ToInt32(donvi - 1)];
                                    }
                                    else
                                    {
                                        hangchuc = tumuoi[Convert.ToInt32(donvi - 1)];
                                    }
                                }
                                if (donvi == 0)
                                {
                                    hangdv = string.Empty;
                                }
                            }
                            //het phan hang chuc khac 0
                            else
                            {
                                if (donvi != 0)
                                {
                                    if (new_number > 9)
                                    {
                                        if (i == 0 && tram == 0)
                                        {
                                            hangchuc = "lẻ";
                                            hangdv = so[Convert.ToInt32(donvi - 1)];
                                        }
                                        else
                                        {
                                            hangchuc = "linh";
                                            hangdv = so[Convert.ToInt32(donvi - 1)];
                                        }
                                    }
                                    else
                                    {
                                        if (i == 0 && tram == 0)
                                        {
                                            hangchuc = "lẻ";
                                            hangdv = so[Convert.ToInt32(donvi - 1)];
                                        }
                                        else
                                        {
                                            hangchuc = string.Empty;
                                            hangdv = so[Convert.ToInt32(donvi - 1)];
                                        }
                                    }
                                }
                                else
                                {
                                    hangchuc = string.Empty;
                                    hangdv = string.Empty;
                                }
                            }
                            if (k == 0)
                            {
                                thanhchu = hangtram.Trim() + (hangchuc.Trim().Length != 0 ? " " : string.Empty) + hangchuc.Trim() +
                                           (hangdv.Trim().Length != 0 ? " ": string.Empty) + hangdv.Trim();

                            }
                            else
                            {
                                thanhchu = hangtram.Trim() + " " + hangchuc.Trim() + (hangchuc.Trim().Length != 0 ? " " : string.Empty) + hangdv.Trim() +
                                           (hangdv.Trim().Length != 0 ? " " : string.Empty) + group[Convert.ToInt32(k - 1)].Trim() + " " + thanhchu.Trim();
                            }
                        }
                        if (new_number == 0 && i * 4 > 12)
                        {
                            thanhchu = hangtram.Trim() + " " + hangchuc.Trim() + (hangchuc.Trim().Length != 0 ? " " : string.Empty) + hangdv.Trim() +
                                       (hangdv.Trim().Length != 0 ? " " : string.Empty) + group[2].Trim() + " " + thanhchu.Trim();
                        }

                        if (k < 3)
                        {
                            k = k + 1;
                        }
                        else
                        {
                            k = 1;
                        }
                    }
                }
            }
            thanhchu = am + thanhchu;
            return (thanhchu.Substring(0,1).ToUpper() + thanhchu.Substring(1,thanhchu.Length-1)).Trim() + " đồng";
        }

    }
    //Enum
    public enum DataMod
    {
        None,
        New,
        Read,
        Edit
    }
}
