﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Base
{
    public partial class FrmChonDm : CnForm
    {
        CnMain cnMain;
        DataSet Dataset;
        string Table_Name;
        string id_field;
        string name_field;
        public DataRow RowRetval;

        public FrmChonDm(CnMain cnmain, DataSet dataset, string table_name)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            //this.MdiParent = this.cnMain.mainForm;
            this.Dataset = dataset;
            this.Table_Name = table_name;
            this.id_field = this.cnMain.GetFieldNameMa(this.Table_Name);
            this.name_field = this.cnMain.GetFieldNameTen(this.Table_Name);
            this.BinGrid();
            this.ConfigGrid();
        }
        protected virtual void BinGrid()
        {
            this.grid.AutoGenerateColumns = false;
            this.grid.DataSource = this.Dataset;
            this.grid.DataMember = this.Table_Name;
            Functions.SetGridTextColumn(this.grid, id_field, "Mã",100);
            Functions.SetGridTextColumn(this.grid, name_field, "Tên",200);
        }
        protected virtual void ConfigGrid()
        {
            this.grid.ReadOnly = true;
            this.grid.CellDoubleClick += grid_CellDoubleClick;
            this.grid.KeyDown += grid_KeyDown;
        }

        void grid_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Return)
                {
                    if (this.grid.CurrentRow.DataBoundItem != null)
                    {
                        if (this.grid.CurrentRow != null)
                        {
                            this.RowRetval = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
                        }
                    }
                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.grid.CurrentRow.DataBoundItem != null)
                {
                    if (this.grid.CurrentRow != null)
                    {
                        this.RowRetval = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
                    }
                }
                this.Hide();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ". " + ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }
        
    }
}
