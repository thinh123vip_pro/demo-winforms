﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Cn.Base
{
    public partial class FrmBaseDm : CnForm
    {
        protected string Table_Name = "";
        protected DataTable Datatable;
        protected DataSet Dataset;
        protected CnMain cnMain;
        protected SqlDataAdapter Dataadapter;

        public FrmBaseDm()
        {
            InitializeComponent();
        }
        public FrmBaseDm(CnMain cnmain, string table_name)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.Table_Name = table_name;
            this.MdiParent = this.cnMain.mainForm;
        }
        protected virtual void LoadData()
        {
            this.Dataset = new DataSet();
            this.Dataadapter = new SqlDataAdapter("SELECT * FROM " + this.Table_Name, this.cnMain.sqlConnection);
            this.Dataadapter.Fill(this.Dataset,this.Table_Name);
            this.Datatable = this.Dataset.Tables[this.Table_Name];
        }
        protected virtual void LoadDanhmuc()
        {
            
        }
        protected virtual void BinGrid()
        {
            this.grid.AutoGenerateColumns = false;
            this.grid.DataSource = this.Dataset;
            this.grid.DataMember = this.Table_Name;
        }
        protected virtual void ConfigGrid()
        {
            this.grid.DataError += grid_DataError;
            this.grid.RowPostPaint += grid_RowPostPaint;
        }
        void grid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                var grid = sender as DataGridView;
                var rowIdx = (e.RowIndex + 1).ToString();

                var centerFormat = new StringFormat()
                {
                    // right alignment might actually make more sense for numbers
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };

                var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
                e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }

        void grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ThrowException)
            {
                throw e.Exception;
            }
            else
            {
                if(e.Exception.Message != "DataGridViewComboBoxCell value is not valid.")
                {
                    MessageBox.Show(e.Exception.Message);
                }
            }
        }

        

        
        
        protected virtual DataRow CopyRecord()
        {
            DataRow nr = null;
            if (this.grid.CurrentRow != null && this.grid.CurrentRow.DataBoundItem != null)
            {
                DataRow r = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
                nr = this.Datatable.NewRow();
                nr.ItemArray = (object[])r.ItemArray.Clone();
                if (this.Datatable.Columns.Contains("PK"))
                {
                    nr["PK"] = this.cnMain.GetPk(this.Table_Name);
                }
                this.Datatable.Rows.Add(nr);
            }
            
            BindingManagerBase bm = this.grid.BindingContext[this.grid.DataSource, this.grid.DataMember];
            bm.Position = this.grid.RowCount;
            return nr;
        }
        protected virtual void DeleteRecord()
        {
            if (this.grid.CurrentRow != null && this.grid.CurrentRow.DataBoundItem != null)
            {
                if(this.grid.SelectedRows.Count >0)
                {
                    foreach(DataGridViewRow grvr in this.grid.SelectedRows)
                    {
                        DataRow r = ((DataRowView)grvr.DataBoundItem).Row;
                        r.Delete();
                    }
                }
                else if (this.grid.CurrentRow != null)
                {
                    DataRow r = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
                    r.Delete();
                }
            }
        }
        protected virtual void RestoreRecord()
        {
            this.grid.CancelEdit();
            this.Datatable.RejectChanges();
        }
        protected virtual void UpdateRecord()
        {
            this.EndEdit();
            DataSet ds = this.Dataset.GetChanges();
            string field_id = this.cnMain.GetFieldNameMa(this.Table_Name);
            this.Dataadapter.InsertCommand = Functions.CreateInsertCommand(this.Table_Name, this.Datatable, this.cnMain.sqlConnection, "");
            this.Dataadapter.UpdateCommand = Functions.CreateUpdateCommand(this.Table_Name, this.Datatable, this.cnMain.sqlConnection,field_id,"");
            this.Dataadapter.DeleteCommand = Functions.CreateDeleteCommand(this.Table_Name, this.Datatable, this.cnMain.sqlConnection, field_id);
            this.Dataadapter.Update(this.Datatable);
        }
        protected virtual void RefreshRecord()
        {
            this.LoadDanhmuc();
        }
        protected virtual void FindRecord()
        {

        }
        protected virtual void ExportRecord()
        {
            
        }
        protected virtual void PrintRecord()
        {

        }
        protected virtual void CloseRecord()
        {
            this.Close();
        }
        
        protected void EndEdit()
        {
            this.grid.EndEdit();
            foreach(DataRow r in this.Datatable.Rows)
            {
                r.EndEdit();
            }
        }

        private void toolbar_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripButton item = sender as ToolStripButton;
                switch (item.Name.ToLower())
                {
                    case "copy":
                        this.CopyRecord();
                        break;
                    case "delete":
                        this.DeleteRecord();
                        break;
                    case "restore":
                        this.RestoreRecord();
                        break;
                    case "update":
                        this.UpdateRecord();
                        break;
                    case "refresh":
                        this.RefreshRecord();
                        break;
                    case "find":
                        this.FindRecord();
                        break;
                    case "export":
                        this.ExportRecord();
                        break;
                    case "print":
                        this.PrintRecord();
                        break;
                    case "close":
                        this.CloseRecord();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (!this.CheckChange())
            {
                e.Cancel = true;
            }
            base.OnClosing(e);
        }
        private bool CheckChange()
        {
            this.EndEdit();
            bool ok = true;
            if (this.Datatable.GetChanges() != null)
            {
                DialogResult result = MessageBox.Show("Bạn có muốn lưu thay đổi trước khi thoát?", "Thông báo", MessageBoxButtons.YesNoCancel);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    this.UpdateRecord();
                }
                else if (result == System.Windows.Forms.DialogResult.Cancel)
                {
                    ok = false;
                }
            }
            return ok;
        }

        private void grid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (this.grid.CurrentRow != null && this.grid.CurrentRow.DataBoundItem != null)
                {
                    this.SetAfterAddNew();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ". " + ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }
        public virtual DataRow SetAfterAddNew()
        {
            DataRow r = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
            foreach (DataColumn c in this.Datatable.Columns)
            {
                if (r[c.ColumnName] == DBNull.Value)
                {
                    if (c.ColumnName == "PK")
                    {
                        r[c.ColumnName] = this.cnMain.GetPk(this.Table_Name);
                    }
                    else if (c.ColumnName == "FK")
                    {
                    }
                    else
                    {
                        if (c.DataType == typeof(System.DateTime)) r[c.ColumnName] = DBNull.Value;
                        else if (c.DataType == typeof(System.String)) r[c.ColumnName] = string.Empty;
                        else r[c.ColumnName] = 0;
                    }
                }
            }
            return r;
        }
    }
}
