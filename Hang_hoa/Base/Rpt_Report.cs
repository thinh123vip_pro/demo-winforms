﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Cn.Base
{
    public class Rpt_Report
    {
        protected CnMain cnMain;
        public DataSet Dataset;
        public string Table_name;
        public string File_name;
        public KySlieu Kyslieu;
        public string Report_name;
        public string Dieu_kien_loc;
        public Rpt_Report(CnMain cnmain, KySlieu kyslieu, string dieu_kien_loc, DataRow rowReport)
        {
            this.cnMain = cnmain;
            this.Kyslieu = kyslieu;
            this.Dataset = new DataSet();
            this.Table_name = rowReport["TABLE_NAME"].ToString(); ;
            this.Report_name = rowReport["REPORT_NAME"].ToString();
            this.File_name = Application.StartupPath + "\\Templates\\" + rowReport["FILE_NAME"].ToString();
            this.Dieu_kien_loc = dieu_kien_loc;
        }
    }
}
