﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cn.Base
{
    public class KySlieu
    {
        public DateTime Tu_Ngay;
        public DateTime Den_Ngay;
        public DateTime Ngay_Dau_Nam;
        public string Ten_KySlieu;
        public KySlieu(DateTime tu_ngay, DateTime den_ngay)
        {
            this.Tu_Ngay = tu_ngay;
            this.Den_Ngay = den_ngay;
            this.Ngay_Dau_Nam = new DateTime(tu_ngay.Year, tu_ngay.Month, 1);
            int so_thang = den_ngay.Month - tu_ngay.Month;
            switch(so_thang)
            {
                case 0:
                    if (this.Den_Ngay == this.Tu_Ngay)
                    {
                        this.Ten_KySlieu = "Tùy ý";
                    }
                    else
                    {
                        this.Ten_KySlieu = "Tháng " + this.Tu_Ngay.Month + " năm " + this.Tu_Ngay.Year;
                    }
                    break;
                case 2:
                    if (this.Tu_Ngay.Month == 1) this.Ten_KySlieu = "Quý 1 năm " + this.Tu_Ngay.Year;
                    else if (this.Tu_Ngay.Month == 4) this.Ten_KySlieu = "Quý 2 năm " + this.Tu_Ngay.Year;
                    else if (this.Tu_Ngay.Month == 7) this.Ten_KySlieu = "Quý 3 năm " + this.Tu_Ngay.Year;
                    else if (this.Tu_Ngay.Month == 10) this.Ten_KySlieu = "Quý 4 năm " + this.Tu_Ngay.Year;
                    break;
                case 5:
                    if (this.Tu_Ngay.Month == 1) this.Ten_KySlieu = "6 tháng đầu năm " + this.Tu_Ngay.Year;
                    else if (this.Tu_Ngay.Month == 7) this.Ten_KySlieu = "6 tháng cuối năm " + this.Tu_Ngay.Year;
                    break;
                case 11:
                    this.Ten_KySlieu = "Năm " + this.Tu_Ngay.Year;
                    break;
                default:
                    break;
            }
        }
        public override string ToString()
        {
            return this.Ten_KySlieu;
        }
    }
}
