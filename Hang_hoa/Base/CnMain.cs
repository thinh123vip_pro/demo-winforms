﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Globalization;

namespace Cn.Base
{
    public class CnMain
    {
        public SqlConnection sqlConnection;
        public string ServerName = "";
        public string ServerUser = "";
        public string ServerPassword = "";
        public string DatabaseName = "";
        public string User_Id = "";
        public string User_Password = "";
        public Form mainForm;
        public DataTable dtDm_Sytemvar;
        public DateTime Day_Start = DateTime.Today;
        public CnMain()
        {
            CultureInfo ci = new CultureInfo("vi-VN");
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            this.GetConfig();
        }
        
        public void GetConfig()
        {
            this.DatabaseName = this.GetConfig("DatabaseName");
            this.ServerName = this.GetConfig("ServerName");
        }
        public string GetConfig(string key)
        {
            string value = "";
            try
            {
                value = ConfigurationManager.AppSettings[key].ToString();
            }
            catch (Exception ex)
            {
                if (key == "ServerName") value = "LAPTOP-OTBR3VAC\\SQLEXPRESS";
                Configuration _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                _config.AppSettings.Settings.Add(key, value);
                _config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("App.config");
            }
            return value;
        }
        public void SetConfig(string key, string value)
        {
            Configuration _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            try
            {
                _config.AppSettings.Settings[key].Value = value;
                _config.Save(ConfigurationSaveMode.Modified);
            }
            catch
            {
                _config.AppSettings.Settings.Add(key, value);
                _config.Save(ConfigurationSaveMode.Modified);
            }
            ConfigurationManager.RefreshSection("appSettings");
        }
        public bool Run()
        {
            try
            {
                var connectionString = $"Data Source={this.ServerName};Initial Catalog={this.DatabaseName};Integrated Security=True";
                sqlConnection = new SqlConnection(connectionString);
                this.sqlConnection.Open();
                this.SetProperty();
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public void SetProperty()
        {
            this.SetInfoSystem();
        }
        public void SetInfoSystem()
        {
            this.dtDm_Sytemvar = this.LoadDatatable("DM_SYSTEMVAR");
            DataRow fr = this.dtDm_Sytemvar.Rows.Find("DAY_START");
            this.Day_Start = Convert.ToDateTime(fr["VAR_VALUE"].ToString(), new CultureInfo("vi-VN", false));
        }
        public DataTable LoadDatatable(string table_name)
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM " + table_name,this.sqlConnection);
            DataTable dt = new DataTable(table_name);
            da.Fill(dt);
            string key = this.GetFieldNameMa(table_name);
            if (key != "")
            {
                dt.PrimaryKey = new DataColumn[] { dt.Columns[key] };
            }
            return dt;
        }
        public DataTable LoadDatatable(string table_name, string dieu_kien)
        {
            if (dieu_kien.Trim() == "") dieu_kien = " 1=1 ";
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM " + table_name + " WHERE " + dieu_kien, this.sqlConnection);
            DataTable dt = new DataTable(table_name);
            da.Fill(dt);
            string key = this.GetFieldNameMa(table_name);
            if (key != "")
            {
                dt.PrimaryKey = new DataColumn[] { dt.Columns[key] };
            }
            return dt;
        }
        public DataTable LoadDatatable(DataSet ds, string table_name)
        {
            if(ds.Tables.Contains(table_name))
            {
                ds.Tables[table_name].Clear();
            }
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM " + table_name, this.sqlConnection);
            da.Fill(ds,table_name);
            DataTable dt = ds.Tables[table_name];
            string key = this.GetFieldNameMa(table_name);
            if(key != "")
            {
                dt.PrimaryKey = new DataColumn[] { dt.Columns[key] };
            }
            return dt;
        }
        public DataTable LoadDatatable(DataSet ds, string table_name, string dieu_kien)
        {
            if (ds.Tables.Contains(table_name))
            {
                ds.Tables[table_name].Clear();
            }
            if (dieu_kien.Trim() == "") dieu_kien = " 1=1 ";
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM " + table_name + " WHERE " + dieu_kien, this.sqlConnection);
            da.Fill(ds, table_name);
            DataTable dt = ds.Tables[table_name];
            string key = this.GetFieldNameMa(table_name);
            if (key != "")
            {
                dt.PrimaryKey = new DataColumn[] { dt.Columns[key] };
            }
            return dt;
        }
        public DataTable LoadDatatableQuery(string sql, string table_name)
        {
            SqlDataAdapter da = new SqlDataAdapter(sql, this.sqlConnection);
            DataTable dt = new DataTable(table_name);
            da.Fill(dt);
            return dt;
        }
        public DataTable LoadDatatableQuery(string query, DataSet ds, string table_name)
        {
            SqlDataAdapter da = new SqlDataAdapter(query, this.sqlConnection);
            da.Fill(ds, table_name);
            DataTable dt = ds.Tables[table_name];
            return dt;
        }
        public int ExecuteNonQuery(string query)
        {
            SqlCommand cmd = new SqlCommand(query, this.sqlConnection);
            return cmd.ExecuteNonQuery();
        }
        public object ExecuteScalar(string query)
        {
            SqlCommand cmd = new SqlCommand(query, this.sqlConnection);
            return cmd.ExecuteScalar();
        }
        public string GetFieldNameMa(string table_name)
        {
            string field_name = "";
            object oj = this.ExecuteScalar("SELECT ID_FIELD FROM DM_TABLE WHERE TABLE_NAME = '" + table_name + "'");
            if(oj != null && oj != DBNull.Value)
            {
                field_name = oj.ToString();
            }
            return field_name;
        }
        public string GetFieldNameTen(string table_name)
        {
            string field_name = "";
            object oj = this.ExecuteScalar("SELECT NAME_FIELD FROM DM_TABLE WHERE TABLE_NAME = '" + table_name + "'");
            if (oj != null && oj != DBNull.Value)
            {
                field_name = oj.ToString();
            }
            return field_name;
        }
        public object GetFieldValue(DataTable datatable, string id, string field_name )
        {
            if(datatable.PrimaryKey == null)
            {
                throw new Exception("Datatable không có cột key");
            }
            DataRow fr = datatable.Rows.Find(id);
            if(fr != null)
            {
                return fr[field_name];
            }
            else
            {
                if(datatable.Columns[field_name].DataType == typeof(System.DateTime))
                {
                    return DBNull.Value;
                }
                else if (datatable.Columns[field_name].DataType == typeof(System.String))
                {
                    return string.Empty;
                }
                else
                {
                    return 0;
                }
            }
        }
        public decimal GetPk(string table_name)
        {
            decimal pk = 1;
            string query = "SELECT LAST_NUM FROM DM_KEY WHERE TABLE_NAME = '" + table_name + "'";
            object oj = this.ExecuteScalar(query);
            if(oj != null && oj != DBNull.Value)
            {
                pk = Convert.ToDecimal(oj) + 1;
                query = "UPDATE DM_KEY SET LAST_NUM = " + pk.ToString();
                this.ExecuteNonQuery(query);
            }
            else
            {
                query = "INSERT INTO DM_KEY VALUES ('" + table_name + "',1)";
                this.ExecuteNonQuery(query);
            }
            return pk;
        }
        public object GetSystemVar(string var_name)
        {
            DataRow fr = this.dtDm_Sytemvar.Rows.Find(var_name);
            if (fr == null) throw new Exception("Biến: " + var_name + " không tồn tại");
            switch(fr["VAR_TYPE"].ToString())
            {
                case "STRING":
                    return fr["VAR_VALUE"].ToString();
                case "INT":
                    return Convert.ToInt32(fr["VAR_VALUE"]);
                case "DATE":
                    return Convert.ToDateTime(fr["VAR_VALUE"].ToString(),new CultureInfo("vi-VN",false));
                case "BOOLEAN":
                    return Convert.ToBoolean(fr["VAR_VALUE"]);
                default:
                    break;
            }
            return null;
        }
        public void ProcessTudienControl(DataSet dataset, string table_name, TextBox texbox, bool always = false)
        {
            string val = texbox.Text;
            if (!always && (val == "" || dataset.Tables[table_name].Rows.Find(val) != null)) return;
            FrmChonDm frm = new FrmChonDm(this, dataset, table_name);
            frm.ShowDialog();
            if(frm.RowRetval != null)
            {
                texbox.Text = frm.RowRetval[0].ToString();
            }
        }
        public void ProcessTudienGrid(DataSet dataset, string table_name, DataGridViewTextBoxEditingControl texbox, bool always = false)
        {
            string val = texbox.EditingControlFormattedValue.ToString();
            if (!always && (val == "" || dataset.Tables[table_name].Rows.Find(val) != null)) return;
            FrmChonDm frm = new FrmChonDm(this, dataset, table_name);
            frm.ShowDialog();
            if (frm.RowRetval != null)
            {
                texbox.EditingControlFormattedValue = frm.RowRetval[0].ToString();
            }
        }
    }
}
