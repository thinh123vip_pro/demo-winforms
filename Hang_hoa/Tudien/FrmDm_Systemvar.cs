﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_Systemvar : FrmBaseDm
    {
        public FrmDm_Systemvar(CnMain cnmain)
            : base(cnmain, "DM_SYSTEMVAR")
        {
            InitializeComponent();
            this.LoadData();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            List<string> listtype = new List<string>();
            listtype.Add("INT");
            listtype.Add("STRING");
            listtype.Add("DATE");
            Functions.SetGridTextColumn(this.grid, "VAR_NAME", "", 200);
            Functions.SetGridTextColumn(this.grid, "VAR_VALUE", "", 200);
            Functions.SetGridTextColumn(this.grid, "DESCRIPTION", "", 200);
            Functions.SetGridComboColumn(this.grid, "VAR_TYPE", "", 100,listtype);
        }
        protected override void UpdateRecord()
        {
            base.UpdateRecord();
            this.cnMain.SetInfoSystem();
        }
    }
}
