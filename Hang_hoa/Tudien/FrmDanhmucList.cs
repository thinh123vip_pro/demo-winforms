﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDanhmucList : CnForm
    {
        CnMain cnMain;
        public FrmDanhmucList(CnMain cnmain)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.MdiParent = this.cnMain.mainForm;
            this.LoadList();
        }
        private void LoadList()
        {
            DataTable dt = this.cnMain.LoadDatatable("DM_TABLE", "TABLE_TYPE = 'TD'");
            foreach(DataRow r in dt.Rows)
            {
                ListViewItem item = new ListViewItem(new string[2]{r["TABLE_NAME"].ToString(),r["DISPLAY_NAME"].ToString()});
                this.listView1.Items.Add(item);
            }
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.LoadDanhmuc();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadDanhmuc()
        {
            if(this.listView1.SelectedItems.Count==0)
            {
                return;
            }
            ListViewItem item = (ListViewItem)this.listView1.SelectedItems[0];
            string table_name = item.SubItems[0].Text;
            switch(table_name.ToLower())
            {
                case "dm_kho":
                    (new FrmDm_Kho(this.cnMain)).Show();
                    break;
                case "dm_vthh":
                    (new FrmDm_Vthh(this.cnMain)).Show();
                    break;
                case "dm_nhom_vthh":
                    (new FrmDm_Nhom_Vthh(this.cnMain)).Show();
                    break;
                case "dm_dtpn":
                    (new FrmDm_Dtpn(this.cnMain)).Show();
                    break;
                case "dm_nhom_dtpn":
                    (new FrmDm_Nhom_Dtpn(this.cnMain)).Show();
                    break;
                case "dm_dtgt":
                    (new FrmDm_Dtgt(this.cnMain)).Show();
                    break;
                case "dm_nhom_dtgt":
                    (new FrmDm_Nhom_Dtgt(this.cnMain)).Show();
                    break;
                case "dm_ptnx":
                    (new FrmDm_Ptnx(this.cnMain)).Show();
                    break;
                case "dm_table":
                    (new FrmDm_Table(this.cnMain)).Show();
                    break;
                case "dm_systemvar":
                    (new FrmDm_Systemvar(this.cnMain)).Show();
                    break;
                case "dm_report":
                    (new FrmDm_Report(this.cnMain)).Show();
                    break;
                default:
                    break;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                this.LoadDanhmuc();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
