﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_Report : FrmBaseDm
    {
        public FrmDm_Report(CnMain cnmain)
            : base(cnmain, "DM_REPORT")
        {
            InitializeComponent();
            this.LoadData();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            Functions.SetGridTextColumn(this.grid, "REPORT_ID", "", 200);
            Functions.SetGridTextColumn(this.grid, "REPORT_NAME", "", 300);
            Functions.SetGridTextColumn(this.grid, "TABLE_NAME", "", 200);
            Functions.SetGridTextColumn(this.grid, "FILE_NAME", "", 200);
            Functions.SetGridTextColumn(this.grid, "FILTER_LIST", "", 500);
            Functions.SetGridNumberColumn(this.grid, "STT", "STT", 50, this.Datatable.Columns["STT"].DataType, 0);
        }
    }
}
