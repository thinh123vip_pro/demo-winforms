﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_Vthh : FrmBaseDm
    {
        public FrmDm_Vthh(CnMain cnmain):base(cnmain,"DM_VTHH")
        {
            InitializeComponent();
            this.LoadData();
            this.LoadDanhmuc();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void LoadDanhmuc()
        {
            this.cnMain.LoadDatatable(this.Dataset, "DM_NHOM_VTHH");
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            Functions.SetGridTextColumn(this.grid, "MA_VTHH", "Mã vthh", 100);
            Functions.SetGridTextColumn(this.grid, "TEN_VTHH", "Tên vthh", 200);
            Functions.SetGridTextColumn(this.grid, "DVT", "Đvt", 100);
            Functions.SetGridTextColumn(this.grid, "MA_NHOM_VTHH", "Nhóm vthh", 100);
        }
        protected override void ConfigGrid()
        {
            base.ConfigGrid();
            this.grid.CellLeave += grid_CellLeave;
            this.grid.CellDoubleClick += grid_CellDoubleClick;
            this.grid.CurrentCellChanged += grid_CurrentCellChanged;
        }
        void grid_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.grid.CurrentCell != null)
                {
                    string columnname = this.grid.CurrentCell.OwningColumn.Name;
                    switch (columnname)
                    {
                        case "MA_NHOM_VTHH":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_NHOM_VTHH"], this.grid.CurrentCell.Value.ToString(), "TEN_NHOM_VTHH").ToString();
                            break;
                        default:
                            this.toolStripMsg.Text = "";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void grid_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!(this.grid.EditingControl is DataGridViewTextBoxEditingControl))
                {
                    return;
                }
                if (this.grid.CurrentRow.DataBoundItem == null || this.grid.EditingControl == null || this.Datatable.Columns.IndexOf(this.grid.Columns[e.ColumnIndex].Name) < 0) return;
                string colname = this.grid.Columns[e.ColumnIndex].Name;
                DataGridViewTextBoxEditingControl textbox = (DataGridViewTextBoxEditingControl)(sender as DataGridView).EditingControl;
                DataRowView row = (DataRowView)this.grid.CurrentRow.DataBoundItem;
                row[this.grid.Columns[e.ColumnIndex].Name] = textbox.EditingControlFormattedValue;
                switch (colname)
                {
                    case "MA_NHOM_VTHH":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_NHOM_VTHH", textbox);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!(this.grid.EditingControl is DataGridViewTextBoxEditingControl))
                {
                    return;
                }
                if (this.grid.CurrentRow.DataBoundItem == null || this.grid.EditingControl == null || this.Datatable.Columns.IndexOf(this.grid.Columns[e.ColumnIndex].Name) < 0) return;
                string colname = this.grid.Columns[e.ColumnIndex].Name;
                DataGridViewTextBoxEditingControl textbox = (DataGridViewTextBoxEditingControl)(sender as DataGridView).EditingControl;
                DataRowView row = (DataRowView)this.grid.CurrentRow.DataBoundItem;
                row[this.grid.Columns[e.ColumnIndex].Name] = textbox.EditingControlFormattedValue;
                switch (colname)
                {
                    case "MA_NHOM_VTHH":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_NHOM_VTHH", textbox, true);
                        this.grid.EndEdit();
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
