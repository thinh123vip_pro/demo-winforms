﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_Ptnx : FrmBaseDm
    {
        public FrmDm_Ptnx(CnMain cnmain)
            : base(cnmain, "DM_PTNX")
        {
            InitializeComponent();
            this.LoadData();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            List<string> listNX = new List<string>();
            listNX.Add("N");
            listNX.Add("X");
            Functions.SetGridTextColumn(this.grid, "MA_PTNX", "Mã ptnx", 100);
            Functions.SetGridTextColumn(this.grid, "TEN_PTNX", "Tên ptnx", 200);
            Functions.SetGridComboColumn(this.grid, "NX", "",50, listNX);
        }
    }
}
