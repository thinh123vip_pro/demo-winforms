﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_User : FrmBaseDm
    {
        public FrmDm_User(CnMain cnmain)
            : base(cnmain, "DM_USER")
        {
            InitializeComponent();
            this.LoadData();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            Functions.SetGridTextColumn(this.grid, "USER_ID", "", 100);
            Functions.SetGridTextColumn(this.grid, "USER_NAME", "", 200);
            Functions.SetGridTextColumn(this.grid, "PASSWORD", "", 200);
        }
    }
}
