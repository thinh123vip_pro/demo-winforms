﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_Nhom_Dtgt : FrmBaseDm
    {
        public FrmDm_Nhom_Dtgt(CnMain cnmain)
            : base(cnmain, "DM_NHOM_DTGT")
        {
            InitializeComponent();
            this.LoadData();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            Functions.SetGridTextColumn(this.grid, "MA_NHOM_DTGT", "Mã nhóm đtgt", 100);
            Functions.SetGridTextColumn(this.grid, "TEN_NHOM_DTGT", "Tên nhóm đtgt", 200);
        }
    }
}
