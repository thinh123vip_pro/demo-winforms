﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_Table : FrmBaseDm
    {
        public FrmDm_Table(CnMain cnmain)
            : base(cnmain, "DM_TABLE")
        {
            InitializeComponent();
            this.LoadData();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            Functions.SetGridTextColumn(this.grid, "TABLE_NAME", "", 100);
            Functions.SetGridTextColumn(this.grid, "DISPLAY_NAME", "", 200);
            Functions.SetGridTextColumn(this.grid, "ID_FIELD", "", 100);
            Functions.SetGridTextColumn(this.grid, "NAME_FIELD", "", 100);
            Functions.SetGridTextColumn(this.grid, "TABLE_TYPE", "", 100);
            Functions.SetGridTextColumn(this.grid, "DIC_TYPE", "", 100);
        }
    }
}
