﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.Tudien
{
    public partial class FrmDm_Kho : FrmBaseDm
    {
        public FrmDm_Kho(CnMain cnmain)
            : base(cnmain, "DM_KHO")
        {
            InitializeComponent();
            this.LoadData();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            Functions.SetGridTextColumn(this.grid, "MA_KHO", "Mã kho", 100);
            Functions.SetGridTextColumn(this.grid, "TEN_KHO", "Tên kho", 200);
        }
    }
}
