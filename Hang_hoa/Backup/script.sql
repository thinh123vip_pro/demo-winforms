USE [master]
GO
/****** Object:  Database [Hang_hoa]    Script Date: 1/13/2022 8:52:25 PM ******/
CREATE DATABASE [Hang_hoa]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CnvtBlank_Data', FILENAME = N'C:\Database\CNVT_BLANK.mdf' , SIZE = 4800KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'CnvtBlank_Log', FILENAME = N'C:\Database\CNVT_BLANK_log.ldf' , SIZE = 1792KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Hang_hoa] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Hang_hoa].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [Hang_hoa] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Hang_hoa] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Hang_hoa] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Hang_hoa] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Hang_hoa] SET ARITHABORT OFF 
GO
ALTER DATABASE [Hang_hoa] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Hang_hoa] SET AUTO_SHRINK ON 
GO
ALTER DATABASE [Hang_hoa] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Hang_hoa] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Hang_hoa] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Hang_hoa] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Hang_hoa] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Hang_hoa] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Hang_hoa] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Hang_hoa] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Hang_hoa] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Hang_hoa] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Hang_hoa] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Hang_hoa] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Hang_hoa] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Hang_hoa] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Hang_hoa] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Hang_hoa] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Hang_hoa] SET  MULTI_USER 
GO
ALTER DATABASE [Hang_hoa] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [Hang_hoa] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Hang_hoa] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Hang_hoa] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Hang_hoa] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Hang_hoa] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Hang_hoa] SET QUERY_STORE = OFF
GO
USE [Hang_hoa]
GO
/****** Object:  Table [dbo].[DM_DTPN]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_DTPN](
	[MA_DTPN] [nvarchar](20) NOT NULL,
	[TEN_DTPN] [nvarchar](100) NOT NULL,
	[MA_NHOM_DTPN] [nvarchar](20) NOT NULL,
	[DIA_CHI] [nvarchar](100) NOT NULL,
	[SDT] [nvarchar](20) NOT NULL,
	[FAX] [nvarchar](20) NOT NULL,
	[MST] [nvarchar](50) NOT NULL,
	[GHI_CHU] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_DM_DTPN] PRIMARY KEY CLUSTERED 
(
	[MA_DTPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VTHH]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VTHH](
	[PK] [decimal](18, 0) NOT NULL,
	[NX] [nvarchar](2) NOT NULL,
	[SO_CTU] [nvarchar](20) NOT NULL,
	[NGAY_CTU] [smalldatetime] NOT NULL,
	[SO_HDON] [nvarchar](30) NOT NULL,
	[NGAY_HDON] [nvarchar](10) NOT NULL,
	[HO_TEN] [nvarchar](40) NOT NULL,
	[MA_DTPN] [nvarchar](20) NOT NULL,
	[MA_KHO] [nvarchar](20) NOT NULL,
	[MA_PTNX] [nvarchar](20) NOT NULL,
	[DIEN_GIAI] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_VTHH] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_KHO]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_KHO](
	[MA_KHO] [nvarchar](20) NOT NULL,
	[TEN_KHO] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_DM_KHO] PRIMARY KEY CLUSTERED 
(
	[MA_KHO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_PTNX]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_PTNX](
	[MA_PTNX] [nvarchar](20) NOT NULL,
	[TEN_PTNX] [nvarchar](100) NOT NULL,
	[NX] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_DM_PTNX] PRIMARY KEY CLUSTERED 
(
	[MA_PTNX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VTHH_VIEW_HEADER]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VTHH_VIEW_HEADER]
AS
SELECT     dbo.VTHH.*, dbo.DM_DTPN.TEN_DTPN, dbo.DM_DTPN.MA_NHOM_DTPN, dbo.DM_KHO.TEN_KHO, dbo.DM_PTNX.TEN_PTNX
FROM         dbo.VTHH LEFT OUTER JOIN
                      dbo.DM_KHO ON dbo.VTHH.MA_KHO = dbo.DM_KHO.MA_KHO LEFT OUTER JOIN
                      dbo.DM_PTNX ON dbo.VTHH.MA_PTNX = dbo.DM_PTNX.MA_PTNX LEFT OUTER JOIN
                      dbo.DM_DTPN ON dbo.VTHH.MA_DTPN = dbo.DM_DTPN.MA_DTPN
GO
/****** Object:  Table [dbo].[DM_NHOM_VTHH]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_NHOM_VTHH](
	[MA_NHOM_VTHH] [nvarchar](20) NOT NULL,
	[TEN_NHOM_VTHH] [nvarchar](100) NOT NULL,
	[PP_GVON] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DM_NHOM_VTHH] PRIMARY KEY CLUSTERED 
(
	[MA_NHOM_VTHH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_NHOM_DTGT]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_NHOM_DTGT](
	[MA_NHOM_DTGT] [nvarchar](20) NOT NULL,
	[TEN_NHOM_DTGT] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_DM_NHOM_DTGT] PRIMARY KEY CLUSTERED 
(
	[MA_NHOM_DTGT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_NHOM_DTPN]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_NHOM_DTPN](
	[MA_NHOM_DTPN] [nvarchar](20) NOT NULL,
	[TEN_NHOM_DTPN] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_DM_NHOM_DTPN] PRIMARY KEY CLUSTERED 
(
	[MA_NHOM_DTPN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VTHH_CT]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VTHH_CT](
	[PK] [decimal](18, 0) NOT NULL,
	[FK] [decimal](18, 0) NOT NULL,
	[STT] [int] NOT NULL,
	[MA_VTHH] [nvarchar](20) NOT NULL,
	[NOI_DUNG] [nvarchar](200) NOT NULL,
	[GHI_CHU] [nvarchar](200) NOT NULL,
	[SO_LUONG] [decimal](19, 4) NOT NULL,
	[GIA_GOC] [decimal](19, 4) NOT NULL,
	[TIEN_GOC] [decimal](19, 4) NOT NULL,
	[THUE_SUAT_VAT] [decimal](19, 4) NOT NULL,
	[TIEN_THUE_VAT] [decimal](19, 4) NOT NULL,
	[TONG_TIEN] [decimal](19, 4) NOT NULL,
	[GIA_VON] [decimal](19, 4) NOT NULL,
	[TIEN_VON] [decimal](19, 4) NOT NULL,
	[MA_DTGT] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_VTHH_CT] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_VTHH]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_VTHH](
	[MA_VTHH] [nvarchar](20) NOT NULL,
	[TEN_VTHH] [nvarchar](100) NOT NULL,
	[DVT] [nvarchar](20) NOT NULL,
	[MA_NHOM_VTHH] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DM_VTHH] PRIMARY KEY CLUSTERED 
(
	[MA_VTHH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_DTGT]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_DTGT](
	[MA_DTGT] [nvarchar](20) NOT NULL,
	[TEN_DTGT] [nvarchar](200) NOT NULL,
	[MA_NHOM_DTGT] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_DM_DTGT] PRIMARY KEY CLUSTERED 
(
	[MA_DTGT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VTHH_VIEW]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VTHH_VIEW]
AS
SELECT     dbo.VTHH_CT.*, dbo.VTHH.PK AS PK_CTU, dbo.VTHH.NX, dbo.VTHH.SO_CTU, dbo.VTHH.NGAY_CTU, dbo.VTHH.SO_HDON, dbo.VTHH.NGAY_HDON, dbo.VTHH.HO_TEN, dbo.VTHH.MA_DTPN, 
                      dbo.VTHH.MA_KHO, dbo.VTHH.MA_PTNX, dbo.VTHH.DIEN_GIAI, dbo.DM_VTHH.TEN_VTHH, dbo.DM_VTHH.DVT, dbo.DM_VTHH.MA_NHOM_VTHH, dbo.DM_PTNX.TEN_PTNX, 
                      dbo.DM_DTGT.TEN_DTGT, dbo.DM_DTGT.MA_NHOM_DTGT, dbo.DM_DTPN.TEN_DTPN, dbo.DM_DTPN.MA_NHOM_DTPN, dbo.DM_DTPN.DIA_CHI, dbo.DM_DTPN.SDT, dbo.DM_DTPN.FAX, 
                      dbo.DM_DTPN.MST, dbo.DM_KHO.TEN_KHO, dbo.DM_NHOM_VTHH.TEN_NHOM_VTHH, dbo.DM_NHOM_DTGT.TEN_NHOM_DTGT, dbo.DM_NHOM_DTPN.TEN_NHOM_DTPN
FROM         dbo.DM_KHO RIGHT OUTER JOIN
                      dbo.DM_DTPN LEFT OUTER JOIN
                      dbo.DM_NHOM_DTPN ON dbo.DM_DTPN.MA_NHOM_DTPN = dbo.DM_NHOM_DTPN.MA_NHOM_DTPN RIGHT OUTER JOIN
                      dbo.VTHH_CT INNER JOIN
                      dbo.VTHH ON dbo.VTHH_CT.FK = dbo.VTHH.PK ON dbo.DM_DTPN.MA_DTPN = dbo.VTHH.MA_DTPN ON dbo.DM_KHO.MA_KHO = dbo.VTHH.MA_KHO LEFT OUTER JOIN
                      dbo.DM_NHOM_DTGT RIGHT OUTER JOIN
                      dbo.DM_DTGT ON dbo.DM_NHOM_DTGT.MA_NHOM_DTGT = dbo.DM_DTGT.MA_NHOM_DTGT ON dbo.VTHH_CT.MA_DTGT = dbo.DM_DTGT.MA_DTGT LEFT OUTER JOIN
                      dbo.DM_PTNX ON dbo.VTHH.MA_PTNX = dbo.DM_PTNX.MA_PTNX LEFT OUTER JOIN
                      dbo.DM_VTHH LEFT OUTER JOIN
                      dbo.DM_NHOM_VTHH ON dbo.DM_VTHH.MA_NHOM_VTHH = dbo.DM_NHOM_VTHH.MA_NHOM_VTHH ON dbo.VTHH_CT.MA_VTHH = dbo.DM_VTHH.MA_VTHH
GO
/****** Object:  Table [dbo].[DU_VTHH]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DU_VTHH](
	[PK] [decimal](18, 0) NOT NULL,
	[NGAY_CTU] [smalldatetime] NOT NULL,
	[MA_KHO] [nvarchar](20) NOT NULL,
	[MA_VTHH] [nvarchar](20) NOT NULL,
	[SO_LUONG] [decimal](19, 4) NOT NULL,
	[GIA_VON] [decimal](19, 4) NOT NULL,
	[TIEN_VON] [decimal](19, 4) NOT NULL,
 CONSTRAINT [PK_DU_VTHH] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[DU_VTHH_VIEW]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DU_VTHH_VIEW]
AS
SELECT     dbo.DU_VTHH.*, dbo.DM_VTHH.TEN_VTHH, dbo.DM_VTHH.DVT, dbo.DM_VTHH.MA_NHOM_VTHH, dbo.DM_KHO.TEN_KHO, dbo.DM_NHOM_VTHH.TEN_NHOM_VTHH
FROM         dbo.DM_NHOM_VTHH RIGHT OUTER JOIN
                      dbo.DM_VTHH ON dbo.DM_NHOM_VTHH.MA_NHOM_VTHH = dbo.DM_VTHH.MA_NHOM_VTHH RIGHT OUTER JOIN
                      dbo.DU_VTHH LEFT OUTER JOIN
                      dbo.DM_KHO ON dbo.DU_VTHH.MA_KHO = dbo.DM_KHO.MA_KHO ON dbo.DM_VTHH.MA_VTHH = dbo.DU_VTHH.MA_VTHH
GO
/****** Object:  View [dbo].[VTHH_TOTAL]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[VTHH_TOTAL] AS SELECT     'DU' AS NX,PK AS PK_CTU,NGAY_CTU,MA_KHO,TEN_KHO,MA_VTHH,TEN_VTHH,DVT,MA_NHOM_VTHH,TEN_NHOM_VTHH,SO_LUONG,GIA_VON,TIEN_VON
FROM         dbo.DU_VTHH_VIEW
UNION ALL
SELECT     NX,PK_CTU,NGAY_CTU,MA_KHO,TEN_KHO,MA_VTHH,TEN_VTHH,DVT,MA_NHOM_VTHH,TEN_NHOM_VTHH,SO_LUONG,GIA_VON,TIEN_VON
FROM         dbo.VTHH_VIEW







GO
/****** Object:  Table [dbo].[DM_KEY]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_KEY](
	[TABLE_NAME] [nvarchar](50) NOT NULL,
	[LAST_NUM] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_DM_KEY] PRIMARY KEY CLUSTERED 
(
	[TABLE_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_REPORT]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_REPORT](
	[REPORT_ID] [nvarchar](50) NOT NULL,
	[REPORT_NAME] [nvarchar](200) NOT NULL,
	[TABLE_NAME] [nvarchar](100) NOT NULL,
	[FILE_NAME] [nvarchar](100) NOT NULL,
	[FILTER_LIST] [nvarchar](500) NOT NULL,
	[STT] [int] NOT NULL,
 CONSTRAINT [PK_DM_REPORT] PRIMARY KEY CLUSTERED 
(
	[REPORT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_SYSTEMVAR]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_SYSTEMVAR](
	[VAR_NAME] [nvarchar](100) NOT NULL,
	[VAR_VALUE] [nvarchar](200) NOT NULL,
	[DESCRIPTION] [nvarchar](200) NOT NULL,
	[VAR_TYPE] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_DM_SYSTEMVAR] PRIMARY KEY CLUSTERED 
(
	[VAR_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_TABLE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_TABLE](
	[TABLE_NAME] [nvarchar](50) NOT NULL,
	[DISPLAY_NAME] [nvarchar](200) NOT NULL,
	[ID_FIELD] [nvarchar](50) NOT NULL,
	[NAME_FIELD] [nvarchar](50) NOT NULL,
	[TABLE_TYPE] [nvarchar](10) NOT NULL,
	[DIC_TYPE] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_DM_TABLE] PRIMARY KEY CLUSTERED 
(
	[TABLE_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DM_USER]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DM_USER](
	[USER_ID] [nvarchar](20) NOT NULL,
	[USER_NAME] [nvarchar](50) NOT NULL,
	[PASSWORD] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_DM_USER] PRIMARY KEY CLUSTERED 
(
	[USER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_DM_USER_PERMISSIONS] UNIQUE NONCLUSTERED 
(
	[USER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DM_REPORT] ADD  CONSTRAINT [DF_DM_REPORT_TABLE_NAME]  DEFAULT ('') FOR [TABLE_NAME]
GO
ALTER TABLE [dbo].[VTHH] ADD  CONSTRAINT [DF_VTHH_NGAY_HDON]  DEFAULT ('') FOR [NGAY_HDON]
GO
ALTER TABLE [dbo].[VTHH] ADD  CONSTRAINT [DF_VTHH_HO_TEN]  DEFAULT ('') FOR [HO_TEN]
GO
ALTER TABLE [dbo].[DM_DTPN]  WITH NOCHECK ADD  CONSTRAINT [FK_DM_DTPN_DM_NHOM_DTPN] FOREIGN KEY([MA_NHOM_DTPN])
REFERENCES [dbo].[DM_NHOM_DTPN] ([MA_NHOM_DTPN])
ON UPDATE CASCADE
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DM_DTPN] CHECK CONSTRAINT [FK_DM_DTPN_DM_NHOM_DTPN]
GO
ALTER TABLE [dbo].[DM_VTHH]  WITH NOCHECK ADD  CONSTRAINT [FK_DM_VTHH_DM_NHOM_VTHH] FOREIGN KEY([MA_NHOM_VTHH])
REFERENCES [dbo].[DM_NHOM_VTHH] ([MA_NHOM_VTHH])
ON UPDATE CASCADE
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DM_VTHH] CHECK CONSTRAINT [FK_DM_VTHH_DM_NHOM_VTHH]
GO
/****** Object:  Trigger [dbo].[DM_DTGT_DELETE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE TRIGGER [dbo].[DM_DTGT_DELETE] ON [dbo].[DM_DTGT] FOR DELETE  AS 	if  
 ( 	
 exists(select 'true' from DELETED I,VTHH_CT J where I.MA_DTGT = J.MA_DTGT and I.MA_DTGT <> '') 	
 )	 	
 begin 			 	
 raiserror('TRIGGER_DELETE',16,1) rollback tran 		 	end
GO
ALTER TABLE [dbo].[DM_DTGT] ENABLE TRIGGER [DM_DTGT_DELETE]
GO
/****** Object:  Trigger [dbo].[DM_DTGT_UPDATE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
 CREATE TRIGGER [dbo].[DM_DTGT_UPDATE] ON [dbo].[DM_DTGT]  FOR UPDATE   AS IF UPDATE (MA_DTGT) 	 
 begin 		 
 begin transaction
  	update J set J.MA_DTGT = INSERTED.MA_DTGT from VTHH_CT J,INSERTED,DELETED where J.MA_DTGT = DELETED.MA_DTGT and DELETED.MA_DTGT<>''
 if(@@error<>0) 			 
 begin 				 
	 raiserror('TRIGGER_UPDATE,MA_DTGT',16,1) rollback transaction 			 
 end 		  
else 			 
	 commit transaction 	
end

GO
ALTER TABLE [dbo].[DM_DTGT] ENABLE TRIGGER [DM_DTGT_UPDATE]
GO
/****** Object:  Trigger [dbo].[DM_DTPN_DELETE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_DTPN_DELETE] ON [dbo].[DM_DTPN] FOR DELETE 
AS 	if 
(
	exists(select 'true' from DELETED I,VTHH J where I.MA_DTPN = J.MA_DTPN and I.MA_DTPN <> '')
)	
	begin 			
	raiserror('TRIGGER_DELETE',16,1) rollback tran 		
	end
GO
ALTER TABLE [dbo].[DM_DTPN] ENABLE TRIGGER [DM_DTPN_DELETE]
GO
/****** Object:  Trigger [dbo].[DM_DTPN_UPDATE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_DTPN_UPDATE] ON [dbo].[DM_DTPN]  FOR UPDATE 
 AS IF UPDATE (MA_DTPN) 	
 begin 		
 begin transaction
 	update J set J.MA_DTPN = INSERTED.MA_DTPN from VTHH J,INSERTED,DELETED where J.MA_DTPN = DELETED.MA_DTPN and DELETED.MA_DTPN<>''
  if(@@error<>0) 			
 begin 				
 raiserror('TRIGGER_UPDATE,MA_DTPN',16,1) rollback transaction 			
 end 		
 else 			
 commit transaction 	end
GO
ALTER TABLE [dbo].[DM_DTPN] ENABLE TRIGGER [DM_DTPN_UPDATE]
GO
/****** Object:  Trigger [dbo].[DM_KHO_DELETE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_KHO_DELETE] ON [dbo].[DM_KHO] FOR DELETE 
AS 	if 
(
	exists(select 'true' from DELETED I,VTHH J where I.MA_KHO = J.MA_KHO and I.MA_KHO <> '')
)	
	begin 			
	raiserror('TRIGGER_DELETE',16,1) rollback tran 		
	end

GO
ALTER TABLE [dbo].[DM_KHO] ENABLE TRIGGER [DM_KHO_DELETE]
GO
/****** Object:  Trigger [dbo].[DM_KHO_UPDATE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_KHO_UPDATE] ON [dbo].[DM_KHO]  FOR UPDATE 
 AS IF UPDATE (MA_KHO) 	
 begin 		
 begin transaction
 	update J set J.MA_KHO = INSERTED.MA_KHO from VTHH J,INSERTED,DELETED where J.MA_KHO = DELETED.MA_KHO and DELETED.MA_KHO<>''
  if(@@error<>0) 			
 begin 				
 raiserror('TRIGGER_UPDATE,MA_KHO',16,1) rollback transaction 			
 end 		
 else 			
 commit transaction 	end

GO
ALTER TABLE [dbo].[DM_KHO] ENABLE TRIGGER [DM_KHO_UPDATE]
GO
/****** Object:  Trigger [dbo].[DM_PTNX_DELETE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_PTNX_DELETE] ON [dbo].[DM_PTNX] FOR DELETE 
AS 	if 
(
	exists(select 'true' from DELETED I,VTHH J where I.MA_PTNX = J.MA_PTNX and I.MA_PTNX <> '')
)	
	begin 			
	raiserror('TRIGGER_DELETE',16,1) rollback tran 		
	end
GO
ALTER TABLE [dbo].[DM_PTNX] ENABLE TRIGGER [DM_PTNX_DELETE]
GO
/****** Object:  Trigger [dbo].[DM_PTNX_UPDATE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_PTNX_UPDATE] ON [dbo].[DM_PTNX]  FOR UPDATE 
 AS IF UPDATE (MA_PTNX) 	
 begin 		
 begin transaction
 	update J set J.MA_PTNX = INSERTED.MA_PTNX from VTHH J,INSERTED,DELETED where J.MA_PTNX = DELETED.MA_PTNX and DELETED.MA_PTNX<>'' 	
  if(@@error<>0) 			
 begin 				
 raiserror('TRIGGER_UPDATE,MA_PTNX',16,1) rollback transaction 			
 end 		
 else 			
 commit transaction 	end
GO
ALTER TABLE [dbo].[DM_PTNX] ENABLE TRIGGER [DM_PTNX_UPDATE]
GO
/****** Object:  Trigger [dbo].[DM_VTHH_DELETE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_VTHH_DELETE] ON [dbo].[DM_VTHH] FOR DELETE 
AS 	if 
(
	exists(select 'true' from DELETED I,VTHH_CT J where I.MA_VTHH = J.MA_VTHH and I.MA_VTHH <> '')
	or exists(select 'true' from DELETED I,DU_VTHH J where I.MA_VTHH = J.MA_VTHH and I.MA_VTHH <> '')
)	
	begin 			
	raiserror('TRIGGER_DELETE',16,1) rollback tran 		
	end

GO
ALTER TABLE [dbo].[DM_VTHH] ENABLE TRIGGER [DM_VTHH_DELETE]
GO
/****** Object:  Trigger [dbo].[DM_VTHH_UPDATE]    Script Date: 1/13/2022 8:52:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DM_VTHH_UPDATE] ON [dbo].[DM_VTHH]  FOR UPDATE 
 AS IF UPDATE (MA_VTHH) 	
 begin 		
 begin transaction
 	update J set J.MA_VTHH = INSERTED.MA_VTHH from VTHH_CT J,INSERTED,DELETED where J.MA_VTHH = DELETED.MA_VTHH and DELETED.MA_VTHH<>''
 	update J set J.MA_VTHH = INSERTED.MA_VTHH from DU_VTHH J,INSERTED,DELETED where J.MA_VTHH = DELETED.MA_VTHH and DELETED.MA_VTHH<>''
  if(@@error<>0) 			
 begin 				
 raiserror('TRIGGER_UPDATE,MA_VTHH',16,1) rollback transaction 			
 end 		
 else 			
 commit transaction 	end

GO
ALTER TABLE [dbo].[DM_VTHH] ENABLE TRIGGER [DM_VTHH_UPDATE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DU_VTHH"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 164
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_VTHH"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_KHO"
            Begin Extent = 
               Top = 113
               Left = 475
               Bottom = 203
               Right = 635
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_NHOM_VTHH"
            Begin Extent = 
               Top = 6
               Left = 644
               Bottom = 111
               Right = 820
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DU_VTHH_VIEW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DU_VTHH_VIEW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DU_VTHH_VIEW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DM_VTHH"
            Begin Extent = 
               Top = 96
               Left = 223
               Bottom = 215
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_DTGT"
            Begin Extent = 
               Top = 2
               Left = 770
               Bottom = 106
               Right = 942
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_DTPN"
            Begin Extent = 
               Top = 31
               Left = 540
               Bottom = 150
               Right = 712
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "VTHH_CT"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "VTHH"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 178
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "DM_KHO"
            Begin Extent = 
               Top = 54
               Left = 836
               Bottom = 143
               Right = 996
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_PTNX"
            Begin Extent = 
               Top = 9
               Left = 392
               Bottom = 113
               Right = 552
            End
            DisplayFlags = 280
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTHH_VIEW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      TopColumn = 0
         End
         Begin Table = "DM_NHOM_VTHH"
            Begin Extent = 
               Top = 168
               Left = 451
               Bottom = 273
               Right = 627
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_NHOM_DTPN"
            Begin Extent = 
               Top = 188
               Left = 26
               Bottom = 278
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_NHOM_DTGT"
            Begin Extent = 
               Top = 198
               Left = 647
               Bottom = 288
               Right = 823
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1740
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTHH_VIEW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTHH_VIEW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "VTHH"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_DTPN"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_KHO"
            Begin Extent = 
               Top = 6
               Left = 446
               Bottom = 95
               Right = 606
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DM_PTNX"
            Begin Extent = 
               Top = 6
               Left = 644
               Bottom = 110
               Right = 804
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
En' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTHH_VIEW_HEADER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'd
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTHH_VIEW_HEADER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTHH_VIEW_HEADER'
GO
USE [master]
GO
ALTER DATABASE [Hang_hoa] SET  READ_WRITE 
GO
