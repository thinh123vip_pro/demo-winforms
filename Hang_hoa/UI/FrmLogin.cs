﻿using System;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.UI
{
    public partial class FrmLogin : CnForm
    {
        CnMain cnMain;
        public FrmLogin(CnMain cnmain)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.txtUser_Id.Text = "admin";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string user_id = this.txtUser_Id.Text;
                string password = this.txtPassword.Text;

                var _password = this.cnMain.ExecuteScalar("SELECT PASSWORD FROM DM_USER WHERE USER_ID = '"+user_id+"'");
                if(_password == null || _password == DBNull.Value)
                {
                    MessageBox.Show("Tên đăng nhập không tồn tại");
                    this.txtUser_Id.Focus();
                    return;
                }
                else if(password != _password.ToString())
                {
                    MessageBox.Show("Mật khẩu sai");
                    this.txtPassword.Focus();
                    return;
                }
                this.cnMain.User_Id = user_id.ToUpper();
                this.cnMain.User_Password = password;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Return)
            {
                SendKeys.Send("{TAB}");
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
