﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public class Rpt_Nxt : Rpt_Report
    {
        public Rpt_Nxt(CnMain cnmain, KySlieu kyslieu, string dieu_kien, DataRow rowReport)
            : base(cnmain, kyslieu, dieu_kien, rowReport)
        {
            string query = "SELECT * FROM VTHH_TOTAL WHERE NGAY_CTU >= " + Functions.ParseDate(this.cnMain.Day_Start) + " AND NGAY_CTU <= " + Functions.ParseDate(kyslieu.Den_Ngay) + " AND " + this.Dieu_kien_loc;
            this.cnMain.LoadDatatableQuery(query, this.Dataset, this.Table_name);
            DataTable dt = this.Dataset.Tables[this.Table_name];
            DataColumn c;
            c = new DataColumn("DKY_LUONG",typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("DKY_GIA",typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("DKY_TIEN", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);

            c = new DataColumn("NHAP_LUONG", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("NHAP_GIA", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("NHAP_TIEN", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);

            c = new DataColumn("XUAT_LUONG", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("XUAT_GIA", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("XUAT_TIEN", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);

            c = new DataColumn("CKY_LUONG", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("CKY_GIA", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);
            c = new DataColumn("CKY_TIEN", typeof(System.Decimal));
            c.DefaultValue = 0;
            dt.Columns.Add(c);

            foreach(DataRow r in dt.Rows)
            {
                string nx = r["NX"].ToString();
                DateTime ngay_ctu = (DateTime)r["NGAY_CTU"];
                if(nx == "DU" || (nx == "N" && ngay_ctu<this.Kyslieu.Tu_Ngay))
                {
                    r["DKY_LUONG"] = r["SO_LUONG"];
                    r["DKY_TIEN"] = r["TIEN_VON"];
                }
                else if(nx == "X" && ngay_ctu<this.Kyslieu.Tu_Ngay)
                {
                    r["DKY_LUONG"] = (decimal)r["SO_LUONG"] * (-1);
                    r["DKY_TIEN"] = (decimal)r["TIEN_VON"] * (-1);
                }
                else if (nx == "N" && ngay_ctu >= this.Kyslieu.Tu_Ngay)
                {
                    r["NHAP_LUONG"] = (decimal)r["SO_LUONG"];
                    r["NHAP_TIEN"] = (decimal)r["TIEN_VON"];
                }
                else if (nx == "X" && ngay_ctu >= this.Kyslieu.Tu_Ngay)
                {
                    r["XUAT_LUONG"] = (decimal)r["SO_LUONG"];
                    r["XUAT_TIEN"] = (decimal)r["TIEN_VON"];
                }
                
            }

            DataTable dtCopy = dt.Copy();
            DataView dv = new DataView(dtCopy, "", "MA_KHO,MA_VTHH", DataViewRowState.CurrentRows);
            dt.Clear();
            dt.PrimaryKey = new DataColumn[] { dt.Columns["MA_KHO"], dt.Columns["MA_VTHH"] };
            string ma_kho = "", ma_vthh = "";
            foreach (DataRowView rv in dv)
            {
                if (rv["MA_KHO"].ToString() != ma_kho || rv["MA_VTHH"].ToString() != ma_vthh)
                {
                    DataRow fr = dt.Rows.Find(new object[2] { rv["MA_KHO"], rv["MA_VTHH"] });
                    if(fr == null)
                    {
                        DataRow nr = dt.NewRow();
                        nr.ItemArray = (object[])rv.Row.ItemArray.Clone();
                        dt.Rows.Add(nr);
                    }
                    else
                    {
                        fr["DKY_LUONG"] = (decimal)fr["DKY_LUONG"] + (decimal)rv["DKY_LUONG"];
                        fr["DKY_TIEN"] = (decimal)fr["DKY_TIEN"] + (decimal)rv["DKY_TIEN"];
                        fr["NHAP_LUONG"] = (decimal)fr["NHAP_LUONG"] + (decimal)rv["NHAP_LUONG"];
                        fr["NHAP_TIEN"] = (decimal)fr["NHAP_TIEN"] + (decimal)rv["NHAP_TIEN"];
                        fr["XUAT_LUONG"] = (decimal)fr["XUAT_LUONG"] + (decimal)rv["XUAT_LUONG"];
                        fr["XUAT_TIEN"] = (decimal)fr["XUAT_TIEN"] + (decimal)rv["XUAT_TIEN"];
                    }
                }
            }
            foreach(DataRow r in dt.Rows)
            {
                r["CKY_LUONG"] = (decimal)r["DKY_LUONG"] + (decimal)r["NHAP_LUONG"] - (decimal)r["XUAT_LUONG"];
                r["CKY_TIEN"] = (decimal)r["DKY_TIEN"] + (decimal)r["NHAP_TIEN"] - (decimal)r["XUAT_TIEN"];
                if ((decimal)r["DKY_LUONG"] != 0) r["DKY_GIA"] = Math.Round((decimal)r["DKY_TIEN"] / (decimal)r["DKY_LUONG"],2);
                if ((decimal)r["NHAP_LUONG"] != 0) r["NHAP_GIA"] = Math.Round((decimal)r["NHAP_TIEN"] / (decimal)r["NHAP_LUONG"], 2);
                if ((decimal)r["XUAT_LUONG"] != 0) r["XUAT_GIA"] = Math.Round((decimal)r["XUAT_TIEN"] / (decimal)r["XUAT_LUONG"], 2);
                if ((decimal)r["CKY_LUONG"] != 0) r["CKY_GIA"] = Math.Round((decimal)r["CKY_TIEN"] / (decimal)r["CKY_LUONG"], 2);
            }
        }
    }
}
