﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.UI
{
    public partial class FrmChangePassword : CnForm
    {
        CnMain cnMain;
        public FrmChangePassword(CnMain cnmain)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.MdiParent = this.cnMain.mainForm;
            this.txtUser_Id.Text = this.cnMain.User_Id;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string passwordNew = this.txtPasswordNew.Text;
                if(this.txtPasswordOld.Text != this.cnMain.User_Password)
                {
                    MessageBox.Show("Mật khẩu cũ không hợp lệ");
                    this.txtPasswordOld.Focus();
                    return;
                }
                else
                {
                    if(passwordNew == this.txtPasswordOld.Text)
                    {
                        MessageBox.Show("Bạn nhập mật khẩu mới giống với mật khẩu cũ");
                        this.txtPasswordNew.Focus();
                        return;
                    }
                    else if(this.txtPasswordNew.Text != this.txtPasswordNewConfirm.Text)
                    {
                        this.txtPasswordNewConfirm.Focus();
                        MessageBox.Show("Xác nhận mật khẩu không khớp với mật khẩu mới");
                        return;
                    }
                }
                string query = "UPDATE DM_USER SET PASSWORD = '" + passwordNew + "' WHERE USER_ID = '" + this.cnMain.User_Id + "'";
                this.cnMain.ExecuteNonQuery(query);
                this.cnMain.User_Password = this.txtPasswordNew.Text;
                MessageBox.Show("Bạn đã đổi mật khẩu thành công");
                this.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Return)
            {
                SendKeys.Send("{TAB}");
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
