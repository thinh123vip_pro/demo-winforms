﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CnMain cnMain = new CnMain();
            cnMain.Run();
            FrmLogin frm = new FrmLogin(cnMain);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new FrmMain(cnMain));
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
