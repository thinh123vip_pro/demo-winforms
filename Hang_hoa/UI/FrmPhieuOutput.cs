﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public partial class FrmPhieuOutput : CnForm
    {
        CnMain cnMain;
        DataSet Dataset;
        string fileName = "";
        public FrmPhieuOutput(CnMain cnmain, DataSet dataset, string fileName)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.Dataset = dataset;
            this.fileName = fileName;
            this.MdiParent = cnmain.mainForm;
            this.LoadReport();
        }

        private void LoadReport()
        {
            ReportDataSource rds = new ReportDataSource("VTHH_CT", this.Dataset.Tables["VTHH_CT"]);
            rds.Name = "Dataset";
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            string file = Application.StartupPath + "\\Templates\\" + this.fileName;
            if (!System.IO.File.Exists(file))
            {
                throw new Exception("File không tồn tại: " + file);
            }
            this.reportViewer1.LocalReport.ReportPath = file;
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            this.reportViewer1.ZoomMode = ZoomMode.Percent;
            this.reportViewer1.ZoomPercent = 75;
            ReportParameterInfoCollection parainfos = this.reportViewer1.LocalReport.GetParameters();
            ReportParameterCollection paras = new ReportParameterCollection();
            ReportParameter para;
            DataRow rowVthh = this.Dataset.Tables["VTHH"].Rows[0];
            if (this.CheckExistsParameter(parainfos, "company_name"))
            {
                para = new ReportParameter("company_name", this.cnMain.GetSystemVar("company_name").ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "so_ctu"))
            {
                para = new ReportParameter("so_ctu", rowVthh["SO_CTU"].ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ngay_ctu"))
            {
                para = new ReportParameter("ngay_ctu", ((DateTime)rowVthh["NGAY_CTU"]).ToString("dd/MM/yyyy"));
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ngay_ctu_full"))
            {
                para = new ReportParameter("ngay_ctu_full", ((DateTime)rowVthh["NGAY_CTU"]).ToString("'Ngày' dd 'tháng' MM 'năm' yyyy"));
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "so_hdon"))
            {
                para = new ReportParameter("so_hdon", rowVthh["so_hdon"].ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ngay_hdon"))
            {
                para = new ReportParameter("ngay_hdon", rowVthh["ngay_hdon"].ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ho_ten"))
            {
                para = new ReportParameter("ho_ten", rowVthh["ho_ten"].ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ma_dtpn"))
            {
                para = new ReportParameter("ma_dtpn", rowVthh["ma_dtpn"].ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ten_dtpn"))
            {
                para = new ReportParameter("ten_dtpn", this.cnMain.GetFieldValue(this.Dataset.Tables["DM_DTPN"], rowVthh["ma_dtpn"].ToString(),"TEN_DTPN").ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ma_kho"))
            {
                para = new ReportParameter("ma_kho", rowVthh["ma_kho"].ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ten_kho"))
            {
                para = new ReportParameter("ten_kho", this.cnMain.GetFieldValue(this.Dataset.Tables["DM_KHO"], rowVthh["ma_kho"].ToString(), "TEN_KHO").ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "dien_giai"))
            {
                para = new ReportParameter("dien_giai", rowVthh["dien_giai"].ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "doc_tong_tien"))
            {
                double tong_tien = 0;
                object oj = this.Dataset.Tables["VTHH_CT"].Compute("Sum(TONG_TIEN)", "");
                if(oj != null && oj != DBNull.Value)
                {
                    tong_tien = Convert.ToDouble(oj);
                }
                para = new ReportParameter("doc_tong_tien", Functions.DocSo(tong_tien));
                paras.Add(para);
            }
            this.reportViewer1.LocalReport.SetParameters(paras);
            this.reportViewer1.RefreshReport();

        }
        private bool CheckExistsParameter(ReportParameterInfoCollection parainfos, string paraname)
        {
            foreach(ReportParameterInfo parainfo in parainfos)
            {
                if(parainfo.Name == paraname)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
