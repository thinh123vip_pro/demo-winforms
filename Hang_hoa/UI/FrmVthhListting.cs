﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public partial class FrmVthhListing : CnForm
    {
        CnMain cnMain;
        string Table_Name_Header = "VTHH_VIEW_HEADER";
        string Table_Name_Ct = "VTHH_VIEW";
        DataTable Datatable_Header;
        DataTable Datatable_ct;
        DataSet Dataset;
        SqlDataAdapter Dataadapter;
        public FrmVthhListing(CnMain cnmain)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.MdiParent = this.cnMain.mainForm;
            this.LoadData();
            this.SetControls();
            this.BinGrid();
            this.ConfigGrid();
        }
        private void LoadData()
        {
            this.Dataset = new DataSet();

            this.cnMain.LoadDatatable(this.Dataset, "DM_KHO");
            this.cnMain.LoadDatatable(this.Dataset, "DM_PTNX");
            this.cnMain.LoadDatatable(this.Dataset, "DM_DTPN");
            this.cnMain.LoadDatatable(this.Dataset, "DM_NHOM_DTPN");
            this.cnMain.LoadDatatable(this.Dataset, "DM_VTHH");
            this.cnMain.LoadDatatable(this.Dataset, "DM_NHOM_VTHH");
            this.cnMain.LoadDatatable(this.Dataset, "DM_DTGT");
            this.cnMain.LoadDatatable(this.Dataset, "DM_NHOM_DTGT");

            this.Dataadapter = new SqlDataAdapter("SELECT * FROM " + this.Table_Name_Header + " WHERE 1=0", this.cnMain.sqlConnection);
            this.Dataadapter.Fill(this.Dataset, this.Table_Name_Header);
            this.Datatable_Header = this.Dataset.Tables[this.Table_Name_Header];

            this.Dataadapter = new SqlDataAdapter("SELECT * FROM " + this.Table_Name_Ct + " WHERE 1=0", this.cnMain.sqlConnection);
            this.Dataadapter.Fill(this.Dataset, this.Table_Name_Ct);
            this.Datatable_ct = this.Dataset.Tables[this.Table_Name_Ct];

        }
        private void BinGrid()
        {
            this.gridHeader.AutoGenerateColumns = false;
            this.gridHeader.DataSource = this.Dataset;
            this.gridHeader.DataMember = this.Table_Name_Header;
            Functions.SetGridTextColumn(this.gridHeader, "SO_CTU", "Số c.từ", 100);
            Functions.SetGridDateColumn(this.gridHeader, "NGAY_CTU", "Ngày c.từ", 100);
            Functions.SetGridTextColumn(this.gridHeader, "SO_HDON", "Số h.đơn", 100);
            Functions.SetGridDateColumn(this.gridHeader, "NGAY_HDON", "Ngày h.đơn", 100);
            Functions.SetGridTextColumn(this.gridHeader, "MA_KHO", "Mã kho", 100);
            Functions.SetGridTextColumn(this.gridHeader, "TEN_KHO", "Tên kho", 200);
            Functions.SetGridTextColumn(this.gridHeader, "MA_PTNX", "Mã ptnx", 100);
            Functions.SetGridTextColumn(this.gridHeader, "TEN_PTNX", "Tên ptnx", 200);
            Functions.SetGridTextColumn(this.gridHeader, "MA_DTPN", "Mã đtpn", 100);
            Functions.SetGridTextColumn(this.gridHeader, "TEN_DTPN", "Tên đtpn", 200);
            Functions.SetGridTextColumn(this.gridHeader, "MA_NHOM_DTPN", "Mã nhóm đtpn", 100);
            Functions.SetGridTextColumn(this.gridHeader, "DIEN_GIAI", "Diễn giải", 200);
            Functions.SetGridTextColumn(this.gridHeader, "NX", "", 50);
            Functions.SetGridNumberColumn(this.gridHeader, "PK", "", 50, this.Datatable_Header.Columns["PK"].DataType, 0);
            
            this.gridCt.AutoGenerateColumns = false;
            this.gridCt.DataSource = this.Dataset;
            this.gridCt.DataMember = this.Table_Name_Ct;
            Functions.SetGridTextColumn(this.gridCt, "SO_CTU", "Số c.từ", 100);
            Functions.SetGridDateColumn(this.gridCt, "NGAY_CTU", "Ngày c.từ", 100);
            Functions.SetGridTextColumn(this.gridCt, "SO_HDON", "Số h.đơn", 100);
            Functions.SetGridDateColumn(this.gridCt, "NGAY_HDON", "Ngày h.đơn", 100);
            Functions.SetGridTextColumn(this.gridCt, "MA_KHO", "Mã kho", 100);
            //Functions.SetGridTextColumn(this.gridCt, "TEN_KHO", "Tên kho", 200);
            Functions.SetGridTextColumn(this.gridCt, "MA_PTNX", "Mã ptnx", 100);
            //Functions.SetGridTextColumn(this.gridCt, "TEN_PTNX", "Tên ptnx", 200);
            Functions.SetGridTextColumn(this.gridCt, "MA_DTPN", "Mã đtpn", 100);
            Functions.SetGridTextColumn(this.gridCt, "TEN_DTPN", "Tên đtpn", 200);
            Functions.SetGridTextColumn(this.gridCt, "MA_NHOM_DTPN", "Mã nhóm đtpn", 100);
            Functions.SetGridTextColumn(this.gridCt, "DIEN_GIAI", "Diễn giải", 200);
            Functions.SetGridTextColumn(this.gridCt, "MA_VTHH", "Mã vthh", 100);
            Functions.SetGridTextColumn(this.gridCt, "TEN_VTHH", "Tên vthh", 200);
            Functions.SetGridTextColumn(this.gridCt, "MA_NHOM_VTHH", "Nhóm vthh", 100);
            Functions.SetGridTextColumn(this.gridCt, "DVT", "Đvt", 50);
            Functions.SetGridNumberColumn(this.gridCt, "SO_LUONG", "Số lượng", 100, this.Datatable_ct.Columns["SO_LUONG"].DataType, 2);
            Functions.SetGridNumberColumn(this.gridCt, "GIA_GOC", "Giá góc", 100, this.Datatable_ct.Columns["GIA_GOC"].DataType, 2);
            Functions.SetGridNumberColumn(this.gridCt, "TIEN_GOC", "Tiền gốc", 100, this.Datatable_ct.Columns["TIEN_GOC"].DataType, 0);
            Functions.SetGridNumberColumn(this.gridCt, "THUE_SUAT_VAT", "Thuế suất vat", 100, this.Datatable_ct.Columns["THUE_SUAT_VAT"].DataType, 2);
            Functions.SetGridNumberColumn(this.gridCt, "TIEN_THUE_VAT", "Tiền thuế vat", 100, this.Datatable_ct.Columns["TIEN_THUE_VAT"].DataType, 0);
            Functions.SetGridNumberColumn(this.gridCt, "TONG_TIEN", "Tổng tiền", 100, this.Datatable_ct.Columns["TONG_TIEN"].DataType, 0);
            Functions.SetGridNumberColumn(this.gridCt, "GIA_VON", "Giá vốn", 100, this.Datatable_ct.Columns["GIA_VON"].DataType, 2);
            Functions.SetGridNumberColumn(this.gridCt, "TIEN_VON", "Tiền vốn", 100, this.Datatable_ct.Columns["TIEN_VON"].DataType, 0);
            Functions.SetGridTextColumn(this.gridCt, "MA_DTGT", "Mã Đtgt", 100);
            Functions.SetGridTextColumn(this.gridCt, "TEN_DTGT", "Tên Đtgt", 200);
            Functions.SetGridTextColumn(this.gridCt, "MA_NHOM_DTGT", "Nhóm Đtgt", 100);
            Functions.SetGridTextColumn(this.gridCt, "GHI_CHU", "Ghi chú", 200);
            Functions.SetGridTextColumn(this.gridCt, "NX", "", 50);
        }
        
        private void ConfigGrid()
        {
            this.gridHeader.AllowUserToAddRows = false;
            this.gridHeader.ReadOnly = true;
            this.gridCt.AllowUserToAddRows = true;
            this.gridCt.ReadOnly = true;
            this.gridCt.CellFormatting += grid_CellFormatting;
            this.gridHeader.CellDoubleClick += gridHeader_CellDoubleClick;
            this.gridCt.CellDoubleClick += gridCt_CellDoubleClick;
            this.gridHeader.RowPostPaint += grid_RowPostPaint;
            this.gridCt.RowPostPaint += grid_RowPostPaint;
            this.gridHeader.CurrentCellChanged += grid_CurrentCellChanged;
            this.gridCt.CurrentCellChanged += grid_CurrentCellChanged;
        }

        void grid_CurrentCellChanged(object sender, System.EventArgs e)
        {
            try
            {
                DataGridView gr = sender as DataGridView;
                if (gr.CurrentCell != null)
                {
                    string columnname = gr.CurrentCell.OwningColumn.Name;
                    switch (columnname)
                    {
                        case "MA_KHO":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_KHO"], gr.CurrentCell.Value.ToString(), "TEN_KHO").ToString();
                            break;
                        case "MA_PTNX":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_PTNX"], gr.CurrentCell.Value.ToString(), "TEN_PTNX").ToString();
                            break;
                        case "MA_NHOM_DTPN":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_NHOM_DTPN"], gr.CurrentCell.Value.ToString(), "TEN_NHOM_DTPN").ToString();
                            break;
                        case "MA_NHOM_VTHH":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_NHOM_VTHH"], gr.CurrentCell.Value.ToString(), "TEN_NHOM_VTHH").ToString();
                            break;
                        case "MA_NHOM_DTGT":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_NHOM_DTGT"], gr.CurrentCell.Value.ToString(), "TEN_NHOM_DTGT").ToString();
                            break;
                        default:
                            this.toolStripMsg.Text = "";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void grid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                var grid = sender as DataGridView;
                var rowIdx = (e.RowIndex + 1).ToString();

                var centerFormat = new StringFormat()
                {
                    // right alignment might actually make more sense for numbers
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };

                var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
                e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }

        void gridCt_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.ShowCtu();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }

        void gridHeader_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.ShowCtu();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }


        void grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.RowIndex == this.gridCt.NewRowIndex)
                {
                    if (this.gridCt.Columns[e.ColumnIndex].ValueType == typeof(System.Decimal))
                    {
                        e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                        e.CellStyle.ForeColor = Color.Blue;
                        decimal sum = 0;
                        for (int i = 0; i < this.gridCt.NewRowIndex; i++)
                        {
                            var value = this.gridCt[2, i].Value;
                            sum += (decimal)this.gridCt[e.ColumnIndex, i].Value;
                        }
                        e.Value = Math.Round(sum, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SetControls()
        {
            List<KySlieu> kyslieulist = Functions.LoadKySlieu((int)this.cnMain.GetSystemVar("WORKING_YEAR"), true);
            foreach (KySlieu kyslieu in kyslieulist)
            {
                this.cboKySlieu.Items.Add(kyslieu);
            }
            this.cboKySlieu.SelectedIndex = 0;
            this.cboNX.Items.Add("N");
            this.cboNX.Items.Add("X");
            this.cboNX.SelectedIndex = 0;
            this.splitContainer1.FixedPanel = FixedPanel.Panel1;
        }
        private void Filter()
        {
            string dk = " NX = '" + this.cboNX.SelectedItem.ToString() + "'";
            if (this.datTu_Ngay.Value != null)
            {
                dk += " AND NGAY_CTU >= " + Functions.ParseDate(this.datTu_Ngay.Value);
            }
            if (this.datDen_Ngay.Value != null)
            {
                dk += " AND NGAY_CTU <= " + Functions.ParseDate(this.datDen_Ngay.Value);
            }
            if (this.txtSo_Ctu.Text != "")
            {
                dk += " AND SO_CTU LIKE '%" + this.txtSo_Ctu.Text + "%'";
            }
            if (this.txtSo_Hdon.Text != "")
            {
                dk += " AND SO_HDON LIKE '%" + this.txtSo_Hdon.Text + "%'";
            }
            if (this.txtMa_Kho.Text != "")
            {
                dk += " AND MA_KHO = '" + this.txtMa_Kho.Text + "'";
            }
            if (this.txtMa_Ptnx.Text != "")
            {
                dk += " AND MA_PTNX = '" + this.txtMa_Ptnx.Text + "'";
            }
            if (this.txtMa_Dtpn.Text != "")
            {
                dk += " AND MA_DTPN = '" + this.txtMa_Dtpn.Text + "'";
            }
            if (this.txtMa_Nhom_Dtpn.Text != "")
            {
                dk += " AND MA_NHOM_DTPN = '" + this.txtMa_Nhom_Dtpn.Text + "'";
            }
            if (this.tabMain.SelectedTab.Equals(this.tabHeader))
            {
                string query = "SELECT * FROM " + this.Table_Name_Header + " WHERE " + dk;
                this.Dataadapter = new SqlDataAdapter(query, this.cnMain.sqlConnection);
                this.Dataset.Tables[this.Table_Name_Header].Clear();
                this.Dataadapter.Fill(this.Dataset, this.Table_Name_Header);
            }
            else if (this.tabMain.SelectedTab.Equals(this.tabCt))
            {
                if (this.txtMa_Dtgt.Text != "")
                {
                    dk += " AND MA_DTGT = '" + this.txtMa_Dtgt.Text + "'";
                }
                if (this.txtMa_Nhom_Dtgt.Text != "")
                {
                    dk += " AND MA_NHOM_DTGT = '" + this.txtMa_Nhom_Dtgt.Text + "'";
                }
                if (this.txtMa_Vthh.Text != "")
                {
                    dk += " AND MA_VTHH = '" + this.txtMa_Vthh.Text + "'";
                }
                if (this.txtMa_Nhom_Vthh.Text != "")
                {
                    dk += " AND MA_NHOM_VTHH = '" + this.txtMa_Nhom_Vthh.Text + "'";
                }
                string query = "SELECT * FROM " + this.Table_Name_Ct + " WHERE " + dk;
                this.Dataadapter = new SqlDataAdapter(query, this.cnMain.sqlConnection);
                this.Dataset.Tables[this.Table_Name_Ct].Clear();
                this.Dataadapter.Fill(this.Dataset, this.Table_Name_Ct);
            }
        }
        private void txtMa_Leave(object sender, System.EventArgs e)
        {
            try
            {
                TextBox texbox = sender as TextBox;
                if(texbox.Equals(this.txtMa_Kho)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_KHO", texbox);
                else if (texbox.Equals(this.txtMa_Ptnx)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_PTNX", texbox);
                else if (texbox.Equals(this.txtMa_Dtpn)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_DTPN", texbox);
                else if (texbox.Equals(this.txtMa_Nhom_Dtpn)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_NHOM_DTPN", texbox);
                else if (texbox.Equals(this.txtMa_Dtgt)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_DTGT", texbox);
                else if (texbox.Equals(this.txtMa_Nhom_Dtgt)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_NHOM_DTGT", texbox);
                else if (texbox.Equals(this.txtMa_Vthh)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_VTHH", texbox);
                else if (texbox.Equals(this.txtMa_Nhom_Vthh)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_NHOM_VTHH", texbox);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ". " + ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }
        private void txtMa_DoubleClick(object sender, System.EventArgs e)
        {
            try
            {
                TextBox texbox = sender as TextBox;
                if (texbox.Equals(this.txtMa_Kho)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_KHO", texbox, true);
                else if (texbox.Equals(this.txtMa_Ptnx)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_PTNX", texbox, true);
                else if (texbox.Equals(this.txtMa_Dtpn)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_DTPN", texbox, true);
                else if (texbox.Equals(this.txtMa_Nhom_Dtpn)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_NHOM_DTPN", texbox, true);
                else if (texbox.Equals(this.txtMa_Dtgt)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_DTGT", texbox, true);
                else if (texbox.Equals(this.txtMa_Nhom_Dtgt)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_NHOM_DTGT", texbox, true);
                else if (texbox.Equals(this.txtMa_Vthh)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_VTHH", texbox, true);
                else if (texbox.Equals(this.txtMa_Nhom_Vthh)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_NHOM_VTHH", texbox, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void txtMa_TextChanged(object sender, EventArgs e)
        {
            TextBox texbox = sender as TextBox;
            if (texbox.Equals(this.txtMa_Kho)) this.lblTen_Kho.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_KHO"], this.txtMa_Kho.Text, "TEN_KHO").ToString();
            else if (texbox.Equals(this.txtMa_Ptnx)) this.lblTen_Ptnx.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_PTNX"], this.txtMa_Ptnx.Text, "TEN_PTNX").ToString();
            else if (texbox.Equals(this.txtMa_Dtpn)) this.lblTen_Dtpn.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_DTPN"], this.txtMa_Dtpn.Text, "TEN_DTPN").ToString();
            else if (texbox.Equals(this.txtMa_Nhom_Dtpn)) this.lblTen_Nhom_Dtpn.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_NHOM_DTPN"], this.txtMa_Nhom_Dtpn.Text, "TEN_NHOM_DTPN").ToString();
            else if (texbox.Equals(this.txtMa_Dtgt)) this.lblTen_Dtgt.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_DTGT"], this.txtMa_Dtgt.Text, "TEN_DTGT").ToString();
            else if (texbox.Equals(this.txtMa_Nhom_Dtgt)) this.cnMain.GetFieldValue(this.Dataset.Tables["DM_NHOM_DTGT"], this.txtMa_Nhom_Dtgt.Text, "TEN_NHOM_DTGT").ToString();
            else if (texbox.Equals(this.txtMa_Vthh)) this.lblTen_Vthh.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_VTHH"], this.txtMa_Vthh.Text, "TEN_VTHH").ToString();
            else if (texbox.Equals(this.txtMa_Nhom_Vthh)) this.lblTen_Nhom_Vthh.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_NHOM_VTHH"], this.txtMa_Nhom_Vthh.Text, "TEN_NHOM_VTHH").ToString();
        }

        private void btnFilter_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.Filter();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ". " + ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }
        private DataGridView GetGridSelected()
        {
            if(this.tabMain.SelectedTab.Equals(this.tabHeader))
            {
                return this.gridHeader;
            }
            else if (this.tabMain.SelectedTab.Equals(this.tabCt))
            {
                return this.gridCt;
            }
            return null;
        }
        private void btnXem_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.ShowCtu();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ". " + ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }
        private void ShowCtu()
        {
            DataGridView grid = this.GetGridSelected();
            if (grid.CurrentRow != null && grid.CurrentRow.DataBoundItem != null)
            {
                DataRow r = ((DataRowView)grid.CurrentRow.DataBoundItem).Row;
                decimal pk = (decimal)r["PK"];
                if (grid.Equals(this.gridCt))
                {
                    pk = (decimal)r["FK"];
                }
                string NX = r["NX"].ToString();
                FrmVthh frm = new FrmVthh(this.cnMain, NX, pk);
                frm.Show();
            }
        }

        private void cboKySlieu_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (this.cboKySlieu.SelectedIndex < 0) return;
                KySlieu kyslieu = (KySlieu)this.cboKySlieu.SelectedItem;
                this.datTu_Ngay.Value = kyslieu.Tu_Ngay;
                this.datDen_Ngay.Value = kyslieu.Den_Ngay;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
