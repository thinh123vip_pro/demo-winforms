﻿namespace Cn.UI
{
    partial class FrmReportInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReportInput));
            this.cboKySlieu = new System.Windows.Forms.ComboBox();
            this.lblDen_Ngay = new System.Windows.Forms.Label();
            this.datDen_Ngay = new System.Windows.Forms.DateTimePicker();
            this.lblKySlieu = new System.Windows.Forms.Label();
            this.lblTu_Ngay = new System.Windows.Forms.Label();
            this.datTu_Ngay = new System.Windows.Forms.DateTimePicker();
            this.gridSrc = new System.Windows.Forms.DataGridView();
            this.gridDes = new System.Windows.Forms.DataGridView();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnXem = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblReportName = new System.Windows.Forms.Label();
            this.lblTable = new System.Windows.Forms.Label();
            this.cboTable = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridSrc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDes)).BeginInit();
            this.SuspendLayout();
            // 
            // cboKySlieu
            // 
            this.cboKySlieu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKySlieu.FormattingEnabled = true;
            this.cboKySlieu.Location = new System.Drawing.Point(99, 62);
            this.cboKySlieu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboKySlieu.Name = "cboKySlieu";
            this.cboKySlieu.Size = new System.Drawing.Size(332, 24);
            this.cboKySlieu.TabIndex = 7;
            this.cboKySlieu.SelectedIndexChanged += new System.EventHandler(this.cboKySlieu_SelectedIndexChanged);
            // 
            // lblDen_Ngay
            // 
            this.lblDen_Ngay.AutoSize = true;
            this.lblDen_Ngay.Location = new System.Drawing.Point(709, 65);
            this.lblDen_Ngay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDen_Ngay.Name = "lblDen_Ngay";
            this.lblDen_Ngay.Size = new System.Drawing.Size(30, 16);
            this.lblDen_Ngay.TabIndex = 10;
            this.lblDen_Ngay.Text = "đến";
            // 
            // datDen_Ngay
            // 
            this.datDen_Ngay.CustomFormat = "dd/MM/yyyy";
            this.datDen_Ngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datDen_Ngay.Location = new System.Drawing.Point(773, 63);
            this.datDen_Ngay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.datDen_Ngay.Name = "datDen_Ngay";
            this.datDen_Ngay.Size = new System.Drawing.Size(131, 22);
            this.datDen_Ngay.TabIndex = 9;
            // 
            // lblKySlieu
            // 
            this.lblKySlieu.AutoSize = true;
            this.lblKySlieu.Location = new System.Drawing.Point(16, 66);
            this.lblKySlieu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKySlieu.Name = "lblKySlieu";
            this.lblKySlieu.Size = new System.Drawing.Size(56, 16);
            this.lblKySlieu.TabIndex = 11;
            this.lblKySlieu.Text = "Kỳ s.liệu";
            // 
            // lblTu_Ngay
            // 
            this.lblTu_Ngay.AutoSize = true;
            this.lblTu_Ngay.Location = new System.Drawing.Point(465, 66);
            this.lblTu_Ngay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTu_Ngay.Name = "lblTu_Ngay";
            this.lblTu_Ngay.Size = new System.Drawing.Size(56, 16);
            this.lblTu_Ngay.TabIndex = 12;
            this.lblTu_Ngay.Text = "Từ ngày";
            // 
            // datTu_Ngay
            // 
            this.datTu_Ngay.CustomFormat = "dd/MM/yyyy";
            this.datTu_Ngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datTu_Ngay.Location = new System.Drawing.Point(552, 63);
            this.datTu_Ngay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.datTu_Ngay.Name = "datTu_Ngay";
            this.datTu_Ngay.Size = new System.Drawing.Size(131, 22);
            this.datTu_Ngay.TabIndex = 8;
            // 
            // gridSrc
            // 
            this.gridSrc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSrc.Location = new System.Drawing.Point(20, 101);
            this.gridSrc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridSrc.Name = "gridSrc";
            this.gridSrc.RowHeadersWidth = 51;
            this.gridSrc.Size = new System.Drawing.Size(412, 366);
            this.gridSrc.TabIndex = 13;
            // 
            // gridDes
            // 
            this.gridDes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDes.Location = new System.Drawing.Point(493, 101);
            this.gridDes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridDes.Name = "gridDes";
            this.gridDes.RowHeadersWidth = 51;
            this.gridDes.Size = new System.Drawing.Size(412, 366);
            this.gridDes.TabIndex = 13;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(440, 213);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(45, 38);
            this.btnSelectAll.TabIndex = 14;
            this.btnSelectAll.Text = ">>";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(440, 299);
            this.btnRemoveAll.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(45, 38);
            this.btnRemoveAll.TabIndex = 14;
            this.btnRemoveAll.Text = "<<";
            this.btnRemoveAll.UseVisualStyleBackColor = true;
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnXem
            // 
            this.btnXem.Location = new System.Drawing.Point(552, 474);
            this.btnXem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(105, 47);
            this.btnXem.TabIndex = 14;
            this.btnXem.Text = "Xem";
            this.btnXem.UseVisualStyleBackColor = true;
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(743, 474);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(101, 47);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Thoát";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblReportName
            // 
            this.lblReportName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReportName.Location = new System.Drawing.Point(20, 11);
            this.lblReportName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReportName.Name = "lblReportName";
            this.lblReportName.Size = new System.Drawing.Size(885, 34);
            this.lblReportName.TabIndex = 11;
            this.lblReportName.Text = "Tên báo cáo";
            this.lblReportName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTable
            // 
            this.lblTable.AutoSize = true;
            this.lblTable.Location = new System.Drawing.Point(16, 491);
            this.lblTable.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTable.Name = "lblTable";
            this.lblTable.Size = new System.Drawing.Size(67, 16);
            this.lblTable.TabIndex = 11;
            this.lblTable.Text = "Danh mục";
            // 
            // cboTable
            // 
            this.cboTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTable.FormattingEnabled = true;
            this.cboTable.Location = new System.Drawing.Point(99, 486);
            this.cboTable.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboTable.Name = "cboTable";
            this.cboTable.Size = new System.Drawing.Size(332, 24);
            this.cboTable.TabIndex = 7;
            this.cboTable.SelectedIndexChanged += new System.EventHandler(this.cboTable_SelectedIndexChanged);
            // 
            // FrmReportInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 535);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnXem);
            this.Controls.Add(this.btnRemoveAll);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.gridDes);
            this.Controls.Add(this.gridSrc);
            this.Controls.Add(this.cboTable);
            this.Controls.Add(this.cboKySlieu);
            this.Controls.Add(this.lblDen_Ngay);
            this.Controls.Add(this.datDen_Ngay);
            this.Controls.Add(this.lblTable);
            this.Controls.Add(this.lblReportName);
            this.Controls.Add(this.lblKySlieu);
            this.Controls.Add(this.lblTu_Ngay);
            this.Controls.Add(this.datTu_Ngay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmReportInput";
            this.Text = "FrmReportInput";
            ((System.ComponentModel.ISupportInitialize)(this.gridSrc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboKySlieu;
        private System.Windows.Forms.Label lblDen_Ngay;
        private System.Windows.Forms.DateTimePicker datDen_Ngay;
        private System.Windows.Forms.Label lblKySlieu;
        private System.Windows.Forms.Label lblTu_Ngay;
        private System.Windows.Forms.DateTimePicker datTu_Ngay;
        private System.Windows.Forms.DataGridView gridSrc;
        private System.Windows.Forms.DataGridView gridDes;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnXem;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblReportName;
        private System.Windows.Forms.Label lblTable;
        private System.Windows.Forms.ComboBox cboTable;
    }
}