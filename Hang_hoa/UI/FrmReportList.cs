﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public partial class FrmReportList : CnForm
    {
        CnMain cnMain;
        DataTable dtDm_Report;
        public FrmReportList(CnMain cnmain)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.MdiParent = this.cnMain.mainForm;
            this.LoadList();
        }
        private void LoadList()
        {
            this.dtDm_Report = this.cnMain.LoadDatatable("DM_REPORT","1=1 order by STT");
            this.listView1.Items.Clear();
            foreach (DataRow r in this.dtDm_Report.Rows)
            {
                ListViewItem item = new ListViewItem(new string[2] { r["REPORT_ID"].ToString(), r["REPORT_NAME"].ToString() });
                this.listView1.Items.Add(item);
            }
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.ShowReportInput();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ShowReportInput()
        {
            if (this.listView1.SelectedItems.Count == 0)
            {
                return;
            }
            ListViewItem item = (ListViewItem)this.listView1.SelectedItems[0];
            string report_id = item.SubItems[0].Text;
            string report_name = item.SubItems[1].Text;
            DataRow rowReport = this.dtDm_Report.Rows.Find(report_id);
            if (rowReport != null)
            {
                FrmReportInput frm = new FrmReportInput(this.cnMain, rowReport);
                frm.Show();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                this.ShowReportInput();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDm_Report_Click(object sender, EventArgs e)
        {
            try
            {
                (new FrmDm_Report(this.cnMain)).Show();
                this.LoadList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
