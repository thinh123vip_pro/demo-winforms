﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Tudien;
using Cn.Base;

namespace Cn.UI
{
    public partial class FrmMain : Form
    {
        CnMain cnMain;
        public FrmMain(CnMain cnmain)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.cnMain.mainForm = this;
            this.Text = this.Text + ": " + this.cnMain.GetSystemVar("COMPANY_NAME").ToString();
        }
        public void ExecuteProcedures(string menu_id)
        {
            try
            {
                switch (menu_id)
                {
                    #region He thong
                    case "mnuDm_User":
                        (new FrmDm_User(this.cnMain)).Show();
                        break;
                    case "mnuChangePassword":
                        (new FrmChangePassword(this.cnMain)).Show();
                        break;
                    case "mnuDm_Systemvar":
                        (new FrmDm_Systemvar(this.cnMain)).Show();
                        break;
                    case "mnuExit":
                        Application.Exit();
                        break;
                    #endregion
                    #region Form
                    case "mnuDu_Vthh":
                        (new FrmDu_Vthh(this.cnMain)).Show();
                        break;
                    case "mnuVthh_Nhap":
                        (new FrmVthh(this.cnMain, "N")).Show();
                        break;
                    case "mnuVthh_Xuat":
                        (new FrmVthh(this.cnMain, "X")).Show();
                        break;
                    case "mnuVthh_Listing":
                        (new FrmVthhListing(this.cnMain)).Show();
                        break;
                    #endregion
                    #region Danh mục
                    case "mnuDm_Kho":
                        (new FrmDm_Kho(this.cnMain)).Show();
                        break;
                    case "mnuDm_Ptnx":
                        (new FrmDm_Ptnx(this.cnMain)).Show();
                        break;
                    case "mnuDm_Vthh":
                        (new FrmDm_Vthh(this.cnMain)).Show();
                        break;
                    case "mnuDm_Nhom_Vthh":
                        (new FrmDm_Nhom_Vthh(this.cnMain)).Show();
                        break;
                    case "mnuDm_Dtpn":
                        (new FrmDm_Dtpn(this.cnMain)).Show();
                        break;
                    case "mnuDm_Nhom_Dtpn":
                        (new FrmDm_Nhom_Dtpn(this.cnMain)).Show();
                        break;
                    case "mnuDm_Dtgt":
                        (new FrmDm_Dtgt(this.cnMain)).Show();
                        break;
                    case "mnuDm_Nhom_Dtgt":
                        (new FrmDm_Nhom_Dtgt(this.cnMain)).Show();
                        break;
                    case "mnuDanhmucList":
                        (new FrmDanhmucList(this.cnMain)).Show();
                        break;
                    #endregion
                    #region BAO CAO
                    case "mnuReport":
                        (new FrmReportList(this.cnMain)).Show();
                        break;
                    #endregion

                    

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }


        private void mnu_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem menu = sender as ToolStripMenuItem;
                this.ExecuteProcedures(menu.Name);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if(MessageBox.Show("Bạn có muốn thoát phần mềm?","Thông báo",MessageBoxButtons.YesNoCancel) != System.Windows.Forms.DialogResult.Yes)
            {
                e.Cancel = true;
            }
            base.OnClosing(e);
        }
    }
}
