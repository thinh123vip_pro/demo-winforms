﻿namespace Cn.UI
{
    partial class FrmVthh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVthh));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.grid = new System.Windows.Forms.DataGridView();
            this.palBottom = new System.Windows.Forms.Panel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.palTop = new System.Windows.Forms.Panel();
            this.datNgay_Ctu = new System.Windows.Forms.DateTimePicker();
            this.txtNgay_Hdon = new System.Windows.Forms.TextBox();
            this.txtSo_Hdon = new System.Windows.Forms.TextBox();
            this.txtMa_Ptnx = new System.Windows.Forms.TextBox();
            this.txtMa_Kho = new System.Windows.Forms.TextBox();
            this.txtMa_Dtpn = new System.Windows.Forms.TextBox();
            this.txtDien_Giai = new System.Windows.Forms.TextBox();
            this.txtHo_Ten = new System.Windows.Forms.TextBox();
            this.txtSo_Ctu = new System.Windows.Forms.TextBox();
            this.lblSo_Hdon = new System.Windows.Forms.Label();
            this.lblNgay_Hdon = new System.Windows.Forms.Label();
            this.lblNgay_Ctu = new System.Windows.Forms.Label();
            this.lblTen_Ptnx = new System.Windows.Forms.Label();
            this.lblTen_Kho = new System.Windows.Forms.Label();
            this.lblTen_Dtpn = new System.Windows.Forms.Label();
            this.lblMa_Ptnx = new System.Windows.Forms.Label();
            this.lblMa_Kho = new System.Windows.Forms.Label();
            this.lblMa_Dtpn = new System.Windows.Forms.Label();
            this.lblDien_Giai = new System.Windows.Forms.Label();
            this.lblHo_Ten = new System.Windows.Forms.Label();
            this.lblSo_Ctu = new System.Windows.Forms.Label();
            this.gridFilter = new System.Windows.Forms.DataGridView();
            this.palFilter = new System.Windows.Forms.Panel();
            this.btnFilter = new System.Windows.Forms.Button();
            this.cboKySlieu = new System.Windows.Forms.ComboBox();
            this.lblDen_Ngay = new System.Windows.Forms.Label();
            this.datDen_Ngay = new System.Windows.Forms.DateTimePicker();
            this.lblKySlieu = new System.Windows.Forms.Label();
            this.txtMa_Dtpn_Filter = new System.Windows.Forms.TextBox();
            this.txtMa_Ptnx_Filter = new System.Windows.Forms.TextBox();
            this.txtMa_Kho_Filter = new System.Windows.Forms.TextBox();
            this.lblTu_Ngay = new System.Windows.Forms.Label();
            this.datTu_Ngay = new System.Windows.Forms.DateTimePicker();
            this.lblMa_Dtpn_Filter = new System.Windows.Forms.Label();
            this.lblMa_Ptnx_Filter = new System.Windows.Forms.Label();
            this.lblMa_Kho_Filter = new System.Windows.Forms.Label();
            this.toolbar = new System.Windows.Forms.ToolStrip();
            this.ToolNew = new System.Windows.Forms.ToolStripButton();
            this.ToolCopy = new System.Windows.Forms.ToolStripButton();
            this.ToolRestore = new System.Windows.Forms.ToolStripButton();
            this.ToolDelete = new System.Windows.Forms.ToolStripButton();
            this.ToolUpdate = new System.Windows.Forms.ToolStripButton();
            this.ToolRefresh = new System.Windows.Forms.ToolStripButton();
            this.ToolPrint = new System.Windows.Forms.ToolStripButton();
            this.ToolClose = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFilter)).BeginInit();
            this.palFilter.SuspendLayout();
            this.toolbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 39);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grid);
            this.splitContainer1.Panel1.Controls.Add(this.palBottom);
            this.splitContainer1.Panel1.Controls.Add(this.statusStrip);
            this.splitContainer1.Panel1.Controls.Add(this.palTop);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridFilter);
            this.splitContainer1.Panel2.Controls.Add(this.palFilter);
            this.splitContainer1.Size = new System.Drawing.Size(1323, 522);
            this.splitContainer1.SplitterDistance = 904;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // grid
            // 
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 186);
            this.grid.Margin = new System.Windows.Forms.Padding(4);
            this.grid.Name = "grid";
            this.grid.RowHeadersWidth = 62;
            this.grid.Size = new System.Drawing.Size(902, 249);
            this.grid.TabIndex = 2;
            // 
            // palBottom
            // 
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 435);
            this.palBottom.Margin = new System.Windows.Forms.Padding(4);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(902, 59);
            this.palBottom.TabIndex = 1;
            this.palBottom.Visible = false;
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMsg});
            this.statusStrip.Location = new System.Drawing.Point(0, 494);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip.Size = new System.Drawing.Size(902, 26);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripMsg
            // 
            this.toolStripMsg.Name = "toolStripMsg";
            this.toolStripMsg.Size = new System.Drawing.Size(95, 20);
            this.toolStripMsg.Text = "toolStripMsg";
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.datNgay_Ctu);
            this.palTop.Controls.Add(this.txtNgay_Hdon);
            this.palTop.Controls.Add(this.txtSo_Hdon);
            this.palTop.Controls.Add(this.txtMa_Ptnx);
            this.palTop.Controls.Add(this.txtMa_Kho);
            this.palTop.Controls.Add(this.txtMa_Dtpn);
            this.palTop.Controls.Add(this.txtDien_Giai);
            this.palTop.Controls.Add(this.txtHo_Ten);
            this.palTop.Controls.Add(this.txtSo_Ctu);
            this.palTop.Controls.Add(this.lblSo_Hdon);
            this.palTop.Controls.Add(this.lblNgay_Hdon);
            this.palTop.Controls.Add(this.lblNgay_Ctu);
            this.palTop.Controls.Add(this.lblTen_Ptnx);
            this.palTop.Controls.Add(this.lblTen_Kho);
            this.palTop.Controls.Add(this.lblTen_Dtpn);
            this.palTop.Controls.Add(this.lblMa_Ptnx);
            this.palTop.Controls.Add(this.lblMa_Kho);
            this.palTop.Controls.Add(this.lblMa_Dtpn);
            this.palTop.Controls.Add(this.lblDien_Giai);
            this.palTop.Controls.Add(this.lblHo_Ten);
            this.palTop.Controls.Add(this.lblSo_Ctu);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Margin = new System.Windows.Forms.Padding(4);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(902, 186);
            this.palTop.TabIndex = 0;
            // 
            // datNgay_Ctu
            // 
            this.datNgay_Ctu.CustomFormat = "dd/MM/yyyy";
            this.datNgay_Ctu.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datNgay_Ctu.Location = new System.Drawing.Point(309, 7);
            this.datNgay_Ctu.Margin = new System.Windows.Forms.Padding(4);
            this.datNgay_Ctu.Name = "datNgay_Ctu";
            this.datNgay_Ctu.Size = new System.Drawing.Size(131, 22);
            this.datNgay_Ctu.TabIndex = 1;
            // 
            // txtNgay_Hdon
            // 
            this.txtNgay_Hdon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgay_Hdon.Location = new System.Drawing.Point(751, 7);
            this.txtNgay_Hdon.Margin = new System.Windows.Forms.Padding(4);
            this.txtNgay_Hdon.Name = "txtNgay_Hdon";
            this.txtNgay_Hdon.Size = new System.Drawing.Size(132, 22);
            this.txtNgay_Hdon.TabIndex = 3;
            // 
            // txtSo_Hdon
            // 
            this.txtSo_Hdon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSo_Hdon.Location = new System.Drawing.Point(517, 7);
            this.txtSo_Hdon.Margin = new System.Windows.Forms.Padding(4);
            this.txtSo_Hdon.Name = "txtSo_Hdon";
            this.txtSo_Hdon.Size = new System.Drawing.Size(132, 22);
            this.txtSo_Hdon.TabIndex = 2;
            // 
            // txtMa_Ptnx
            // 
            this.txtMa_Ptnx.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Ptnx.Location = new System.Drawing.Point(517, 39);
            this.txtMa_Ptnx.Margin = new System.Windows.Forms.Padding(4);
            this.txtMa_Ptnx.Name = "txtMa_Ptnx";
            this.txtMa_Ptnx.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Ptnx.TabIndex = 5;
            this.txtMa_Ptnx.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Ptnx.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Ptnx.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Kho
            // 
            this.txtMa_Kho.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Kho.Location = new System.Drawing.Point(89, 39);
            this.txtMa_Kho.Margin = new System.Windows.Forms.Padding(4);
            this.txtMa_Kho.Name = "txtMa_Kho";
            this.txtMa_Kho.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Kho.TabIndex = 4;
            this.txtMa_Kho.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Kho.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Kho.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Dtpn
            // 
            this.txtMa_Dtpn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Dtpn.Location = new System.Drawing.Point(89, 108);
            this.txtMa_Dtpn.Margin = new System.Windows.Forms.Padding(4);
            this.txtMa_Dtpn.Name = "txtMa_Dtpn";
            this.txtMa_Dtpn.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Dtpn.TabIndex = 7;
            this.txtMa_Dtpn.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Dtpn.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Dtpn.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtDien_Giai
            // 
            this.txtDien_Giai.Location = new System.Drawing.Point(89, 144);
            this.txtDien_Giai.Margin = new System.Windows.Forms.Padding(4);
            this.txtDien_Giai.Name = "txtDien_Giai";
            this.txtDien_Giai.Size = new System.Drawing.Size(793, 22);
            this.txtDien_Giai.TabIndex = 8;
            // 
            // txtHo_Ten
            // 
            this.txtHo_Ten.Location = new System.Drawing.Point(89, 75);
            this.txtHo_Ten.Margin = new System.Windows.Forms.Padding(4);
            this.txtHo_Ten.Name = "txtHo_Ten";
            this.txtHo_Ten.Size = new System.Drawing.Size(793, 22);
            this.txtHo_Ten.TabIndex = 6;
            // 
            // txtSo_Ctu
            // 
            this.txtSo_Ctu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSo_Ctu.Location = new System.Drawing.Point(89, 7);
            this.txtSo_Ctu.Margin = new System.Windows.Forms.Padding(4);
            this.txtSo_Ctu.Name = "txtSo_Ctu";
            this.txtSo_Ctu.Size = new System.Drawing.Size(132, 22);
            this.txtSo_Ctu.TabIndex = 0;
            this.txtSo_Ctu.Leave += new System.EventHandler(this.txtSo_Ctu_Leave);
            // 
            // lblSo_Hdon
            // 
            this.lblSo_Hdon.AutoSize = true;
            this.lblSo_Hdon.Location = new System.Drawing.Point(444, 11);
            this.lblSo_Hdon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSo_Hdon.Name = "lblSo_Hdon";
            this.lblSo_Hdon.Size = new System.Drawing.Size(60, 16);
            this.lblSo_Hdon.TabIndex = 0;
            this.lblSo_Hdon.Text = "Số h.đơn";
            // 
            // lblNgay_Hdon
            // 
            this.lblNgay_Hdon.AutoSize = true;
            this.lblNgay_Hdon.Location = new System.Drawing.Point(659, 15);
            this.lblNgay_Hdon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNgay_Hdon.Name = "lblNgay_Hdon";
            this.lblNgay_Hdon.Size = new System.Drawing.Size(76, 16);
            this.lblNgay_Hdon.TabIndex = 0;
            this.lblNgay_Hdon.Text = "Ngày h.đơn";
            // 
            // lblNgay_Ctu
            // 
            this.lblNgay_Ctu.AutoSize = true;
            this.lblNgay_Ctu.Location = new System.Drawing.Point(231, 11);
            this.lblNgay_Ctu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNgay_Ctu.Name = "lblNgay_Ctu";
            this.lblNgay_Ctu.Size = new System.Drawing.Size(63, 16);
            this.lblNgay_Ctu.TabIndex = 0;
            this.lblNgay_Ctu.Text = "Ngày c.từ";
            // 
            // lblTen_Ptnx
            // 
            this.lblTen_Ptnx.AutoSize = true;
            this.lblTen_Ptnx.Location = new System.Drawing.Point(659, 43);
            this.lblTen_Ptnx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Ptnx.Name = "lblTen_Ptnx";
            this.lblTen_Ptnx.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Ptnx.TabIndex = 0;
            this.lblTen_Ptnx.Text = ".....";
            // 
            // lblTen_Kho
            // 
            this.lblTen_Kho.AutoSize = true;
            this.lblTen_Kho.Location = new System.Drawing.Point(231, 43);
            this.lblTen_Kho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Kho.Name = "lblTen_Kho";
            this.lblTen_Kho.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Kho.TabIndex = 0;
            this.lblTen_Kho.Text = ".....";
            // 
            // lblTen_Dtpn
            // 
            this.lblTen_Dtpn.AutoSize = true;
            this.lblTen_Dtpn.Location = new System.Drawing.Point(231, 112);
            this.lblTen_Dtpn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Dtpn.Name = "lblTen_Dtpn";
            this.lblTen_Dtpn.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Dtpn.TabIndex = 0;
            this.lblTen_Dtpn.Text = ".....";
            // 
            // lblMa_Ptnx
            // 
            this.lblMa_Ptnx.AutoSize = true;
            this.lblMa_Ptnx.Location = new System.Drawing.Point(444, 43);
            this.lblMa_Ptnx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Ptnx.Name = "lblMa_Ptnx";
            this.lblMa_Ptnx.Size = new System.Drawing.Size(32, 16);
            this.lblMa_Ptnx.TabIndex = 0;
            this.lblMa_Ptnx.Text = "Ptnx";
            // 
            // lblMa_Kho
            // 
            this.lblMa_Kho.AutoSize = true;
            this.lblMa_Kho.Location = new System.Drawing.Point(16, 43);
            this.lblMa_Kho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Kho.Name = "lblMa_Kho";
            this.lblMa_Kho.Size = new System.Drawing.Size(30, 16);
            this.lblMa_Kho.TabIndex = 0;
            this.lblMa_Kho.Text = "Kho";
            // 
            // lblMa_Dtpn
            // 
            this.lblMa_Dtpn.AutoSize = true;
            this.lblMa_Dtpn.Location = new System.Drawing.Point(16, 112);
            this.lblMa_Dtpn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Dtpn.Name = "lblMa_Dtpn";
            this.lblMa_Dtpn.Size = new System.Drawing.Size(34, 16);
            this.lblMa_Dtpn.TabIndex = 0;
            this.lblMa_Dtpn.Text = "Đtpn";
            // 
            // lblDien_Giai
            // 
            this.lblDien_Giai.AutoSize = true;
            this.lblDien_Giai.Location = new System.Drawing.Point(16, 148);
            this.lblDien_Giai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDien_Giai.Name = "lblDien_Giai";
            this.lblDien_Giai.Size = new System.Drawing.Size(60, 16);
            this.lblDien_Giai.TabIndex = 0;
            this.lblDien_Giai.Text = "Diễn giải";
            // 
            // lblHo_Ten
            // 
            this.lblHo_Ten.AutoSize = true;
            this.lblHo_Ten.Location = new System.Drawing.Point(16, 79);
            this.lblHo_Ten.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHo_Ten.Name = "lblHo_Ten";
            this.lblHo_Ten.Size = new System.Drawing.Size(46, 16);
            this.lblHo_Ten.TabIndex = 0;
            this.lblHo_Ten.Text = "Họ tên";
            // 
            // lblSo_Ctu
            // 
            this.lblSo_Ctu.AutoSize = true;
            this.lblSo_Ctu.Location = new System.Drawing.Point(16, 11);
            this.lblSo_Ctu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSo_Ctu.Name = "lblSo_Ctu";
            this.lblSo_Ctu.Size = new System.Drawing.Size(47, 16);
            this.lblSo_Ctu.TabIndex = 0;
            this.lblSo_Ctu.Text = "Số c.từ";
            // 
            // gridFilter
            // 
            this.gridFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridFilter.Location = new System.Drawing.Point(0, 133);
            this.gridFilter.Margin = new System.Windows.Forms.Padding(4);
            this.gridFilter.Name = "gridFilter";
            this.gridFilter.RowHeadersWidth = 62;
            this.gridFilter.Size = new System.Drawing.Size(412, 387);
            this.gridFilter.TabIndex = 1;
            // 
            // palFilter
            // 
            this.palFilter.Controls.Add(this.btnFilter);
            this.palFilter.Controls.Add(this.cboKySlieu);
            this.palFilter.Controls.Add(this.lblDen_Ngay);
            this.palFilter.Controls.Add(this.datDen_Ngay);
            this.palFilter.Controls.Add(this.lblKySlieu);
            this.palFilter.Controls.Add(this.txtMa_Dtpn_Filter);
            this.palFilter.Controls.Add(this.txtMa_Ptnx_Filter);
            this.palFilter.Controls.Add(this.txtMa_Kho_Filter);
            this.palFilter.Controls.Add(this.lblTu_Ngay);
            this.palFilter.Controls.Add(this.datTu_Ngay);
            this.palFilter.Controls.Add(this.lblMa_Dtpn_Filter);
            this.palFilter.Controls.Add(this.lblMa_Ptnx_Filter);
            this.palFilter.Controls.Add(this.lblMa_Kho_Filter);
            this.palFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.palFilter.Location = new System.Drawing.Point(0, 0);
            this.palFilter.Margin = new System.Windows.Forms.Padding(4);
            this.palFilter.Name = "palFilter";
            this.palFilter.Size = new System.Drawing.Size(412, 133);
            this.palFilter.TabIndex = 0;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(265, 96);
            this.btnFilter.Margin = new System.Windows.Forms.Padding(4);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(132, 28);
            this.btnFilter.TabIndex = 6;
            this.btnFilter.Text = "Lọc";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // cboKySlieu
            // 
            this.cboKySlieu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKySlieu.FormattingEnabled = true;
            this.cboKySlieu.Location = new System.Drawing.Point(83, 6);
            this.cboKySlieu.Margin = new System.Windows.Forms.Padding(4);
            this.cboKySlieu.Name = "cboKySlieu";
            this.cboKySlieu.Size = new System.Drawing.Size(313, 24);
            this.cboKySlieu.TabIndex = 0;
            this.cboKySlieu.SelectedIndexChanged += new System.EventHandler(this.cboKySlieu_SelectedIndexChanged);
            // 
            // lblDen_Ngay
            // 
            this.lblDen_Ngay.AutoSize = true;
            this.lblDen_Ngay.Location = new System.Drawing.Point(223, 39);
            this.lblDen_Ngay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDen_Ngay.Name = "lblDen_Ngay";
            this.lblDen_Ngay.Size = new System.Drawing.Size(30, 16);
            this.lblDen_Ngay.TabIndex = 0;
            this.lblDen_Ngay.Text = "đến";
            // 
            // datDen_Ngay
            // 
            this.datDen_Ngay.CustomFormat = "dd/MM/yyyy";
            this.datDen_Ngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datDen_Ngay.Location = new System.Drawing.Point(265, 36);
            this.datDen_Ngay.Margin = new System.Windows.Forms.Padding(4);
            this.datDen_Ngay.Name = "datDen_Ngay";
            this.datDen_Ngay.Size = new System.Drawing.Size(131, 22);
            this.datDen_Ngay.TabIndex = 2;
            // 
            // lblKySlieu
            // 
            this.lblKySlieu.AutoSize = true;
            this.lblKySlieu.Location = new System.Drawing.Point(4, 11);
            this.lblKySlieu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKySlieu.Name = "lblKySlieu";
            this.lblKySlieu.Size = new System.Drawing.Size(56, 16);
            this.lblKySlieu.TabIndex = 0;
            this.lblKySlieu.Text = "Kỳ s.liệu";
            // 
            // txtMa_Dtpn_Filter
            // 
            this.txtMa_Dtpn_Filter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Dtpn_Filter.Location = new System.Drawing.Point(83, 96);
            this.txtMa_Dtpn_Filter.Margin = new System.Windows.Forms.Padding(4);
            this.txtMa_Dtpn_Filter.Name = "txtMa_Dtpn_Filter";
            this.txtMa_Dtpn_Filter.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Dtpn_Filter.TabIndex = 5;
            this.txtMa_Dtpn_Filter.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Dtpn_Filter.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Ptnx_Filter
            // 
            this.txtMa_Ptnx_Filter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Ptnx_Filter.Location = new System.Drawing.Point(265, 66);
            this.txtMa_Ptnx_Filter.Margin = new System.Windows.Forms.Padding(4);
            this.txtMa_Ptnx_Filter.Name = "txtMa_Ptnx_Filter";
            this.txtMa_Ptnx_Filter.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Ptnx_Filter.TabIndex = 4;
            this.txtMa_Ptnx_Filter.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Ptnx_Filter.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Kho_Filter
            // 
            this.txtMa_Kho_Filter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Kho_Filter.Location = new System.Drawing.Point(83, 64);
            this.txtMa_Kho_Filter.Margin = new System.Windows.Forms.Padding(4);
            this.txtMa_Kho_Filter.Name = "txtMa_Kho_Filter";
            this.txtMa_Kho_Filter.Size = new System.Drawing.Size(131, 22);
            this.txtMa_Kho_Filter.TabIndex = 3;
            this.txtMa_Kho_Filter.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Kho_Filter.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // lblTu_Ngay
            // 
            this.lblTu_Ngay.AutoSize = true;
            this.lblTu_Ngay.Location = new System.Drawing.Point(4, 43);
            this.lblTu_Ngay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTu_Ngay.Name = "lblTu_Ngay";
            this.lblTu_Ngay.Size = new System.Drawing.Size(63, 16);
            this.lblTu_Ngay.TabIndex = 0;
            this.lblTu_Ngay.Text = "Ngày c.từ";
            // 
            // datTu_Ngay
            // 
            this.datTu_Ngay.CustomFormat = "dd/MM/yyyy";
            this.datTu_Ngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datTu_Ngay.Location = new System.Drawing.Point(83, 39);
            this.datTu_Ngay.Margin = new System.Windows.Forms.Padding(4);
            this.datTu_Ngay.Name = "datTu_Ngay";
            this.datTu_Ngay.Size = new System.Drawing.Size(131, 22);
            this.datTu_Ngay.TabIndex = 1;
            // 
            // lblMa_Dtpn_Filter
            // 
            this.lblMa_Dtpn_Filter.AutoSize = true;
            this.lblMa_Dtpn_Filter.Location = new System.Drawing.Point(4, 100);
            this.lblMa_Dtpn_Filter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Dtpn_Filter.Name = "lblMa_Dtpn_Filter";
            this.lblMa_Dtpn_Filter.Size = new System.Drawing.Size(34, 16);
            this.lblMa_Dtpn_Filter.TabIndex = 0;
            this.lblMa_Dtpn_Filter.Text = "Đtpn";
            // 
            // lblMa_Ptnx_Filter
            // 
            this.lblMa_Ptnx_Filter.AutoSize = true;
            this.lblMa_Ptnx_Filter.Location = new System.Drawing.Point(219, 70);
            this.lblMa_Ptnx_Filter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Ptnx_Filter.Name = "lblMa_Ptnx_Filter";
            this.lblMa_Ptnx_Filter.Size = new System.Drawing.Size(32, 16);
            this.lblMa_Ptnx_Filter.TabIndex = 0;
            this.lblMa_Ptnx_Filter.Text = "Ptnx";
            // 
            // lblMa_Kho_Filter
            // 
            this.lblMa_Kho_Filter.AutoSize = true;
            this.lblMa_Kho_Filter.Location = new System.Drawing.Point(4, 70);
            this.lblMa_Kho_Filter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Kho_Filter.Name = "lblMa_Kho_Filter";
            this.lblMa_Kho_Filter.Size = new System.Drawing.Size(30, 16);
            this.lblMa_Kho_Filter.TabIndex = 0;
            this.lblMa_Kho_Filter.Text = "Kho";
            // 
            // toolbar
            // 
            this.toolbar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolNew,
            this.ToolCopy,
            this.ToolRestore,
            this.ToolDelete,
            this.ToolUpdate,
            this.ToolRefresh,
            this.ToolPrint,
            this.ToolClose});
            this.toolbar.Location = new System.Drawing.Point(0, 0);
            this.toolbar.Name = "toolbar";
            this.toolbar.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.toolbar.Size = new System.Drawing.Size(1323, 39);
            this.toolbar.TabIndex = 1;
            this.toolbar.Text = "toolStrip1";
            // 
            // ToolNew
            // 
            this.ToolNew.Image = ((System.Drawing.Image)(resources.GetObject("ToolNew.Image")));
            this.ToolNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolNew.Name = "ToolNew";
            this.ToolNew.Size = new System.Drawing.Size(82, 36);
            this.ToolNew.Text = "Thêm";
            this.ToolNew.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // ToolCopy
            // 
            this.ToolCopy.Image = ((System.Drawing.Image)(resources.GetObject("ToolCopy.Image")));
            this.ToolCopy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolCopy.Name = "ToolCopy";
            this.ToolCopy.Size = new System.Drawing.Size(79, 36);
            this.ToolCopy.Text = "Copy";
            this.ToolCopy.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // ToolRestore
            // 
            this.ToolRestore.Image = ((System.Drawing.Image)(resources.GetObject("ToolRestore.Image")));
            this.ToolRestore.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolRestore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolRestore.Name = "ToolRestore";
            this.ToolRestore.Size = new System.Drawing.Size(111, 36);
            this.ToolRestore.Text = "Khôi phục";
            this.ToolRestore.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // ToolDelete
            // 
            this.ToolDelete.Image = ((System.Drawing.Image)(resources.GetObject("ToolDelete.Image")));
            this.ToolDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolDelete.Name = "ToolDelete";
            this.ToolDelete.Size = new System.Drawing.Size(71, 36);
            this.ToolDelete.Text = "Xóa";
            this.ToolDelete.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // ToolUpdate
            // 
            this.ToolUpdate.Image = ((System.Drawing.Image)(resources.GetObject("ToolUpdate.Image")));
            this.ToolUpdate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolUpdate.Name = "ToolUpdate";
            this.ToolUpdate.Size = new System.Drawing.Size(69, 36);
            this.ToolUpdate.Text = "Lưu";
            this.ToolUpdate.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // ToolRefresh
            // 
            this.ToolRefresh.Image = ((System.Drawing.Image)(resources.GetObject("ToolRefresh.Image")));
            this.ToolRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolRefresh.Name = "ToolRefresh";
            this.ToolRefresh.Size = new System.Drawing.Size(94, 36);
            this.ToolRefresh.Text = "Refresh";
            this.ToolRefresh.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // ToolPrint
            // 
            this.ToolPrint.Image = ((System.Drawing.Image)(resources.GetObject("ToolPrint.Image")));
            this.ToolPrint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolPrint.Name = "ToolPrint";
            this.ToolPrint.Size = new System.Drawing.Size(57, 36);
            this.ToolPrint.Text = "In";
            this.ToolPrint.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // ToolClose
            // 
            this.ToolClose.Image = ((System.Drawing.Image)(resources.GetObject("ToolClose.Image")));
            this.ToolClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolClose.Name = "ToolClose";
            this.ToolClose.Size = new System.Drawing.Size(83, 36);
            this.ToolClose.Text = "Thoát";
            this.ToolClose.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // FrmVthh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1323, 561);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolbar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "FrmVthh";
            this.Text = "FrmVthh";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.palTop.ResumeLayout(false);
            this.palTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFilter)).EndInit();
            this.palFilter.ResumeLayout(false);
            this.palFilter.PerformLayout();
            this.toolbar.ResumeLayout(false);
            this.toolbar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Panel palTop;
        private System.Windows.Forms.DataGridView gridFilter;
        private System.Windows.Forms.Panel palFilter;
        protected System.Windows.Forms.ToolStrip toolbar;
        protected System.Windows.Forms.ToolStripButton ToolNew;
        protected System.Windows.Forms.ToolStripButton ToolCopy;
        protected System.Windows.Forms.ToolStripButton ToolRestore;
        protected System.Windows.Forms.ToolStripButton ToolDelete;
        protected System.Windows.Forms.ToolStripButton ToolUpdate;
        protected System.Windows.Forms.ToolStripButton ToolRefresh;
        protected System.Windows.Forms.ToolStripButton ToolPrint;
        protected System.Windows.Forms.ToolStripButton ToolClose;
        private System.Windows.Forms.DateTimePicker datNgay_Ctu;
        private System.Windows.Forms.TextBox txtSo_Hdon;
        private System.Windows.Forms.TextBox txtMa_Ptnx;
        private System.Windows.Forms.TextBox txtMa_Kho;
        private System.Windows.Forms.TextBox txtMa_Dtpn;
        private System.Windows.Forms.TextBox txtDien_Giai;
        private System.Windows.Forms.TextBox txtHo_Ten;
        private System.Windows.Forms.TextBox txtSo_Ctu;
        private System.Windows.Forms.Label lblSo_Hdon;
        private System.Windows.Forms.Label lblNgay_Hdon;
        private System.Windows.Forms.Label lblNgay_Ctu;
        private System.Windows.Forms.Label lblTen_Ptnx;
        private System.Windows.Forms.Label lblTen_Kho;
        private System.Windows.Forms.Label lblTen_Dtpn;
        private System.Windows.Forms.Label lblMa_Ptnx;
        private System.Windows.Forms.Label lblMa_Kho;
        private System.Windows.Forms.Label lblMa_Dtpn;
        private System.Windows.Forms.Label lblDien_Giai;
        private System.Windows.Forms.Label lblHo_Ten;
        private System.Windows.Forms.Label lblSo_Ctu;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.ComboBox cboKySlieu;
        private System.Windows.Forms.Label lblDen_Ngay;
        private System.Windows.Forms.DateTimePicker datDen_Ngay;
        private System.Windows.Forms.Label lblKySlieu;
        private System.Windows.Forms.TextBox txtMa_Dtpn_Filter;
        private System.Windows.Forms.TextBox txtMa_Ptnx_Filter;
        private System.Windows.Forms.TextBox txtMa_Kho_Filter;
        private System.Windows.Forms.Label lblTu_Ngay;
        private System.Windows.Forms.DateTimePicker datTu_Ngay;
        private System.Windows.Forms.Label lblMa_Dtpn_Filter;
        private System.Windows.Forms.Label lblMa_Ptnx_Filter;
        private System.Windows.Forms.Label lblMa_Kho_Filter;
        private System.Windows.Forms.TextBox txtNgay_Hdon;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripMsg;
    }
}