﻿namespace Cn.UI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.mnuHeThong = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_User = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuChangePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Systemvar = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTudien = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Kho = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Ptnx = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Vthh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Nhom_Vthh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Dtpn = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Nhom_Dtpn = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Dtgt = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDm_Nhom_Dtgt = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDanhmucList = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVatTu = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDu_Vthh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVthh_Nhap = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVthh_Xuat = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVthh_Listing = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReport = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHeThong,
            this.mnuTudien,
            this.mnuVatTu,
            this.mnuReport});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1189, 28);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // mnuHeThong
            // 
            this.mnuHeThong.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDm_User,
            this.mnuChangePassword,
            this.mnuDm_Systemvar,
            this.mnuExit});
            this.mnuHeThong.Name = "mnuHeThong";
            this.mnuHeThong.Size = new System.Drawing.Size(85, 24);
            this.mnuHeThong.Text = "Hệ thống";
            // 
            // mnuDm_User
            // 
            this.mnuDm_User.Name = "mnuDm_User";
            this.mnuDm_User.Size = new System.Drawing.Size(251, 26);
            this.mnuDm_User.Text = "Khai báo người sử dụng";
            this.mnuDm_User.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuChangePassword
            // 
            this.mnuChangePassword.Name = "mnuChangePassword";
            this.mnuChangePassword.Size = new System.Drawing.Size(251, 26);
            this.mnuChangePassword.Text = "Đổi mật khẩu";
            this.mnuChangePassword.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Systemvar
            // 
            this.mnuDm_Systemvar.Name = "mnuDm_Systemvar";
            this.mnuDm_Systemvar.Size = new System.Drawing.Size(251, 26);
            this.mnuDm_Systemvar.Text = "Khai báo hệ thống";
            this.mnuDm_Systemvar.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(251, 26);
            this.mnuExit.Text = "Thoát";
            this.mnuExit.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuTudien
            // 
            this.mnuTudien.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDm_Kho,
            this.mnuDm_Ptnx,
            this.mnuDm_Vthh,
            this.mnuDm_Nhom_Vthh,
            this.mnuDm_Dtpn,
            this.mnuDm_Nhom_Dtpn,
            this.mnuDm_Dtgt,
            this.mnuDm_Nhom_Dtgt,
            this.mnuDanhmucList});
            this.mnuTudien.Name = "mnuTudien";
            this.mnuTudien.Size = new System.Drawing.Size(90, 24);
            this.mnuTudien.Text = "Danh mục";
            // 
            // mnuDm_Kho
            // 
            this.mnuDm_Kho.Name = "mnuDm_Kho";
            this.mnuDm_Kho.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Kho.Text = "Danh mục kho";
            this.mnuDm_Kho.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Ptnx
            // 
            this.mnuDm_Ptnx.Name = "mnuDm_Ptnx";
            this.mnuDm_Ptnx.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Ptnx.Text = "Danh mục ptnx";
            this.mnuDm_Ptnx.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Vthh
            // 
            this.mnuDm_Vthh.Name = "mnuDm_Vthh";
            this.mnuDm_Vthh.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Vthh.Text = "Danh mục vật tư";
            this.mnuDm_Vthh.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Nhom_Vthh
            // 
            this.mnuDm_Nhom_Vthh.Name = "mnuDm_Nhom_Vthh";
            this.mnuDm_Nhom_Vthh.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Nhom_Vthh.Text = "Danh mục nhóm vật tư";
            this.mnuDm_Nhom_Vthh.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Dtpn
            // 
            this.mnuDm_Dtpn.Name = "mnuDm_Dtpn";
            this.mnuDm_Dtpn.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Dtpn.Text = "Danh mục đtpn";
            this.mnuDm_Dtpn.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Nhom_Dtpn
            // 
            this.mnuDm_Nhom_Dtpn.Name = "mnuDm_Nhom_Dtpn";
            this.mnuDm_Nhom_Dtpn.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Nhom_Dtpn.Text = "Danh mục nhóm đtpn";
            this.mnuDm_Nhom_Dtpn.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Dtgt
            // 
            this.mnuDm_Dtgt.Name = "mnuDm_Dtgt";
            this.mnuDm_Dtgt.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Dtgt.Text = "Danh mục đtgt";
            this.mnuDm_Dtgt.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDm_Nhom_Dtgt
            // 
            this.mnuDm_Nhom_Dtgt.Name = "mnuDm_Nhom_Dtgt";
            this.mnuDm_Nhom_Dtgt.Size = new System.Drawing.Size(243, 26);
            this.mnuDm_Nhom_Dtgt.Text = "Danh mục nhóm đtgt";
            this.mnuDm_Nhom_Dtgt.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuDanhmucList
            // 
            this.mnuDanhmucList.Name = "mnuDanhmucList";
            this.mnuDanhmucList.Size = new System.Drawing.Size(243, 26);
            this.mnuDanhmucList.Text = "Danh mục khác";
            this.mnuDanhmucList.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuVatTu
            // 
            this.mnuVatTu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDu_Vthh,
            this.mnuVthh_Nhap,
            this.mnuVthh_Xuat,
            this.mnuVthh_Listing});
            this.mnuVatTu.Name = "mnuVatTu";
            this.mnuVatTu.Size = new System.Drawing.Size(63, 24);
            this.mnuVatTu.Text = "Vật tư";
            // 
            // mnuDu_Vthh
            // 
            this.mnuDu_Vthh.Name = "mnuDu_Vthh";
            this.mnuDu_Vthh.Size = new System.Drawing.Size(204, 26);
            this.mnuDu_Vthh.Text = "Dư vật tư";
            this.mnuDu_Vthh.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuVthh_Nhap
            // 
            this.mnuVthh_Nhap.Name = "mnuVthh_Nhap";
            this.mnuVthh_Nhap.Size = new System.Drawing.Size(204, 26);
            this.mnuVthh_Nhap.Text = "Nhập";
            this.mnuVthh_Nhap.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuVthh_Xuat
            // 
            this.mnuVthh_Xuat.Name = "mnuVthh_Xuat";
            this.mnuVthh_Xuat.Size = new System.Drawing.Size(204, 26);
            this.mnuVthh_Xuat.Text = "Xuất";
            this.mnuVthh_Xuat.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuVthh_Listing
            // 
            this.mnuVthh_Listing.Name = "mnuVthh_Listing";
            this.mnuVthh_Listing.Size = new System.Drawing.Size(204, 26);
            this.mnuVthh_Listing.Text = "Liệt kê nhập xuất";
            this.mnuVthh_Listing.Click += new System.EventHandler(this.mnu_Click);
            // 
            // mnuReport
            // 
            this.mnuReport.Name = "mnuReport";
            this.mnuReport.Size = new System.Drawing.Size(77, 24);
            this.mnuReport.Text = "Báo cáo";
            this.mnuReport.Click += new System.EventHandler(this.mnu_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1189, 582);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menu;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vật tư hàng hóa";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem mnuHeThong;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_User;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Systemvar;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.ToolStripMenuItem mnuTudien;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Vthh;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Kho;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Dtpn;
        private System.Windows.Forms.ToolStripMenuItem mnuDanhmucList;
        private System.Windows.Forms.ToolStripMenuItem mnuVatTu;
        private System.Windows.Forms.ToolStripMenuItem mnuDu_Vthh;
        private System.Windows.Forms.ToolStripMenuItem mnuVthh_Nhap;
        private System.Windows.Forms.ToolStripMenuItem mnuVthh_Xuat;
        private System.Windows.Forms.ToolStripMenuItem mnuReport;
        private System.Windows.Forms.ToolStripMenuItem mnuVthh_Listing;
        private System.Windows.Forms.ToolStripMenuItem mnuChangePassword;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Ptnx;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Nhom_Vthh;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Nhom_Dtpn;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Dtgt;
        private System.Windows.Forms.ToolStripMenuItem mnuDm_Nhom_Dtgt;
    }
}

