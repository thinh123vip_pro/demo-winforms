﻿namespace Cn.UI
{
    partial class FrmVthhListing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVthhListing));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupCondition = new System.Windows.Forms.GroupBox();
            this.txtSo_Hdon = new System.Windows.Forms.TextBox();
            this.txtMa_Ptnx = new System.Windows.Forms.TextBox();
            this.txtMa_Kho = new System.Windows.Forms.TextBox();
            this.txtMa_Nhom_Vthh = new System.Windows.Forms.TextBox();
            this.txtMa_Nhom_Dtgt = new System.Windows.Forms.TextBox();
            this.txtMa_Nhom_Dtpn = new System.Windows.Forms.TextBox();
            this.txtMa_Vthh = new System.Windows.Forms.TextBox();
            this.txtMa_Dtgt = new System.Windows.Forms.TextBox();
            this.txtMa_Dtpn = new System.Windows.Forms.TextBox();
            this.txtSo_Ctu = new System.Windows.Forms.TextBox();
            this.lblSo_Hdon = new System.Windows.Forms.Label();
            this.lblTen_Ptnx = new System.Windows.Forms.Label();
            this.lblTen_Kho = new System.Windows.Forms.Label();
            this.lblTen_Nhom_Vthh = new System.Windows.Forms.Label();
            this.lblTen_Nhom_Dtgt = new System.Windows.Forms.Label();
            this.lblTen_Nhom_Dtpn = new System.Windows.Forms.Label();
            this.lblTen_Vthh = new System.Windows.Forms.Label();
            this.lblTen_Dtgt = new System.Windows.Forms.Label();
            this.lblTen_Dtpn = new System.Windows.Forms.Label();
            this.lblMa_Ptnx = new System.Windows.Forms.Label();
            this.lblMa_Kho = new System.Windows.Forms.Label();
            this.lblMa_Nhom_Vthh = new System.Windows.Forms.Label();
            this.lblMa_Nhom_Dtgt = new System.Windows.Forms.Label();
            this.lblMa_Nhom_Dtpn = new System.Windows.Forms.Label();
            this.lblMa_Vthh = new System.Windows.Forms.Label();
            this.lblMa_Dtgt = new System.Windows.Forms.Label();
            this.lblMa_Dtpn = new System.Windows.Forms.Label();
            this.lblSo_Ctu = new System.Windows.Forms.Label();
            this.cboNX = new System.Windows.Forms.ComboBox();
            this.cboKySlieu = new System.Windows.Forms.ComboBox();
            this.lblDen_Ngay = new System.Windows.Forms.Label();
            this.datDen_Ngay = new System.Windows.Forms.DateTimePicker();
            this.lblNX = new System.Windows.Forms.Label();
            this.lblKySlieu = new System.Windows.Forms.Label();
            this.lblTu_Ngay = new System.Windows.Forms.Label();
            this.datTu_Ngay = new System.Windows.Forms.DateTimePicker();
            this.groupButton = new System.Windows.Forms.GroupBox();
            this.btnXem = new System.Windows.Forms.Button();
            this.btnFilter = new System.Windows.Forms.Button();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabHeader = new System.Windows.Forms.TabPage();
            this.gridHeader = new System.Windows.Forms.DataGridView();
            this.tabCt = new System.Windows.Forms.TabPage();
            this.gridCt = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripMsg = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupCondition.SuspendLayout();
            this.groupButton.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridHeader)).BeginInit();
            this.tabCt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCt)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupCondition);
            this.splitContainer1.Panel1.Controls.Add(this.groupButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabMain);
            this.splitContainer1.Panel2.Controls.Add(this.statusStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(1061, 561);
            this.splitContainer1.SplitterDistance = 225;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupCondition
            // 
            this.groupCondition.Controls.Add(this.txtSo_Hdon);
            this.groupCondition.Controls.Add(this.txtMa_Ptnx);
            this.groupCondition.Controls.Add(this.txtMa_Kho);
            this.groupCondition.Controls.Add(this.txtMa_Nhom_Vthh);
            this.groupCondition.Controls.Add(this.txtMa_Nhom_Dtgt);
            this.groupCondition.Controls.Add(this.txtMa_Nhom_Dtpn);
            this.groupCondition.Controls.Add(this.txtMa_Vthh);
            this.groupCondition.Controls.Add(this.txtMa_Dtgt);
            this.groupCondition.Controls.Add(this.txtMa_Dtpn);
            this.groupCondition.Controls.Add(this.txtSo_Ctu);
            this.groupCondition.Controls.Add(this.lblSo_Hdon);
            this.groupCondition.Controls.Add(this.lblTen_Ptnx);
            this.groupCondition.Controls.Add(this.lblTen_Kho);
            this.groupCondition.Controls.Add(this.lblTen_Nhom_Vthh);
            this.groupCondition.Controls.Add(this.lblTen_Nhom_Dtgt);
            this.groupCondition.Controls.Add(this.lblTen_Nhom_Dtpn);
            this.groupCondition.Controls.Add(this.lblTen_Vthh);
            this.groupCondition.Controls.Add(this.lblTen_Dtgt);
            this.groupCondition.Controls.Add(this.lblTen_Dtpn);
            this.groupCondition.Controls.Add(this.lblMa_Ptnx);
            this.groupCondition.Controls.Add(this.lblMa_Kho);
            this.groupCondition.Controls.Add(this.lblMa_Nhom_Vthh);
            this.groupCondition.Controls.Add(this.lblMa_Nhom_Dtgt);
            this.groupCondition.Controls.Add(this.lblMa_Nhom_Dtpn);
            this.groupCondition.Controls.Add(this.lblMa_Vthh);
            this.groupCondition.Controls.Add(this.lblMa_Dtgt);
            this.groupCondition.Controls.Add(this.lblMa_Dtpn);
            this.groupCondition.Controls.Add(this.lblSo_Ctu);
            this.groupCondition.Controls.Add(this.cboNX);
            this.groupCondition.Controls.Add(this.cboKySlieu);
            this.groupCondition.Controls.Add(this.lblDen_Ngay);
            this.groupCondition.Controls.Add(this.datDen_Ngay);
            this.groupCondition.Controls.Add(this.lblNX);
            this.groupCondition.Controls.Add(this.lblKySlieu);
            this.groupCondition.Controls.Add(this.lblTu_Ngay);
            this.groupCondition.Controls.Add(this.datTu_Ngay);
            this.groupCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupCondition.Location = new System.Drawing.Point(0, 0);
            this.groupCondition.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupCondition.Name = "groupCondition";
            this.groupCondition.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupCondition.Size = new System.Drawing.Size(921, 225);
            this.groupCondition.TabIndex = 1;
            this.groupCondition.TabStop = false;
            this.groupCondition.Text = "Điều kiện";
            // 
            // txtSo_Hdon
            // 
            this.txtSo_Hdon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSo_Hdon.Location = new System.Drawing.Point(773, 66);
            this.txtSo_Hdon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSo_Hdon.Name = "txtSo_Hdon";
            this.txtSo_Hdon.Size = new System.Drawing.Size(132, 22);
            this.txtSo_Hdon.TabIndex = 5;
            // 
            // txtMa_Ptnx
            // 
            this.txtMa_Ptnx.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Ptnx.Location = new System.Drawing.Point(552, 97);
            this.txtMa_Ptnx.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Ptnx.Name = "txtMa_Ptnx";
            this.txtMa_Ptnx.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Ptnx.TabIndex = 7;
            this.txtMa_Ptnx.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Ptnx.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Ptnx.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Kho
            // 
            this.txtMa_Kho.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Kho.Location = new System.Drawing.Point(97, 98);
            this.txtMa_Kho.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Kho.Name = "txtMa_Kho";
            this.txtMa_Kho.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Kho.TabIndex = 6;
            this.txtMa_Kho.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Kho.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Kho.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Nhom_Vthh
            // 
            this.txtMa_Nhom_Vthh.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Nhom_Vthh.Location = new System.Drawing.Point(552, 192);
            this.txtMa_Nhom_Vthh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Nhom_Vthh.Name = "txtMa_Nhom_Vthh";
            this.txtMa_Nhom_Vthh.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Nhom_Vthh.TabIndex = 13;
            this.txtMa_Nhom_Vthh.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Nhom_Vthh.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Nhom_Vthh.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Nhom_Dtgt
            // 
            this.txtMa_Nhom_Dtgt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Nhom_Dtgt.Location = new System.Drawing.Point(552, 160);
            this.txtMa_Nhom_Dtgt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Nhom_Dtgt.Name = "txtMa_Nhom_Dtgt";
            this.txtMa_Nhom_Dtgt.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Nhom_Dtgt.TabIndex = 11;
            this.txtMa_Nhom_Dtgt.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Nhom_Dtgt.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Nhom_Dtgt.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Nhom_Dtpn
            // 
            this.txtMa_Nhom_Dtpn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Nhom_Dtpn.Location = new System.Drawing.Point(552, 129);
            this.txtMa_Nhom_Dtpn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Nhom_Dtpn.Name = "txtMa_Nhom_Dtpn";
            this.txtMa_Nhom_Dtpn.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Nhom_Dtpn.TabIndex = 9;
            this.txtMa_Nhom_Dtpn.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Nhom_Dtpn.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Nhom_Dtpn.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Vthh
            // 
            this.txtMa_Vthh.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Vthh.Location = new System.Drawing.Point(99, 192);
            this.txtMa_Vthh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Vthh.Name = "txtMa_Vthh";
            this.txtMa_Vthh.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Vthh.TabIndex = 12;
            this.txtMa_Vthh.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Vthh.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Vthh.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Dtgt
            // 
            this.txtMa_Dtgt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Dtgt.Location = new System.Drawing.Point(99, 160);
            this.txtMa_Dtgt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Dtgt.Name = "txtMa_Dtgt";
            this.txtMa_Dtgt.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Dtgt.TabIndex = 10;
            this.txtMa_Dtgt.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Dtgt.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Dtgt.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtMa_Dtpn
            // 
            this.txtMa_Dtpn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMa_Dtpn.Location = new System.Drawing.Point(99, 129);
            this.txtMa_Dtpn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMa_Dtpn.Name = "txtMa_Dtpn";
            this.txtMa_Dtpn.Size = new System.Drawing.Size(132, 22);
            this.txtMa_Dtpn.TabIndex = 8;
            this.txtMa_Dtpn.TextChanged += new System.EventHandler(this.txtMa_TextChanged);
            this.txtMa_Dtpn.DoubleClick += new System.EventHandler(this.txtMa_DoubleClick);
            this.txtMa_Dtpn.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // txtSo_Ctu
            // 
            this.txtSo_Ctu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSo_Ctu.Location = new System.Drawing.Point(551, 65);
            this.txtSo_Ctu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSo_Ctu.Name = "txtSo_Ctu";
            this.txtSo_Ctu.Size = new System.Drawing.Size(132, 22);
            this.txtSo_Ctu.TabIndex = 4;
            // 
            // lblSo_Hdon
            // 
            this.lblSo_Hdon.AutoSize = true;
            this.lblSo_Hdon.Location = new System.Drawing.Point(697, 69);
            this.lblSo_Hdon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSo_Hdon.Name = "lblSo_Hdon";
            this.lblSo_Hdon.Size = new System.Drawing.Size(60, 16);
            this.lblSo_Hdon.TabIndex = 10;
            this.lblSo_Hdon.Text = "Số h.đơn";
            // 
            // lblTen_Ptnx
            // 
            this.lblTen_Ptnx.AutoSize = true;
            this.lblTen_Ptnx.Location = new System.Drawing.Point(696, 101);
            this.lblTen_Ptnx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Ptnx.Name = "lblTen_Ptnx";
            this.lblTen_Ptnx.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Ptnx.TabIndex = 11;
            this.lblTen_Ptnx.Text = ".....";
            // 
            // lblTen_Kho
            // 
            this.lblTen_Kho.AutoSize = true;
            this.lblTen_Kho.Location = new System.Drawing.Point(240, 102);
            this.lblTen_Kho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Kho.Name = "lblTen_Kho";
            this.lblTen_Kho.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Kho.TabIndex = 12;
            this.lblTen_Kho.Text = ".....";
            // 
            // lblTen_Nhom_Vthh
            // 
            this.lblTen_Nhom_Vthh.AutoSize = true;
            this.lblTen_Nhom_Vthh.Location = new System.Drawing.Point(693, 196);
            this.lblTen_Nhom_Vthh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Nhom_Vthh.Name = "lblTen_Nhom_Vthh";
            this.lblTen_Nhom_Vthh.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Nhom_Vthh.TabIndex = 13;
            this.lblTen_Nhom_Vthh.Text = ".....";
            // 
            // lblTen_Nhom_Dtgt
            // 
            this.lblTen_Nhom_Dtgt.AutoSize = true;
            this.lblTen_Nhom_Dtgt.Location = new System.Drawing.Point(693, 164);
            this.lblTen_Nhom_Dtgt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Nhom_Dtgt.Name = "lblTen_Nhom_Dtgt";
            this.lblTen_Nhom_Dtgt.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Nhom_Dtgt.TabIndex = 13;
            this.lblTen_Nhom_Dtgt.Text = ".....";
            // 
            // lblTen_Nhom_Dtpn
            // 
            this.lblTen_Nhom_Dtpn.AutoSize = true;
            this.lblTen_Nhom_Dtpn.Location = new System.Drawing.Point(693, 133);
            this.lblTen_Nhom_Dtpn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Nhom_Dtpn.Name = "lblTen_Nhom_Dtpn";
            this.lblTen_Nhom_Dtpn.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Nhom_Dtpn.TabIndex = 13;
            this.lblTen_Nhom_Dtpn.Text = ".....";
            // 
            // lblTen_Vthh
            // 
            this.lblTen_Vthh.AutoSize = true;
            this.lblTen_Vthh.Location = new System.Drawing.Point(240, 196);
            this.lblTen_Vthh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Vthh.Name = "lblTen_Vthh";
            this.lblTen_Vthh.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Vthh.TabIndex = 13;
            this.lblTen_Vthh.Text = ".....";
            // 
            // lblTen_Dtgt
            // 
            this.lblTen_Dtgt.AutoSize = true;
            this.lblTen_Dtgt.Location = new System.Drawing.Point(240, 164);
            this.lblTen_Dtgt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Dtgt.Name = "lblTen_Dtgt";
            this.lblTen_Dtgt.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Dtgt.TabIndex = 13;
            this.lblTen_Dtgt.Text = ".....";
            // 
            // lblTen_Dtpn
            // 
            this.lblTen_Dtpn.AutoSize = true;
            this.lblTen_Dtpn.Location = new System.Drawing.Point(240, 133);
            this.lblTen_Dtpn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen_Dtpn.Name = "lblTen_Dtpn";
            this.lblTen_Dtpn.Size = new System.Drawing.Size(22, 16);
            this.lblTen_Dtpn.TabIndex = 13;
            this.lblTen_Dtpn.Text = ".....";
            // 
            // lblMa_Ptnx
            // 
            this.lblMa_Ptnx.AutoSize = true;
            this.lblMa_Ptnx.Location = new System.Drawing.Point(465, 106);
            this.lblMa_Ptnx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Ptnx.Name = "lblMa_Ptnx";
            this.lblMa_Ptnx.Size = new System.Drawing.Size(32, 16);
            this.lblMa_Ptnx.TabIndex = 14;
            this.lblMa_Ptnx.Text = "Ptnx";
            // 
            // lblMa_Kho
            // 
            this.lblMa_Kho.AutoSize = true;
            this.lblMa_Kho.Location = new System.Drawing.Point(16, 101);
            this.lblMa_Kho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Kho.Name = "lblMa_Kho";
            this.lblMa_Kho.Size = new System.Drawing.Size(30, 16);
            this.lblMa_Kho.TabIndex = 15;
            this.lblMa_Kho.Text = "Kho";
            // 
            // lblMa_Nhom_Vthh
            // 
            this.lblMa_Nhom_Vthh.AutoSize = true;
            this.lblMa_Nhom_Vthh.Location = new System.Drawing.Point(465, 199);
            this.lblMa_Nhom_Vthh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Nhom_Vthh.Name = "lblMa_Nhom_Vthh";
            this.lblMa_Nhom_Vthh.Size = new System.Drawing.Size(70, 16);
            this.lblMa_Nhom_Vthh.TabIndex = 16;
            this.lblMa_Nhom_Vthh.Text = "Nhóm vthh";
            // 
            // lblMa_Nhom_Dtgt
            // 
            this.lblMa_Nhom_Dtgt.AutoSize = true;
            this.lblMa_Nhom_Dtgt.Location = new System.Drawing.Point(463, 167);
            this.lblMa_Nhom_Dtgt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Nhom_Dtgt.Name = "lblMa_Nhom_Dtgt";
            this.lblMa_Nhom_Dtgt.Size = new System.Drawing.Size(69, 16);
            this.lblMa_Nhom_Dtgt.TabIndex = 16;
            this.lblMa_Nhom_Dtgt.Text = "Nhóm Đtgt";
            // 
            // lblMa_Nhom_Dtpn
            // 
            this.lblMa_Nhom_Dtpn.AutoSize = true;
            this.lblMa_Nhom_Dtpn.Location = new System.Drawing.Point(463, 137);
            this.lblMa_Nhom_Dtpn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Nhom_Dtpn.Name = "lblMa_Nhom_Dtpn";
            this.lblMa_Nhom_Dtpn.Size = new System.Drawing.Size(73, 16);
            this.lblMa_Nhom_Dtpn.TabIndex = 16;
            this.lblMa_Nhom_Dtpn.Text = "Nhóm Đtpn";
            // 
            // lblMa_Vthh
            // 
            this.lblMa_Vthh.AutoSize = true;
            this.lblMa_Vthh.Location = new System.Drawing.Point(16, 194);
            this.lblMa_Vthh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Vthh.Name = "lblMa_Vthh";
            this.lblMa_Vthh.Size = new System.Drawing.Size(33, 16);
            this.lblMa_Vthh.TabIndex = 16;
            this.lblMa_Vthh.Text = "Vthh";
            // 
            // lblMa_Dtgt
            // 
            this.lblMa_Dtgt.AutoSize = true;
            this.lblMa_Dtgt.Location = new System.Drawing.Point(16, 162);
            this.lblMa_Dtgt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Dtgt.Name = "lblMa_Dtgt";
            this.lblMa_Dtgt.Size = new System.Drawing.Size(30, 16);
            this.lblMa_Dtgt.TabIndex = 16;
            this.lblMa_Dtgt.Text = "Đtgt";
            // 
            // lblMa_Dtpn
            // 
            this.lblMa_Dtpn.AutoSize = true;
            this.lblMa_Dtpn.Location = new System.Drawing.Point(16, 132);
            this.lblMa_Dtpn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMa_Dtpn.Name = "lblMa_Dtpn";
            this.lblMa_Dtpn.Size = new System.Drawing.Size(34, 16);
            this.lblMa_Dtpn.TabIndex = 16;
            this.lblMa_Dtpn.Text = "Đtpn";
            // 
            // lblSo_Ctu
            // 
            this.lblSo_Ctu.AutoSize = true;
            this.lblSo_Ctu.Location = new System.Drawing.Point(465, 70);
            this.lblSo_Ctu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSo_Ctu.Name = "lblSo_Ctu";
            this.lblSo_Ctu.Size = new System.Drawing.Size(47, 16);
            this.lblSo_Ctu.TabIndex = 17;
            this.lblSo_Ctu.Text = "Số c.từ";
            // 
            // cboNX
            // 
            this.cboNX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNX.FormattingEnabled = true;
            this.cboNX.Location = new System.Drawing.Point(99, 63);
            this.cboNX.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboNX.Name = "cboNX";
            this.cboNX.Size = new System.Drawing.Size(132, 24);
            this.cboNX.TabIndex = 3;
            // 
            // cboKySlieu
            // 
            this.cboKySlieu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKySlieu.FormattingEnabled = true;
            this.cboKySlieu.Location = new System.Drawing.Point(99, 27);
            this.cboKySlieu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboKySlieu.Name = "cboKySlieu";
            this.cboKySlieu.Size = new System.Drawing.Size(332, 24);
            this.cboKySlieu.TabIndex = 0;
            this.cboKySlieu.SelectedIndexChanged += new System.EventHandler(this.cboKySlieu_SelectedIndexChanged);
            // 
            // lblDen_Ngay
            // 
            this.lblDen_Ngay.AutoSize = true;
            this.lblDen_Ngay.Location = new System.Drawing.Point(709, 31);
            this.lblDen_Ngay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDen_Ngay.Name = "lblDen_Ngay";
            this.lblDen_Ngay.Size = new System.Drawing.Size(30, 16);
            this.lblDen_Ngay.TabIndex = 4;
            this.lblDen_Ngay.Text = "đến";
            // 
            // datDen_Ngay
            // 
            this.datDen_Ngay.CustomFormat = "dd/MM/yyyy";
            this.datDen_Ngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datDen_Ngay.Location = new System.Drawing.Point(773, 28);
            this.datDen_Ngay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.datDen_Ngay.Name = "datDen_Ngay";
            this.datDen_Ngay.Size = new System.Drawing.Size(131, 22);
            this.datDen_Ngay.TabIndex = 2;
            // 
            // lblNX
            // 
            this.lblNX.AutoSize = true;
            this.lblNX.Location = new System.Drawing.Point(16, 66);
            this.lblNX.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNX.Name = "lblNX";
            this.lblNX.Size = new System.Drawing.Size(67, 16);
            this.lblNX.TabIndex = 5;
            this.lblNX.Text = "Nhập xuất";
            // 
            // lblKySlieu
            // 
            this.lblKySlieu.AutoSize = true;
            this.lblKySlieu.Location = new System.Drawing.Point(16, 32);
            this.lblKySlieu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKySlieu.Name = "lblKySlieu";
            this.lblKySlieu.Size = new System.Drawing.Size(56, 16);
            this.lblKySlieu.TabIndex = 5;
            this.lblKySlieu.Text = "Kỳ s.liệu";
            // 
            // lblTu_Ngay
            // 
            this.lblTu_Ngay.AutoSize = true;
            this.lblTu_Ngay.Location = new System.Drawing.Point(465, 32);
            this.lblTu_Ngay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTu_Ngay.Name = "lblTu_Ngay";
            this.lblTu_Ngay.Size = new System.Drawing.Size(56, 16);
            this.lblTu_Ngay.TabIndex = 6;
            this.lblTu_Ngay.Text = "Từ ngày";
            // 
            // datTu_Ngay
            // 
            this.datTu_Ngay.CustomFormat = "dd/MM/yyyy";
            this.datTu_Ngay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datTu_Ngay.Location = new System.Drawing.Point(552, 28);
            this.datTu_Ngay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.datTu_Ngay.Name = "datTu_Ngay";
            this.datTu_Ngay.Size = new System.Drawing.Size(131, 22);
            this.datTu_Ngay.TabIndex = 1;
            // 
            // groupButton
            // 
            this.groupButton.Controls.Add(this.btnXem);
            this.groupButton.Controls.Add(this.btnFilter);
            this.groupButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupButton.Location = new System.Drawing.Point(921, 0);
            this.groupButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupButton.Name = "groupButton";
            this.groupButton.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupButton.Size = new System.Drawing.Size(140, 225);
            this.groupButton.TabIndex = 0;
            this.groupButton.TabStop = false;
            // 
            // btnXem
            // 
            this.btnXem.Location = new System.Drawing.Point(21, 97);
            this.btnXem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(100, 60);
            this.btnXem.TabIndex = 1;
            this.btnXem.Text = "Xem";
            this.btnXem.UseVisualStyleBackColor = true;
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(21, 20);
            this.btnFilter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(100, 66);
            this.btnFilter.TabIndex = 0;
            this.btnFilter.Text = "Lọc";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabHeader);
            this.tabMain.Controls.Add(this.tabCt);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1061, 305);
            this.tabMain.TabIndex = 1;
            // 
            // tabHeader
            // 
            this.tabHeader.Controls.Add(this.gridHeader);
            this.tabHeader.Location = new System.Drawing.Point(4, 25);
            this.tabHeader.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabHeader.Name = "tabHeader";
            this.tabHeader.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabHeader.Size = new System.Drawing.Size(1053, 276);
            this.tabHeader.TabIndex = 0;
            this.tabHeader.Text = "Header";
            this.tabHeader.UseVisualStyleBackColor = true;
            // 
            // gridHeader
            // 
            this.gridHeader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridHeader.Location = new System.Drawing.Point(4, 4);
            this.gridHeader.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridHeader.Name = "gridHeader";
            this.gridHeader.RowHeadersWidth = 62;
            this.gridHeader.Size = new System.Drawing.Size(1045, 268);
            this.gridHeader.TabIndex = 0;
            // 
            // tabCt
            // 
            this.tabCt.Controls.Add(this.gridCt);
            this.tabCt.Location = new System.Drawing.Point(4, 25);
            this.tabCt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCt.Name = "tabCt";
            this.tabCt.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCt.Size = new System.Drawing.Size(1053, 270);
            this.tabCt.TabIndex = 1;
            this.tabCt.Text = "Chi tiết";
            this.tabCt.UseVisualStyleBackColor = true;
            // 
            // gridCt
            // 
            this.gridCt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCt.Location = new System.Drawing.Point(4, 4);
            this.gridCt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridCt.Name = "gridCt";
            this.gridCt.RowHeadersWidth = 62;
            this.gridCt.Size = new System.Drawing.Size(1045, 262);
            this.gridCt.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMsg});
            this.statusStrip1.Location = new System.Drawing.Point(0, 305);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1061, 26);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripMsg
            // 
            this.toolStripMsg.Name = "toolStripMsg";
            this.toolStripMsg.Size = new System.Drawing.Size(95, 20);
            this.toolStripMsg.Text = "toolStripMsg";
            // 
            // FrmVthhListing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 561);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "FrmVthhListing";
            this.Text = "FrmVthhListing";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupCondition.ResumeLayout(false);
            this.groupCondition.PerformLayout();
            this.groupButton.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridHeader)).EndInit();
            this.tabCt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCt)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupCondition;
        private System.Windows.Forms.GroupBox groupButton;
        private System.Windows.Forms.ComboBox cboKySlieu;
        private System.Windows.Forms.Label lblDen_Ngay;
        private System.Windows.Forms.DateTimePicker datDen_Ngay;
        private System.Windows.Forms.Label lblKySlieu;
        private System.Windows.Forms.Label lblTu_Ngay;
        private System.Windows.Forms.DateTimePicker datTu_Ngay;
        private System.Windows.Forms.TextBox txtSo_Hdon;
        private System.Windows.Forms.TextBox txtMa_Ptnx;
        private System.Windows.Forms.TextBox txtMa_Kho;
        private System.Windows.Forms.TextBox txtMa_Dtpn;
        private System.Windows.Forms.TextBox txtSo_Ctu;
        private System.Windows.Forms.Label lblSo_Hdon;
        private System.Windows.Forms.Label lblTen_Ptnx;
        private System.Windows.Forms.Label lblTen_Kho;
        private System.Windows.Forms.Label lblTen_Dtpn;
        private System.Windows.Forms.Label lblMa_Ptnx;
        private System.Windows.Forms.Label lblMa_Kho;
        private System.Windows.Forms.Label lblMa_Dtpn;
        private System.Windows.Forms.Label lblSo_Ctu;
        private System.Windows.Forms.Button btnXem;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DataGridView gridHeader;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabHeader;
        private System.Windows.Forms.TabPage tabCt;
        private System.Windows.Forms.DataGridView gridCt;
        private System.Windows.Forms.TextBox txtMa_Nhom_Vthh;
        private System.Windows.Forms.TextBox txtMa_Nhom_Dtgt;
        private System.Windows.Forms.TextBox txtMa_Nhom_Dtpn;
        private System.Windows.Forms.TextBox txtMa_Vthh;
        private System.Windows.Forms.TextBox txtMa_Dtgt;
        private System.Windows.Forms.Label lblTen_Nhom_Vthh;
        private System.Windows.Forms.Label lblTen_Nhom_Dtgt;
        private System.Windows.Forms.Label lblTen_Nhom_Dtpn;
        private System.Windows.Forms.Label lblTen_Vthh;
        private System.Windows.Forms.Label lblTen_Dtgt;
        private System.Windows.Forms.Label lblMa_Nhom_Vthh;
        private System.Windows.Forms.Label lblMa_Nhom_Dtgt;
        private System.Windows.Forms.Label lblMa_Nhom_Dtpn;
        private System.Windows.Forms.Label lblMa_Vthh;
        private System.Windows.Forms.Label lblMa_Dtgt;
        private System.Windows.Forms.ComboBox cboNX;
        private System.Windows.Forms.Label lblNX;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripMsg;
    }
}