﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public partial class FrmVthh : CnForm
    {
        CnMain cnMain;
        string NX;
        string Table_Name = "VTHH";
        string Table_Name_Ct = "VTHH_CT";
        DataTable Datatable;
        DataTable Datatable_ct;
        DataTable Datatable_filter;
        DataSet Dataset;
        SqlDataAdapter Dataadapter;
        DataMod dataMode = DataMod.None;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;

        public FrmVthh(CnMain cnmain, string NX)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.MdiParent = this.cnMain.mainForm;
            this.NX = NX;
            this.LoadData();
            this.LoadDanhMuc();
            this.SetControls();
            this.BinHeadControl();
            this.BinGrid();
            this.ConfigGrid();
            this.FilterList();
            this.ConfigAction();
            this.LoadContextMenu();
        }
        public FrmVthh(CnMain cnmain, string NX, decimal pk)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.MdiParent = this.cnMain.mainForm;
            this.NX = NX;
            this.LoadData();
            this.LoadDanhMuc();
            this.SetControls();
            this.BinHeadControl();
            this.BinGrid();
            this.ConfigGrid();
            this.FilterList();
            this.ConfigAction();
            this.LoadDataWithPk(pk);
            this.dataMode = DataMod.Edit;
            this.ConfigAction();
            this.LoadContextMenu();
        }
        private void LoadData()
        {
            this.Dataset = new DataSet();

            this.Dataadapter = new SqlDataAdapter("SELECT * FROM " + this.Table_Name + " WHERE 1=0", this.cnMain.sqlConnection);
            this.Dataadapter.Fill(this.Dataset, this.Table_Name);
            this.Datatable = this.Dataset.Tables[this.Table_Name];

            this.Dataadapter = new SqlDataAdapter("SELECT * FROM " + this.Table_Name_Ct + " WHERE 1=0", this.cnMain.sqlConnection);
            this.Dataadapter.Fill(this.Dataset, this.Table_Name_Ct);
            this.Datatable_ct = this.Dataset.Tables[this.Table_Name_Ct];

        }
        private void LoadDataWithPk(decimal pk)
        {
            this.Datatable.Clear();
            this.Datatable_ct.Clear();

            this.Dataadapter = new SqlDataAdapter("SELECT * FROM " + this.Table_Name + " WHERE PK = " + pk, this.cnMain.sqlConnection);
            this.Dataadapter.Fill(this.Dataset, this.Table_Name);
            this.Datatable = this.Dataset.Tables[this.Table_Name];

            this.Dataadapter = new SqlDataAdapter("SELECT * FROM " + this.Table_Name_Ct + " WHERE FK IN (SELECT PK FROM VTHH WHERE PK="+pk+")", this.cnMain.sqlConnection);
            this.Dataadapter.Fill(this.Dataset, this.Table_Name_Ct);
            this.Datatable_ct = this.Dataset.Tables[this.Table_Name_Ct];

            this.dataMode = DataMod.Edit;
            this.ConfigAction();

        }
        private void LoadDanhMuc()
        {
            this.cnMain.LoadDatatable(this.Dataset, "DM_KHO");
            this.cnMain.LoadDatatable(this.Dataset, "DM_PTNX","NX = '"+this.NX+"'");
            this.cnMain.LoadDatatable(this.Dataset, "DM_DTPN");
            this.cnMain.LoadDatatable(this.Dataset, "DM_VTHH");
            this.cnMain.LoadDatatable(this.Dataset, "DM_DTGT");
        }
        private void RefreshDanhMuc()
        {
            this.LoadDanhMuc();
        }
        
        private void FilterList()
        {
            string query = "SELECT * FROM " + this.Table_Name + " WHERE NX = '"+this.NX+"'";
            if(this.datTu_Ngay.Value != null)
            {
                query += " AND NGAY_CTU >= " + Functions.ParseDate(this.datTu_Ngay.Value);
            }
            if (this.datDen_Ngay.Value != null)
            {
                query += " AND NGAY_CTU <= " + Functions.ParseDate(this.datDen_Ngay.Value);
            }
            if (this.txtMa_Kho_Filter.Text != "")
            {
                query += " AND MA_KHO = '" + this.txtMa_Kho_Filter.Text + "'";
            }
            if (this.txtMa_Ptnx_Filter.Text != "")
            {
                query += " AND MA_PTNX = '" + this.txtMa_Ptnx_Filter.Text + "'";
            }
            if (this.txtMa_Dtpn_Filter.Text != "")
            {
                query += " AND MA_DTPN = '" + this.txtMa_Dtpn_Filter.Text + "'";
            }
            this.Dataadapter = new SqlDataAdapter(query, this.cnMain.sqlConnection);
            this.Datatable_filter = new DataTable();
            this.Dataadapter.Fill(this.Datatable_filter);
            this.BinGridFilter();
        }
        private void BinHeadControl()
        {
            this.txtSo_Ctu.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".SO_CTU",false,DataSourceUpdateMode.OnPropertyChanged));
            this.datNgay_Ctu.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".NGAY_CTU"));
            this.txtSo_Hdon.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".SO_HDON"));
            this.txtNgay_Hdon.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".NGAY_HDON"));
            this.txtMa_Kho.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".MA_KHO"));
            this.txtMa_Ptnx.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".MA_PTNX"));
            this.txtMa_Dtpn.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".MA_DTPN"));
            this.txtHo_Ten.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".HO_TEN"));
            this.txtDien_Giai.DataBindings.Add(new Binding("Text", this.Dataset, this.Table_Name + ".DIEN_GIAI"));
        }
        
        private void BinGrid()
        {
            this.grid.AutoGenerateColumns = false;
            this.grid.DataSource = this.Dataset;
            this.grid.DataMember = this.Table_Name_Ct;
            Functions.SetGridTextColumn(this.grid, "MA_VTHH", "Mã vthh",100);
            Functions.SetGridTextColumn(this.grid, "TEN_VTHH", "Tên vthh",200,false);
            Functions.SetGridTextColumn(this.grid, "DVT", "Đvt",50,false);
            Functions.SetGridNumberColumn(this.grid, "SO_LUONG", "Số lượng",100, this.Datatable_ct.Columns["SO_LUONG"].DataType, 2);
            Functions.SetGridNumberColumn(this.grid, "GIA_GOC", "Giá gốc", 100, this.Datatable_ct.Columns["GIA_GOC"].DataType, 2);
            Functions.SetGridNumberColumn(this.grid, "TIEN_GOC", "Tiền gốc", 100, this.Datatable_ct.Columns["TIEN_GOC"].DataType, 0);
            Functions.SetGridNumberColumn(this.grid, "THUE_SUAT_VAT", "Thuế suất vat", 100, this.Datatable_ct.Columns["THUE_SUAT_VAT"].DataType, 2);
            Functions.SetGridNumberColumn(this.grid, "TIEN_THUE_VAT", "Tiền thuế vat", 100, this.Datatable_ct.Columns["TIEN_THUE_VAT"].DataType, 0);
            Functions.SetGridNumberColumn(this.grid, "TONG_TIEN", "Tổng tiền", 100, this.Datatable_ct.Columns["TONG_TIEN"].DataType, 0);
            Functions.SetGridNumberColumn(this.grid, "GIA_VON", "Giá vốn", 100, this.Datatable_ct.Columns["GIA_VON"].DataType, 2);
            Functions.SetGridNumberColumn(this.grid, "TIEN_VON", "Tiền vốn", 100, this.Datatable_ct.Columns["TIEN_VON"].DataType, 0);
            Functions.SetGridTextColumn(this.grid, "MA_DTGT", "Đtgt", 100);
            Functions.SetGridTextColumn(this.grid, "GHI_CHU", "Ghi chú", 200);
            Functions.SetGridNumberColumn(this.grid, "STT", "Stt", 50, this.Datatable_ct.Columns["STT"].DataType, 0);
        }
        private void BinGridFilter()
        {
            this.gridFilter.SelectionChanged -= new System.EventHandler(this.gridFilter_SelectionChanged);
            this.gridFilter.Columns.Clear();
            this.gridFilter.AutoGenerateColumns = false;
            this.gridFilter.DataSource = this.Datatable_filter;
            Functions.SetGridTextColumn(this.gridFilter, "SO_CTU", "Số c.từ", 100);
            Functions.SetGridDateColumn(this.gridFilter, "NGAY_CTU", "Ngày c.từ", 100);
            Functions.SetGridTextColumn(this.gridFilter, "SO_HDON", "Số h.đơn", 100);
            Functions.SetGridDateColumn(this.gridFilter, "NGAY_HDON", "Ngày h.đơn", 100);
            Functions.SetGridTextColumn(this.gridFilter, "MA_KHO", "Mã kho", 100);
            Functions.SetGridTextColumn(this.gridFilter, "MA_PTNX", "Mã ptnx", 100);
            Functions.SetGridTextColumn(this.gridFilter, "MA_DTPN", "Mã đtpn", 100);
            Functions.SetGridNumberColumn(this.gridFilter, "PK", "", 50, this.Datatable_filter.Columns["PK"].DataType, 0);
            this.FocusRowGridFilter();
            this.gridFilter.SelectionChanged += new System.EventHandler(this.gridFilter_SelectionChanged);
        }
        private void FocusRowGridFilter()
        {
            if (this.Datatable.Rows.Count == 0) return;
            decimal pk = (decimal)this.Datatable.Rows[0]["PK"];
            foreach(DataGridViewRow r in this.gridFilter.Rows)
            {
                if((decimal)r.Cells["PK"].Value == pk)
                {
                    this.gridFilter.ClearSelection();
                    r.Selected = true;
                    break;
                }
            }
        }
        private void ConfigGrid()
        {
            this.grid.Columns["TEN_VTHH"].ReadOnly = true;
            this.grid.Columns["DVT"].ReadOnly = true;
            this.gridFilter.AllowUserToAddRows = false;
            this.gridFilter.ReadOnly = true;
            this.grid.CellLeave += grid_CellLeave;
            this.grid.CellDoubleClick += grid_CellDoubleClick;
            this.grid.KeyDown += grid_KeyDown;
            this.grid.CellFormatting += grid_CellFormatting;
            this.grid.RowPostPaint += grid_RowPostPaint;
            this.grid.CellEndEdit += grid_CellEndEdit;
            this.grid.CurrentCellChanged += grid_CurrentCellChanged;
            this.grid.RowsAdded += this.grid_RowsAdded;
        }

        void grid_CurrentCellChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (this.grid.CurrentCell != null)
                {
                    string columnname = this.grid.CurrentCell.OwningColumn.Name;
                    switch (columnname)
                    {
                        case "MA_DTGT":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_DTGT"], this.grid.CurrentCell.Value.ToString(), "TEN_DTGT").ToString();
                            break;
                        default:
                            this.toolStripMsg.Text = "";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.grid.Refresh();
        }
        void grid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                var grid = sender as DataGridView;
                var rowIdx = (e.RowIndex + 1).ToString();

                var centerFormat = new StringFormat()
                {
                    // right alignment might actually make more sense for numbers
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };

                var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
                e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += "./r/n " + ex.InnerException;
                }
                if (ex.StackTrace != null)
                {
                    msg += "./r/n " + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }

        void grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0 || e.ColumnIndex < 0) return;
                if (e.RowIndex == this.grid.NewRowIndex)
                {
                    if (this.grid.Columns[e.ColumnIndex].ValueType == typeof(System.Decimal))
                    {
                        e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                        e.CellStyle.ForeColor = Color.Blue;
                        decimal sum = 0;
                        for (int i = 0; i < this.grid.NewRowIndex; i++)
                        {
                            var value = this.grid[e.ColumnIndex, i].Value;
                            if (value != null)
                            {
                                sum += (decimal)this.grid[e.ColumnIndex, i].Value;
                            }
                        }
                        e.Value = Math.Round(sum, 2);
                    }
                }
                else
                {
                    string colname = this.grid.Columns[e.ColumnIndex].Name;
                    DataRow fr;
                    switch(colname)
                    {
                        case "TEN_VTHH":
                            fr = this.Dataset.Tables["DM_VTHH"].Rows.Find(this.grid.Rows[e.RowIndex].Cells["MA_VTHH"].Value);
                            if (fr != null) e.Value = fr["TEN_VTHH"];
                            else e.Value = "";
                            break;
                        case "DVT":
                            fr = this.Dataset.Tables["DM_VTHH"].Rows.Find(this.grid.Rows[e.RowIndex].Cells["MA_VTHH"].Value);
                            if (fr != null) e.Value = fr["DVT"];
                            else e.Value = "";
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        void grid_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Return)
                {
                    e.SuppressKeyPress = true;
                    int iColumn = this.grid.CurrentCell.ColumnIndex;
                    int iRow = this.grid.CurrentCell.RowIndex;
                    if (iColumn == this.grid.ColumnCount - 1)
                    {
                        if (this.grid.RowCount > (iRow + 1))
                        {
                            this.grid.CurrentCell = this.grid[0, iRow + 1];
                        }
                        else
                        {
                            //focus next control
                        }
                    }
                    else
                        this.grid.CurrentCell = this.grid[iColumn + 1, iRow];
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        

        private void NewRecord()
        {
            if (this.CheckChange())
            {
                this.Datatable.Clear();
                this.Datatable_ct.Clear();
                DataRow row = this.AddNewVthh();
                decimal pk = (decimal)row["PK"];
                //this.AddNewVthh_Ct(pk);
                this.dataMode = DataMod.New;
                this.ConfigAction();
                this.txtSo_Ctu.Focus();
            }
        }
        private DataRow AddNewVthh()
        {
            DataRow nr = this.Datatable.NewRow();
            foreach (DataColumn c in this.Datatable.Columns)
            {
                if (c.ColumnName == "PK")
                {
                    nr[c.ColumnName] = this.cnMain.GetPk(this.Table_Name);
                }
                else
                {
                    if (c.DataType == typeof(System.DateTime)) nr[c.ColumnName] = DBNull.Value;
                    else if (c.DataType == typeof(System.String)) nr[c.ColumnName] = string.Empty;
                    else nr[c.ColumnName] = 0;
                }
            }
            nr["NGAY_CTU"] = DateTime.Today;
            nr["NX"] = this.NX;
            this.Datatable.Rows.Add(nr);
            return nr;
        }
        private DataRow AddNewVthh_Ct(decimal pkVthh)
        {
            DataRow nr = this.Datatable_ct.NewRow();
            foreach (DataColumn c in this.Datatable_ct.Columns)
            {
                if (c.ColumnName == "PK")
                {
                    nr[c.ColumnName] = this.cnMain.GetPk(this.Table_Name);
                }
                else
                {
                    if (c.DataType == typeof(System.DateTime)) nr[c.ColumnName] = DBNull.Value;
                    else if (c.DataType == typeof(System.String)) nr[c.ColumnName] = string.Empty;
                    else nr[c.ColumnName] = 0;
                }
                nr["FK"] = pkVthh;
            }
            this.Datatable_ct.Rows.Add(nr);
            return nr;
        }
        private void CopyRecord()
        {
            if(this.Dataset.GetChanges() != null)
            {
                DialogResult result = MessageBox.Show("Dữ liệu có thay đổi, bạn có muốn lưu trước khi copy?", "Thông báo", MessageBoxButtons.YesNoCancel);
                if(result == System.Windows.Forms.DialogResult.Yes)
                {
                    this.UpdateRecord();
                }
                else if(result == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                this.Dataset.RejectChanges();
            }
            DataSet ds = this.Dataset.Copy();
            this.Datatable.Clear();
            this.Datatable_ct.Clear();
            DataRow nr = this.Datatable.NewRow();
            nr.ItemArray = (object[])ds.Tables[this.Table_Name].Rows[0].ItemArray.Clone();
            decimal pk = this.cnMain.GetPk(this.Table_Name);
            nr["PK"] = pk;
            nr["SO_CTU"] = "";
            this.Datatable.Rows.Add(nr);
            foreach (DataRow rCt in ds.Tables[this.Table_Name_Ct].Rows)
            {
                DataRow nrCt = this.Datatable_ct.NewRow();
                nrCt.ItemArray = (object[])rCt.ItemArray.Clone();
                decimal pkCt = this.cnMain.GetPk(this.Table_Name_Ct);
                nrCt["PK"] = pkCt;
                nrCt["FK"] = pk;
                this.Datatable_ct.Rows.Add(nrCt);
            }
            this.dataMode = DataMod.New;
            this.ConfigAction();
            this.txtSo_Ctu.Focus();
        }
        private void DeleteRecord()
        {
            this.txtSo_Ctu.Focus();
            DialogResult result = MessageBox.Show("Bạn có muốn xóa chứng từ này?", "Thông báo", MessageBoxButtons.YesNoCancel);
            if(result == System.Windows.Forms.DialogResult.Yes)
            {
                Functions.DeleteDatatable(this.Datatable);
                Functions.DeleteDatatable(this.Datatable_ct);
                this.UpdateRecord();
            }
            this.dataMode = DataMod.None;
        }
        private void RestoreRecord()
        {
            this.txtSo_Ctu.Focus();
            this.Datatable.Rows[0].EndEdit();
            this.Datatable.RejectChanges();
            this.Datatable_ct.RejectChanges();
            this.EndEdit();
            if (this.dataMode == DataMod.New)
            {
                this.dataMode = DataMod.None;
            }
            else
            {
                this.dataMode = DataMod.Edit;
            }
            this.ConfigAction();
        }
        private void UpdateRecord()
        {
            this.txtSo_Ctu.Focus();
            this.EndEdit();
            DataSet ds = this.Dataset.GetChanges();
            string field_id = "PK";
            SqlTransaction tran = this.cnMain.sqlConnection.BeginTransaction();
            try
            {
                this.Dataadapter.InsertCommand = Functions.CreateInsertCommand(this.Table_Name, this.Datatable, this.cnMain.sqlConnection, "");
                this.Dataadapter.InsertCommand.Transaction = tran;
                this.Dataadapter.UpdateCommand = Functions.CreateUpdateCommand(this.Table_Name, this.Datatable, this.cnMain.sqlConnection, field_id, "");
                this.Dataadapter.UpdateCommand.Transaction = tran;
                this.Dataadapter.DeleteCommand = Functions.CreateDeleteCommand(this.Table_Name, this.Datatable, this.cnMain.sqlConnection, field_id);
                this.Dataadapter.DeleteCommand.Transaction = tran;
                this.Dataadapter.Update(this.Dataset,this.Table_Name);

                this.Dataadapter.InsertCommand = Functions.CreateInsertCommand(this.Table_Name_Ct, this.Datatable_ct, this.cnMain.sqlConnection, "");
                this.Dataadapter.InsertCommand.Transaction = tran;
                this.Dataadapter.UpdateCommand = Functions.CreateUpdateCommand(this.Table_Name_Ct, this.Datatable_ct, this.cnMain.sqlConnection, field_id, "");
                this.Dataadapter.UpdateCommand.Transaction = tran;
                this.Dataadapter.DeleteCommand = Functions.CreateDeleteCommand(this.Table_Name_Ct, this.Datatable_ct, this.cnMain.sqlConnection, field_id);
                this.Dataadapter.DeleteCommand.Transaction = tran;
                this.Dataadapter.Update(this.Dataset, this.Table_Name_Ct);

                tran.Commit();
                this.FilterList();
                this.dataMode = DataMod.Edit;
                this.ConfigAction();
            }
            catch(Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
        }
        private void RefreshRecord()
        {
            this.RefreshDanhMuc();
        }
        private void FindRecord()
        {

        }
        private void ExportRecord()
        {

        }
        private void PrintRecord()
        {
            DataSet ds = this.Dataset.Copy();
            DataTable dtVthh_ct = ds.Tables["VTHH_CT"];
            DataColumn c = new DataColumn("TEN_VTHH", typeof(System.String));
            c.DefaultValue = "";
            dtVthh_ct.Columns.Add(c);
            c = new DataColumn("DVT", typeof(System.String));
            c.DefaultValue = "";
            dtVthh_ct.Columns.Add(c);
            foreach(DataRow r in dtVthh_ct.Rows)
            {
                r["TEN_VTHH"] = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_VTHH"], r["MA_VTHH"].ToString(), "TEN_VTHH");
                r["DVT"] = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_VTHH"], r["MA_VTHH"].ToString(), "DVT");
            }
            string fileName = "";
            if (this.NX == "N") fileName = "Phieu_Nhap.rdlc";
            else fileName = "Phieu_Xuat.rdlc";
            FrmPhieuOutput frm = new FrmPhieuOutput(this.cnMain, ds, fileName);
            frm.Show();
        }
        private void CloseRecord()
        {
            this.Close();
        }
        private DataRow CopyDetail()
        {
            DataRow nr = null;
            if (this.grid.CurrentRow != null && this.grid.CurrentRow.DataBoundItem != null)
            {
                DataRow r = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
                nr = this.Datatable.NewRow();
                nr.ItemArray = (object[])r.ItemArray.Clone();
                if (this.Datatable.Columns.Contains("PK"))
                {
                    nr["PK"] = this.cnMain.GetPk(this.Table_Name);
                }
                this.Datatable.Rows.Add(nr);
            }

            BindingManagerBase bm = this.grid.BindingContext[this.grid.DataSource, this.grid.DataMember];
            bm.Position = this.grid.RowCount;
            return nr;
        }
        private void DeleteDetail()
        {
            if (this.grid.CurrentRow.DataBoundItem != null)
            {
                if (this.grid.SelectedRows.Count > 0)
                {
                    foreach (DataGridViewRow grvr in this.grid.SelectedRows)
                    {
                        DataRow r = ((DataRowView)grvr.DataBoundItem).Row;
                        r.Delete();
                    }
                }
                else if (this.grid.CurrentRow != null)
                {
                    DataRow r = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
                    r.Delete();
                }
            }
        }
        protected void EndEdit()
        {
            BindingManagerBase bm = this.BindingContext[this.Dataset, this.Table_Name];
            bm.EndCurrentEdit();
            foreach (DataRow r in this.Datatable.Rows)
            {
                r.EndEdit();
            }

            this.grid.EndEdit();
            foreach (DataRow r in this.Datatable_ct.Rows)
            {
                r.EndEdit();
            }
        }

        private void toolbar_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripButton item = sender as ToolStripButton;
                switch (item.Name.ToLower())
                {
                    case "toolnew":
                        this.NewRecord();
                        break;
                    case "toolcopy":
                        this.CopyRecord();
                        break;
                    case "tooldelete":
                        this.DeleteRecord();
                        break;
                    case "toolrestore":
                        this.RestoreRecord();
                        break;
                    case "toolupdate":
                        this.UpdateRecord();
                        break;
                    case "toolrefresh":
                        this.RefreshRecord();
                        break;
                    case "toolfind":
                        this.FindRecord();
                        break;
                    case "toolexport":
                        this.ExportRecord();
                        break;
                    case "toolprint":
                        this.PrintRecord();
                        break;
                    case "toolclose":
                        this.CloseRecord();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if(ex.InnerException != null)
                {
                    msg += ". " + ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }

        private void LoadContextMenu()
        {
            this.contextMenuStrip = new ContextMenuStrip();
            System.Windows.Forms.ToolStripMenuItem deleteitem = new ToolStripMenuItem();
            deleteitem.Name = "delete";
            deleteitem.Text = "Xóa dòng";
            deleteitem.Click += ContextMenuItem_Click;
            this.contextMenuStrip.Items.Add(deleteitem);

            System.Windows.Forms.ToolStripMenuItem copyitem = new ToolStripMenuItem();
            copyitem.Name = "copy";
            copyitem.Text = "Copy dòng";
            copyitem.Click += ContextMenuItem_Click;
            this.contextMenuStrip.Items.Add(copyitem);

            this.grid.ContextMenuStrip = this.contextMenuStrip;
        }

        private void ContextMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem item = sender as ToolStripMenuItem;
                switch (item.Name.ToLower())
                {
                    case "delete":
                        this.DeleteDetail();
                        break;
                    case "copy":
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ". " + ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }

        private void grid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                if (this.grid.CurrentRow != null && this.grid.CurrentRow.DataBoundItem != null)
                {
                    DataRow r = ((DataRowView)this.grid.CurrentRow.DataBoundItem).Row;
                    foreach (DataColumn c in this.Datatable_ct.Columns)
                    {
                        if (r[c.ColumnName] == DBNull.Value)
                        {
                            if (c.ColumnName == "PK")
                            {
                                r[c.ColumnName] = this.cnMain.GetPk(this.Table_Name);
                            }
                            else if (c.ColumnName == "FK")
                            {
                                r[c.ColumnName] = this.Datatable.Rows[0]["PK"];
                            }
                            else if (c.ColumnName == "STT")
                            {
                                int stt = 1;
                                object oj = this.Datatable_ct.Compute("Max(STT)", "");
                                if(oj != null && oj != DBNull.Value)
                                {
                                    stt = Convert.ToInt32(oj) + 1;
                                }
                                r[c.ColumnName] = stt;
                            }
                            else
                            {
                                if (c.DataType == typeof(System.DateTime)) r[c.ColumnName] = DBNull.Value;
                                else if (c.DataType == typeof(System.String)) r[c.ColumnName] = string.Empty;
                                else r[c.ColumnName] = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (!this.CheckChange())
            {
                e.Cancel = true;
            }
            base.OnClosing(e);
        }
        private bool CheckChange()
        {
            this.EndEdit();
            bool ok = true;
            if (this.Dataset.GetChanges() != null)
            {
                DialogResult result = MessageBox.Show("Bạn có muốn lưu thay đổi trước khi thoát?", "Thông báo", MessageBoxButtons.YesNoCancel);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    this.UpdateRecord();
                }
                else if (result == System.Windows.Forms.DialogResult.Cancel)
                {
                    ok = false;
                }
            }
            return ok;
        }
        private void SetControls()
        {
            List<KySlieu> kyslieulist = Functions.LoadKySlieu((int)this.cnMain.GetSystemVar("WORKING_YEAR"), true);
            foreach (KySlieu kyslieu in kyslieulist)
            {
                this.cboKySlieu.Items.Add(kyslieu);
            }
            this.cboKySlieu.SelectedIndex = 0;
            this.splitContainer1.FixedPanel = FixedPanel.Panel2;

            if (this.NX == "N") this.Text = "Nhập kho";
            else this.Text = "Xuất kho";
        }

        
        private void ConfigAction()
        {
            switch(this.dataMode)
            {
                case DataMod.None:
                    this.ToolNew.Enabled = true;
                    this.ToolCopy.Enabled = false;
                    this.ToolRestore.Enabled = false;
                    this.ToolDelete.Enabled = false;
                    this.ToolUpdate.Enabled = false;
                    this.ToolPrint.Enabled = false;
                    this.palTop.Enabled = false;
                    this.grid.ReadOnly = true;
                    this.palBottom.Enabled = false;
                    break;
                case DataMod.New:
                    this.ToolNew.Enabled = false;
                    this.ToolCopy.Enabled = false;
                    this.ToolRestore.Enabled = true;
                    this.ToolDelete.Enabled = true;
                    this.ToolUpdate.Enabled = true;
                    this.ToolPrint.Enabled = true;
                    this.palTop.Enabled = true;
                    this.grid.ReadOnly = false;
                    this.palBottom.Enabled = true;
                    break;
                case DataMod.Edit:
                    this.ToolNew.Enabled = true;
                    this.ToolCopy.Enabled = true;
                    this.ToolRestore.Enabled = true;
                    this.ToolDelete.Enabled = true;
                    this.ToolUpdate.Enabled = true;
                    this.ToolPrint.Enabled = true;
                    this.palTop.Enabled = true;
                    this.grid.ReadOnly = false;
                    this.palBottom.Enabled = true;
                    break;
                default:
                    break;
            }
        }
        private void btnFilter_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.FilterList();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cboKySlieu_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (this.cboKySlieu.SelectedIndex < 0) return;
                KySlieu kyslieu = (KySlieu)this.cboKySlieu.SelectedItem;
                this.datTu_Ngay.Value = kyslieu.Tu_Ngay;
                this.datDen_Ngay.Value = kyslieu.Den_Ngay;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridFilter_SelectionChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (this.gridFilter != null && this.gridFilter.CurrentRow != null && this.gridFilter.CurrentRow.DataBoundItem != null)
                {
                    DataRow r = ((DataRowView)this.gridFilter.CurrentRow.DataBoundItem).Row;
                    decimal pk = (decimal)r["PK"];
                    this.LoadDataWithPk(pk);
                    this.dataMode = DataMod.Edit;
                    this.ConfigAction();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void grid_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.txtSo_Ctu.Focused) return;
                if (this.grid.CurrentRow.DataBoundItem == null || this.grid.EditingControl == null || this.Datatable_ct.Columns.IndexOf(this.grid.Columns[e.ColumnIndex].Name)<0) return;
                string colname = this.grid.Columns[e.ColumnIndex].Name;
                DataGridViewTextBoxEditingControl textbox = (DataGridViewTextBoxEditingControl)(sender as DataGridView).EditingControl;
                DataRowView row = (DataRowView)this.grid.CurrentRow.DataBoundItem;
                row[this.grid.Columns[e.ColumnIndex].Name] = textbox.EditingControlFormattedValue;
                switch(colname)
                {
                    case "MA_VTHH":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_VTHH", textbox);
                        this.grid.EndEdit();
                        this.GetGiaVon(row);
                        break;
                    case "MA_DTGT":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_DTGT", textbox);
                        break;
                    case "SO_LUONG":
                        row["TIEN_GOC"] = Math.Round((decimal)row["SO_LUONG"] * (decimal)row["GIA_GOC"],0);
                        row["TIEN_VON"] = Math.Round((decimal)row["SO_LUONG"] * (decimal)row["GIA_VON"], 0);
                        this.TinhTienThueVat(row);
                        this.TinhTongTien(row);
                        row.EndEdit();
                        break;
                    case "GIA_GOC":
                        row["TIEN_GOC"] = Math.Round((decimal)row["SO_LUONG"] * (decimal)row["GIA_GOC"], 0);
                        if(this.NX == "N")
                        {
                            row["GIA_VON"] = row["GIA_GOC"];
                            row["TIEN_VON"] = row["TIEN_GOC"];
                        }
                        this.TinhTienThueVat(row);
                        this.TinhTongTien(row);
                        row.EndEdit();
                        break;
                    case "TIEN_GOC":
                        if ((decimal)row["GIA_GOC"] == 0 && (decimal)row["SO_LUONG"] != 0)
                        {
                            row["GIA_GOC"] = Math.Round((decimal)row["TIEN_GOC"]/(decimal)row["SO_LUONG"], 2);
                        }
                        if (this.NX == "N")
                        {
                            row["GIA_VON"] = row["GIA_GOC"];
                            row["TIEN_VON"] = row["TIEN_GOC"];
                        }
                        this.TinhTienThueVat(row);
                        this.TinhTongTien(row);
                        row.EndEdit();
                        break;
                    case "THUE_SUAT_VAT":
                        this.TinhTienThueVat(row);
                        this.TinhTongTien(row);
                        row.EndEdit();
                        break;
                    case "GIA_VON":
                        row["TIEN_VON"] = Math.Round((decimal)row["SO_LUONG"] * (decimal)row["GIA_VON"], 0);
                        row.EndEdit();
                        break;
                    default:
                        break;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!(this.grid.EditingControl is DataGridViewTextBoxEditingControl))
                {
                    return;
                }
                if (this.grid.CurrentRow.DataBoundItem == null || this.grid.EditingControl == null || this.Datatable_ct.Columns.IndexOf(this.grid.Columns[e.ColumnIndex].Name) < 0) return;
                string colname = this.grid.Columns[e.ColumnIndex].Name;
                DataGridViewTextBoxEditingControl textbox = (DataGridViewTextBoxEditingControl)(sender as DataGridView).EditingControl;
                DataRowView row = (DataRowView)this.grid.CurrentRow.DataBoundItem;
                row[this.grid.Columns[e.ColumnIndex].Name] = textbox.EditingControlFormattedValue;
                switch (colname)
                {
                    case "MA_VTHH":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_VTHH", textbox, true);
                        this.grid.EndEdit();
                        this.GetGiaVon(row);
                        break;
                    case "MA_DTGT":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_DTGT", textbox, true);
                        this.grid.EndEdit();
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void TinhTienThueVat(DataRowView row)
        {
            row["TIEN_THUE_VAT"] = Math.Round((decimal)row["TIEN_GOC"] * (decimal)row["THUE_SUAT_VAT"]/100,0);
            row.EndEdit();
        }
        private void TinhTongTien(DataRowView row)
        {
            row["TONG_TIEN"] = (decimal)row["TIEN_GOC"] + (decimal)row["TIEN_THUE_VAT"];
            row.EndEdit();
        }

        private void txtMa_Leave(object sender, System.EventArgs e)
        {
            try
            {
                TextBox texbox = sender as TextBox;
                if (texbox.Equals(this.txtMa_Kho) || texbox.Equals(this.txtMa_Kho_Filter)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_KHO", texbox);
                else if (texbox.Equals(this.txtMa_Ptnx) || texbox.Equals(this.txtMa_Ptnx_Filter)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_PTNX", texbox);
                else if (texbox.Equals(this.txtMa_Dtpn) || texbox.Equals(this.txtMa_Dtpn_Filter)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_DTPN", texbox);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void txtMa_DoubleClick(object sender, System.EventArgs e)
        {
            try
            {
                TextBox texbox = sender as TextBox;
                if (texbox.Equals(this.txtMa_Kho) || texbox.Equals(this.txtMa_Kho_Filter)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_KHO", texbox, true);
                else if (texbox.Equals(this.txtMa_Ptnx) || texbox.Equals(this.txtMa_Ptnx_Filter)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_PTNX", texbox, true);
                else if (texbox.Equals(this.txtMa_Dtpn) || texbox.Equals(this.txtMa_Dtpn_Filter)) this.cnMain.ProcessTudienControl(this.Dataset, "DM_DTPN", texbox, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            //MessageBox.Show(keyData.ToString());
            if (keyData == Keys.Return)
            {
                SendKeys.Send("{TAB}");
            }
            return base.ProcessDialogKey(keyData);
        }

        private void GetGiaVon(DataRowView rowCt)
        {
            if (this.Datatable.Rows.Count == 0 || this.NX == "N") return;
            DataRow rowHead = this.Datatable.Rows[0];
            decimal pk = (decimal)rowHead["PK"];
            DateTime ngay_ctu = (DateTime)rowHead["NGAY_CTU"];
            string ma_kho = (string)rowHead["MA_KHO"];
            string ma_vthh = rowCt["MA_VTHH"].ToString();
            string query = "SELECT GIA_VON,SUM(SO_LUONG) AS SO_LUONG FROM VTHH_TOTAL WHERE NX <> 'X' AND NGAY_CTU <= " + Functions.ParseDate(ngay_ctu) + " AND MA_KHO = '" + ma_kho + "' AND MA_VTHH = '" + ma_vthh + "' GROUP BY GIA_VON";
            DataTable dtTon = this.cnMain.LoadDatatableQuery(query, "TON");
            dtTon.PrimaryKey = new DataColumn[] { dtTon.Columns["MA_VTHH"], dtTon.Columns["GIA_VON"] };
            query = "SELECT MA_VTHH,GIA_VON,SUM(SO_LUONG) AS SO_LUONG FROM VTHH_TOTAL WHERE NX = 'X' AND NGAY_CTU <= " + Functions.ParseDate(ngay_ctu) + " AND MA_KHO = '" + ma_kho + "' AND MA_VTHH = '" + ma_vthh + "' AND PK_CTU <> "+pk+" GROUP BY MA_VTHH,GIA_VON";
            DataTable dtXuat = this.cnMain.LoadDatatableQuery(query, "XUAT");
            foreach(DataRow r in dtXuat.Rows)
            {
                DataRow fr = dtTon.Rows.Find(r["GIA_VON"]);
                if(fr != null)
                {
                    fr["SO_LUONG"] = (decimal)fr["SO_LUONG"] - (decimal)r["SO_LUONG"];
                }
            }
            foreach(DataRow r in dtTon.Rows)
            {
                if ((decimal)r["SO_LUONG"] > 0)
                {
                    rowCt["GIA_VON"] = r["GIA_VON"];
                    rowCt["TIEN_VON"] = Math.Round((decimal)r["SO_LUONG"] * (decimal)r["GIA_VON"],0);
                    rowCt.EndEdit();
                    break;
                }
            }
        }

        private void txtSo_Ctu_Leave(object sender, System.EventArgs e)
        {
            try
            {
                this.EndEdit();
                if (this.Datatable.Rows.Count == 0 || this.Datatable.Rows[0]["SO_CTU"].ToString() != "") return;
                DataRow rowHead = this.Datatable.Rows[0];
                string so_ctu = "";
                string query = "SELECT SO_CTU, 0 as SO FROM VTHH WHERE NX = '" + this.NX + "' AND PK <> " + rowHead["PK"].ToString();
                DataTable dt = this.cnMain.LoadDatatableQuery(query,"SO_CTU");
                int so_max = 0;
                foreach(DataRow r in dt.Rows)
                {
                    string s = r["SO_CTU"].ToString().Trim();
                    int so = 0;
                    if(int.TryParse(s, out so))
                    {
                        so = int.Parse(s);
                        if(so_max < so)
                        {
                            so_max = so;
                        }
                    }
                }
                so_ctu = (so_max + 1).ToString().PadLeft(4, '0');
                this.Datatable.Rows[0]["SO_CTU"] = so_ctu;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void txtMa_TextChanged(object sender, EventArgs e)
        {
            TextBox texbox = sender as TextBox;
            if (texbox.Equals(this.txtMa_Kho)) this.lblTen_Kho.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_KHO"], this.txtMa_Kho.Text, "TEN_KHO").ToString();
            else if (texbox.Equals(this.txtMa_Ptnx)) this.lblTen_Ptnx.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_PTNX"], this.txtMa_Ptnx.Text, "TEN_PTNX").ToString();
            else if (texbox.Equals(this.txtMa_Dtpn)) this.lblTen_Dtpn.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_DTPN"], this.txtMa_Dtpn.Text, "TEN_DTPN").ToString();
        }
    }
}

