﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public partial class FrmReportOutput : CnForm
    {
        CnMain cnMain;
        Rpt_Report rpt;
        public FrmReportOutput(CnMain cnmain, Rpt_Report rpt)
        {
            try
            {
                InitializeComponent();
                this.cnMain = cnmain;
                this.MdiParent = cnmain.mainForm;
                this.rpt = rpt;
                this.LoadReport();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void LoadReport()
        {
            ReportDataSource rds = new ReportDataSource(this.rpt.Table_name, this.rpt.Dataset.Tables[0]);
            rds.Name = "Dataset";
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            string file = rpt.File_name;
            if (!System.IO.File.Exists(file))
            {
                throw new Exception("File không tồn tại: " + file);
            }
            this.reportViewer1.LocalReport.ReportPath = file;
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            this.reportViewer1.ZoomMode = ZoomMode.Percent;
            this.reportViewer1.ZoomPercent = 75;
            ReportParameterInfoCollection parainfos = this.reportViewer1.LocalReport.GetParameters();
            ReportParameterCollection paras = new ReportParameterCollection();
            ReportParameter para;
            if (this.CheckExistsParameter(parainfos, "company_name"))
            {
                para = new ReportParameter("company_name", this.cnMain.GetSystemVar("company_name").ToString());
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "report_name"))
            {
                para = new ReportParameter("report_name", this.rpt.Report_name);
                paras.Add(para);
            }
            if (this.CheckExistsParameter(parainfos, "ten_kyslieu"))
            {
                para = new ReportParameter("ten_kyslieu", "Từ ngày " + this.rpt.Kyslieu.Tu_Ngay.ToString("dd/MM/yyyy") + " đến ngày " + this.rpt.Kyslieu.Den_Ngay.ToString("dd/MM/yyyy"));
                paras.Add(para);
            }
            this.reportViewer1.LocalReport.SetParameters(paras);
            this.reportViewer1.RefreshReport();

        }
        private bool CheckExistsParameter(ReportParameterInfoCollection parainfos, string paraname)
        {
            foreach (ReportParameterInfo parainfo in parainfos)
            {
                if (parainfo.Name == paraname)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
