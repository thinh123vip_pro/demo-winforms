﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public partial class FrmReportInput : CnForm
    {
        CnMain cnMain;
        DataSet Dataset;
        DataRow RowReport;
        DataTable dtSrc, dtDes;
        public FrmReportInput(CnMain cnmain, DataRow rowReport)
        {
            InitializeComponent();
            this.cnMain = cnmain;
            this.Dataset = new DataSet();
            this.MdiParent = cnmain.mainForm;
            this.RowReport = rowReport;
            this.Loaddata();
            this.BinGrid();
            this.ConfigGrid();
            this.SetControls();

        }
        private void Loaddata()
        {
            string query = "SELECT '' AS ID, '' AS NAME FROM DM_REPORT WHERE 1=0";
            this.dtSrc = this.cnMain.LoadDatatableQuery(query, "SRC");
            query = "SELECT '' AS TABLE_NAME, '' AS ID, '' AS NAME FROM DM_REPORT WHERE 1=0";
            this.dtDes = this.cnMain.LoadDatatableQuery(query, "DES");
        }
        private void BinGrid()
        {
            this.gridSrc.DataSource = this.dtSrc;
            this.gridDes.DataSource = this.dtDes;
        }
        private void ConfigGrid()
        {
            this.gridSrc.Columns[0].Width = 50;
            this.gridSrc.Columns[1].Width = 200;
            this.gridDes.Columns[0].Width = 100;
            this.gridDes.Columns[1].Width = 50;
            this.gridDes.Columns[2].Width = 100;
            this.gridSrc.AllowUserToAddRows = false;
            this.gridDes.AllowUserToAddRows = false;
            this.gridSrc.ReadOnly = true;
            this.gridDes.ReadOnly = true;
            this.gridSrc.CellDoubleClick += gridSrc_CellDoubleClick;
            this.gridDes.CellDoubleClick += gridDes_CellDoubleClick;
        }

        void gridDes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.gridDes.CurrentRow != null && this.gridDes.CurrentRow.DataBoundItem != null)
                {
                    DataRow r = ((DataRowView)this.gridDes.CurrentRow.DataBoundItem).Row;
                    string table_name = r[0].ToString();
                    string id = r[1].ToString();
                    string name = r[2].ToString();

                    r.Delete();
                    if (table_name == this.cboTable.SelectedItem.ToString())
                    {
                        DataRow nr = this.dtSrc.NewRow();
                        nr[0] = id;
                        nr[1] = name;
                        this.dtSrc.Rows.Add(nr);
                    }
                }
                this.dtSrc.AcceptChanges();
                this.dtDes.AcceptChanges();
                this.gridSrc.Sort(this.gridSrc.Columns[0], ListSortDirection.Ascending);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void gridSrc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.gridSrc.CurrentRow != null && this.gridSrc.CurrentRow.DataBoundItem != null)
                {
                    DataRow r = ((DataRowView)this.gridSrc.CurrentRow.DataBoundItem).Row;
                    string table_name = this.cboTable.SelectedItem.ToString();
                    string id = r[0].ToString();
                    string name = r[1].ToString();

                    DataRow nr = this.dtDes.NewRow();
                    nr[0] = table_name;
                    nr[1] = id;
                    nr[2] = name;
                    this.dtDes.Rows.Add(nr);
                    r.Delete();
                }
                this.dtSrc.AcceptChanges();
                this.dtDes.AcceptChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SetControls()
        {
            List<KySlieu> kyslieulist = Functions.LoadKySlieu((int)this.cnMain.GetSystemVar("WORKING_YEAR"), true);
            foreach (KySlieu kyslieu in kyslieulist)
            {
                this.cboKySlieu.Items.Add(kyslieu);
            }
            this.cboKySlieu.SelectedIndex = 0;
            string[] filter_list_arr = this.RowReport["FILTER_LIST"].ToString().Split(',');
            foreach(string s in filter_list_arr)
            {
                this.cboTable.Items.Add(s);
            }
            this.cboTable.SelectedIndex = 0;
            this.lblReportName.Text = this.RowReport["REPORT_NAME"].ToString();
        }
        private void cboKySlieu_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (this.cboKySlieu.SelectedIndex < 0) return;
                KySlieu kyslieu = (KySlieu)this.cboKySlieu.SelectedItem;
                this.datTu_Ngay.Value = kyslieu.Tu_Ngay;
                this.datDen_Ngay.Value = kyslieu.Den_Ngay;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cboTable_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (this.cboTable.SelectedIndex < 0) return;
                string table_name = this.cboTable.SelectedItem.ToString();
                string id_field = this.cnMain.GetFieldNameMa(table_name);
                string name_field = this.cnMain.GetFieldNameTen(table_name);
                string query = "SELECT " + id_field + " AS ID, " + name_field + " AS NAME FROM " + table_name;
                this.dtSrc = this.cnMain.LoadDatatableQuery(query,table_name);
                dtSrc.PrimaryKey = new DataColumn[] { dtSrc.Columns[0] };
                this.gridSrc.DataSource = dtSrc;
                foreach(DataGridViewRow row in this.gridDes.Rows)
                {
                    string table_name_des = row.Cells[0].Value.ToString();
                    string id_des = row.Cells[1].Value.ToString();
                    if (table_name_des == table_name)
                    {
                        DataRow fr = dtSrc.Rows.Find(id_des);
                        if (fr != null)
                        {
                            fr.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSelectAll_Click(object sender, System.EventArgs e)
        {
            try
            {
                foreach (DataRow r in this.dtSrc.Rows)
                {
                    string table_name = this.cboTable.SelectedItem.ToString();
                    string id = r[0].ToString();
                    string name = r[1].ToString();

                    DataRow nr = this.dtDes.NewRow();
                    nr[0] = table_name;
                    nr[1] = id;
                    nr[2] = name;
                    this.dtDes.Rows.Add(nr);
                }
                this.dtSrc.Clear();
                this.dtSrc.AcceptChanges();
                this.dtDes.AcceptChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRemoveAll_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.dtDes.Clear();
                this.cboTable_SelectedIndexChanged(null, null);
                this.dtSrc.AcceptChanges();
                this.dtDes.AcceptChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void btnXem_Click(object sender, System.EventArgs e)
        {
            try
            {
                KySlieu kyslieu = (KySlieu)this.cboKySlieu.SelectedItem;
                kyslieu.Tu_Ngay = this.datTu_Ngay.Value;
                kyslieu.Den_Ngay = this.datDen_Ngay.Value;
                string dk = this.GetDieuKien();
                string report_id = this.RowReport["REPORT_ID"].ToString();
                
                Rpt_Report rpt = null;
                if(report_id.StartsWith("RPT_BKEVTHH_NHAP"))
                {
                    rpt = new Rpt_BkeVthh(this.cnMain, kyslieu, dk,this.RowReport,"N");
                }
                else if (report_id.StartsWith("RPT_BKEVTHH_XUAT"))
                {
                    rpt = new Rpt_BkeVthh(this.cnMain, kyslieu, dk, this.RowReport, "X");
                }
                else if (report_id.StartsWith("RPT_NXT"))
                {
                    rpt = new Rpt_Nxt(this.cnMain, kyslieu, dk, this.RowReport);
                }
                else
                {
                    throw new Exception("REPORT_ID không hợp lệ");
                }
                FrmReportOutput frm = new FrmReportOutput(this.cnMain, rpt);
                frm.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private string GetDieuKien()
        {
            string dk = "(1=1";
            DataView dv = new DataView(this.dtDes, "", "TABLE_NAME,ID", DataViewRowState.CurrentRows);
            string table_name_truoc = "";
            foreach(DataRowView r in dv)
            {
                string table_name = r[0].ToString();
                string id = r[1].ToString();
                string field_ma = this.cnMain.GetFieldNameMa(table_name);
                if(table_name != table_name_truoc)
                {
                    dk += ") and (" + field_ma + " = '" + id + "'";
                }
                else
                {
                    dk += " or " + field_ma + " = '" + id + "'";
                }
                table_name_truoc = table_name;
            }
            dk += ")";
            
            return dk;
        }
    }
}
