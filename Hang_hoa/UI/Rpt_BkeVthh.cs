﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Cn.Base;
using Cn.Tudien;

namespace Cn.UI
{
    public class Rpt_BkeVthh:Rpt_Report
    {
        public Rpt_BkeVthh(CnMain cnmain, KySlieu kyslieu, string dieu_kien, DataRow rowReport, string NX):base(cnmain,kyslieu,dieu_kien,rowReport)
        {
            string query = "SELECT * FROM VTHH_VIEW WHERE NGAY_CTU >= " + Functions.ParseDate(kyslieu.Tu_Ngay) + " AND NGAY_CTU <= " + Functions.ParseDate(kyslieu.Den_Ngay) + " AND " + this.Dieu_kien_loc + " AND NX = '"+NX+"'";
            this.cnMain.LoadDatatableQuery(query, this.Dataset, this.Table_name);
        }
    }
}
