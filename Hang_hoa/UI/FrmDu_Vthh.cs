﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cn.Base;

namespace Cn.UI
{
    public partial class FrmDu_Vthh : FrmBaseDm
    {
        public FrmDu_Vthh(CnMain cnmain)
            : base(cnmain, "DU_VTHH")
        {
            InitializeComponent();
            this.LoadData();
            this.LoadDanhmuc();
            this.BinGrid();
            this.ConfigGrid();
        }
        protected override void LoadDanhmuc()
        {
            this.cnMain.LoadDatatable(this.Dataset, "DM_VTHH");
            this.cnMain.LoadDatatable(this.Dataset, "DM_KHO");
        }
        protected override void BinGrid()
        {
            base.BinGrid();
            Functions.SetGridDateColumn(this.grid, "NGAY_CTU", "Ngày chứng từ", 100);
            Functions.SetGridTextColumn(this.grid, "MA_KHO", "Kho", 100);
            Functions.SetGridTextColumn(this.grid, "MA_VTHH", "Mã vthh", 100);
            Functions.SetGridTextColumn(this.grid, "TEN_VTHH", "Tên vthh", 200, false);
            Functions.SetGridTextColumn(this.grid, "DVT", "Đvt", 50, false);
            Functions.SetGridNumberColumn(this.grid, "SO_LUONG", "Số lượng", 100, this.Datatable.Columns["SO_LUONG"].DataType, 2);
            Functions.SetGridNumberColumn(this.grid, "GIA_VON", "Giá vốn", 100, this.Datatable.Columns["GIA_VON"].DataType, 2);
            Functions.SetGridNumberColumn(this.grid, "TIEN_VON", "Tiền vốn", 100, this.Datatable.Columns["TIEN_VON"].DataType, 0);
        }
        protected override void ConfigGrid()
        {
            base.ConfigGrid();
            this.grid.Columns["TEN_VTHH"].ReadOnly = true;
            this.grid.Columns["DVT"].ReadOnly = true;
            this.grid.CellLeave += grid_CellLeave;
            this.grid.CellDoubleClick += grid_CellDoubleClick;
            this.grid.CellFormatting += grid_CellFormatting;
            this.grid.CellEndEdit += grid_CellEndEdit;
            this.grid.CurrentCellChanged += grid_CurrentCellChanged;
        }

        void grid_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.grid.CurrentCell != null)
                {
                    string columnname = this.grid.CurrentCell.OwningColumn.Name;
                    switch (columnname)
                    {
                        case "MA_KHO":
                            this.toolStripMsg.Text = this.cnMain.GetFieldValue(this.Dataset.Tables["DM_KHO"], this.grid.CurrentCell.Value.ToString(), "TEN_KHO").ToString();
                            break;
                        default:
                            this.toolStripMsg.Text = "";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        void grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.grid.Refresh();
        }
        void grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.RowIndex == this.grid.NewRowIndex)
                {
                    if (this.grid.Columns[e.ColumnIndex].ValueType == typeof(System.Decimal))
                    {
                        e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
                        e.CellStyle.ForeColor = Color.Blue;
                        decimal sum = 0;
                        for (int i = 0; i < this.grid.NewRowIndex; i++)
                        {
                            var value = this.grid[e.ColumnIndex, i].Value;
                            if (value != null)
                            {
                                sum += (decimal)this.grid[e.ColumnIndex, i].Value;
                            }
                        }
                        e.Value = Math.Round(sum, 2);
                    }
                }
                else
                {
                    string colname = this.grid.Columns[e.ColumnIndex].Name;
                    DataRow fr;
                    switch (colname)
                    {
                        case "TEN_VTHH":
                            fr = this.Dataset.Tables["DM_VTHH"].Rows.Find(this.grid.Rows[e.RowIndex].Cells["MA_VTHH"].Value);
                            if (fr != null) e.Value = fr["TEN_VTHH"];
                            else e.Value = "";
                            break;
                        case "DVT":
                            fr = this.Dataset.Tables["DM_VTHH"].Rows.Find(this.grid.Rows[e.RowIndex].Cells["MA_VTHH"].Value);
                            if (fr != null) e.Value = fr["DVT"];
                            else e.Value = "";
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void grid_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!(this.grid.EditingControl is DataGridViewTextBoxEditingControl))
                {
                    return;
                }
                if (this.grid.CurrentRow.DataBoundItem == null || this.grid.EditingControl == null || this.Datatable.Columns.IndexOf(this.grid.Columns[e.ColumnIndex].Name) < 0) return;
                string colname = this.grid.Columns[e.ColumnIndex].Name;
                DataGridViewTextBoxEditingControl textbox = (DataGridViewTextBoxEditingControl)(sender as DataGridView).EditingControl;
                DataRowView row = (DataRowView)this.grid.CurrentRow.DataBoundItem;
                row[this.grid.Columns[e.ColumnIndex].Name] = textbox.EditingControlFormattedValue;
                switch (colname)
                {
                    case "MA_KHO":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_KHO", textbox);
                        break;
                    case "MA_VTHH":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_VTHH", textbox);
                        break;
                    case "SO_LUONG":
                        row["TIEN_VON"] = Math.Round((decimal)row["SO_LUONG"] * (decimal)row["GIA_VON"], 0);
                        row.EndEdit();
                        break;
                    case "GIA_VON":
                        row["TIEN_VON"] = Math.Round((decimal)row["SO_LUONG"] * (decimal)row["GIA_VON"], 0);
                        row.EndEdit();
                        break;
                    case "TIEN_VON":
                        if ((decimal)row["GIA_VON"] == 0 && (decimal)row["SO_LUONG"] != 0)
                        {
                            row["GIA_VON"] = Math.Round((decimal)row["TIEN_VON"] / (decimal)row["SO_LUONG"], 2);
                        }
                        row.EndEdit();
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!(this.grid.EditingControl is DataGridViewTextBoxEditingControl))
                {
                    return;
                }
                if (this.grid.CurrentRow.DataBoundItem == null || this.grid.EditingControl == null || this.Datatable.Columns.IndexOf(this.grid.Columns[e.ColumnIndex].Name) < 0) return;
                string colname = this.grid.Columns[e.ColumnIndex].Name;
                DataGridViewTextBoxEditingControl textbox = (DataGridViewTextBoxEditingControl)(sender as DataGridView).EditingControl;
                DataRowView row = (DataRowView)this.grid.CurrentRow.DataBoundItem;
                row[this.grid.Columns[e.ColumnIndex].Name] = textbox.EditingControlFormattedValue;
                switch (colname)
                {
                    case "MA_KHO":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_KHO", textbox,true);
                        this.grid.EndEdit();
                        break;
                    case "MA_VTHH":
                        this.cnMain.ProcessTudienGrid(this.Dataset, "DM_VTHH", textbox,true);
                        this.grid.EndEdit();
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public override DataRow SetAfterAddNew()
        {
            DataRow nr = base.SetAfterAddNew();
            if (this.grid.CurrentCell.OwningColumn.Name != "NGAY_CTU")
            {
                nr["NGAY_CTU"] = this.cnMain.Day_Start;
            }
            return nr;
        }
    }
}
